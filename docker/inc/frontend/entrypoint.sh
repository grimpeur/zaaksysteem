#!/bin/sh

#
# USAGE: ZAAKSYSTEEM_API_HOST=zaysteem-api.default.svc.cluster.local ./entrypoint.sh
#

cd /etc/nginx
cp zaaksysteem.conf.tpl zaaksysteem.conf

DNS_RESOLVER=$(grep nameserver /etc/resolv.conf| head -n 1| awk '{ print $2; }');

sed -i -e "s/ZAAKSYSTEEM_HOST_API/${ZAAKSYSTEEM_HOST_API-backend}/" zaaksysteem.conf
sed -i -e "s/ZAAKSYSTEEM_HOST_CSV/${ZAAKSYSTEEM_HOST_CSV-api2csv}/" zaaksysteem.conf
sed -i -e "s/ZAAKSYSTEEM_HOST_BBV/${ZAAKSYSTEEM_HOST_BBV-bbvproxy}/" zaaksysteem.conf
sed -i -e "s/DNS_RESOLVER/${DNS_RESOLVER}/" zaaksysteem.conf

exec "$@"