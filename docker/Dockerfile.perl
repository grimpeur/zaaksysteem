FROM perl:5.22

# Build: docker build -f docker/Dockerfile.perl -t registry.gitlab.com/zaaksysteem/zaaksysteem-perl .
# Push: docker push registry.gitlab.com/zaaksysteem/zaaksysteem-perl

ENV DEBIAN_FRONTEND noninteractive

RUN groupadd -g 1001 zaaksysteem \
    && useradd -ms /bin/bash -u 1001 -g 1001 zaaksysteem -d /opt/zaaksysteem \
    && chown -R zaaksysteem /opt/zaaksysteem \
    && echo "deb http://ftp.nl.debian.org/debian jessie-backports main" > /etc/apt/sources.list.d/backports.list \
    && echo "deb http://ftp.nl.debian.org/debian jessie main" > /etc/apt/sources.list \
    && echo "deb http://ftp.nl.debian.org/debian jessie-updates main" >> /etc/apt/sources.list \
    && echo "deb http://security.debian.org jessie/updates main" >> /etc/apt/sources.list \
    && apt-get update \
    && apt-get -y install libpq-dev uuid-dev locales ghostscript netpbm xmlsec1 unzip fonts-liberation \
          fonts-crosextra-caladea fonts-crosextra-carlito fonts-linuxlibertine fonts-sil-gentium-basic libmagic-dev \
    && apt-get --no-install-recommends -t jessie-backports -y install libreoffice unoconv \
    && localedef -i nl_NL -c -f UTF-8 -A /usr/share/locale/locale.alias nl_NL.UTF-8 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# This is a bit of a special case...
RUN ln -s /usr/include/x86_64-linux-gnu/ImageMagick-6 /usr/local/include \
    && ln -s /usr/local/lib/perl5/5.22.*/x86_64-linux/CORE/libperl.so /usr/local/lib \
    && cpanm --notest Image::Magick \
    && rm -rf ~/.cpanm

COPY cpanfile /opt/zaaksysteem/cpanfile

# Do NOT remove the || cpanm: Michiel says:
# Laten staan, cpanm komt er niet in 1 keer doorheen. Er zit een
# dependency somewhere in de tree welke een oudere versie nodig heeft
# dan de versie die automatisch (als laatste versie) wordt geinstalleerd
# door een derde module. Gevolg is dat cpanm breekt: is informatie op
# google over te vinden, enige oplossing is om het nogmaals te proberen.
# Vandaar deze "-OR"
RUN cpanm --notest --installdeps /opt/zaaksysteem \
    || cpanm --notest --installdeps /opt/zaaksysteem \
    && rm -rf ~/.cpanm

RUN cpanm https://gitlab.com/zaaksysteem/File-ArchivableFormats.git \
    && cpanm https://gitlab.com/zaaksysteem/zaaksysteem-instance_config.git \
    && cpanm https://gitlab.com/zaaksysteem/zaaksysteem-tools.git \
    && rm -rf ~/.cpanm
