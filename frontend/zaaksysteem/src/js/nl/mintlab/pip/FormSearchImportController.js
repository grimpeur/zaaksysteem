/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.pip')
		.controller('nl.mintlab.pip.FormSearchImportController', [ '$scope', '$window', 'smartHttp', 'translationService', function ( $scope, $window, smartHttp, translationService ) {
			
			smartHttp.connect({
				method: 'GET',
				url: '/form/integrale_zoekvraag_import',
				params: {
					url: $window.location.href
				}
			})
				.success(function ( response ) {
					var url = response.result[0];
					$window.location = url;
				})
				.error(function ( ) {
					$scope.$emit('systemMessage', {
						type: 'error',
						content: translationService.get('Er ging iets fout bij de zoekopdracht. Probeer het opnieuw.')
					});
				});
			
		}]);
	
})();