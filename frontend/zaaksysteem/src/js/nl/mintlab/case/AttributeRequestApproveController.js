/*global angular*/
(function ( ) {
    "use strict";


    angular.module('Zaaksysteem.case')
        .controller('nl.mintlab.case.AttributeRequestApproveController', [ '$scope', function ( $scope ) {

            $scope.editMode = false;

            $scope.enableEditMode = function ( ) {
                $scope.editMode = true;
            };

            $scope.cancelEditMode = function ( ) {
                $scope.editMode = false;
            };
            
            

        }]);

})();