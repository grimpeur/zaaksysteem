/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.directive('zsAttributeEditForm', [ function ( ) {
			
			return {
				require: '^?zsAttributeFormField',
				scope: true,
				link: function ( scope, element, attrs, zsAttributeFormField ) {
					scope.$on('form.change.committed', function ( ) {
						zsAttributeFormField.saveList();
					});
				}
			};
			
		}]);
	
})();