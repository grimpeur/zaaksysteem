/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.AttributeItemController', [ '$scope', function ( $scope ) {
			
			$scope.isOpen = function ( ) {
				return $scope.getSelectedAttribute() === $scope.object;
			};
			
			$scope.toggleOpen = function ( ) {
				$scope.isOpen() ? $scope.setSelectedAttribute(null) : $scope.setSelectedAttribute($scope.object);
			};
			
		}]);
	
})();