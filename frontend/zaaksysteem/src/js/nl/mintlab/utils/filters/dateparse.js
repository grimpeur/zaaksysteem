/*global angular,console*/
/* parse date in format YYYYMMDD to convert it into something Date()/Angular understands */
(function () {
    "use strict";
    angular.module('Zaaksysteem')
        .filter('dateParse', function () {

            function parse(str) {
                if (!/^(\d){8}$/.test(str)) {
                    return "-";
                }
                var y = str.substr(0, 4),
                    m = str.substr(4, 2),
                    d = str.substr(6, 2),
                    // months start with zero, year and day don't :)
                    month = parseInt(m, 10) - 1,
                    date = new Date(y, month, d);

                return date.getTime();
            }

            return function (source) {
                if (typeof source === 'undefined') {
                    return;
                }
                return parse(source);
            };
        });
}());