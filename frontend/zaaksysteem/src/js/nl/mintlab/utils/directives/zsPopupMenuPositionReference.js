/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsPopupMenuPositionReference', [ function ( ) {
			
			return {
				require: '^zsPopupMenu',
				link: function ( scope, element, attrs, zsPopupMenu ) {
					zsPopupMenu.setReference(element);
					
					scope.$on('$destroy', function ( ) {
						zsPopupMenu.setReference(null);
					});
				}
			};
						
		}]);
})();