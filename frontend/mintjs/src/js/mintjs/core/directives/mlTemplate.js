/*global angular*/
(function ( ) {
	
	angular.module('MintJS.core')
		.directive('mlTemplate', [ '$compile', function ( $compile ) {
			
			return {
				link: function ( scope, element, attrs ) {
					
					var template = scope.$eval(attrs.mlTemplate);
					
					$compile(template)(scope, function ( clonedElement/*, sc*/) {
						element.append(angular.element(clonedElement));
					});
					
				}
			};
			
		}]);
	
})();