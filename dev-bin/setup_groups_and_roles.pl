#!/usr/bin/env perl

use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI;

my $cli = Zaaksysteem::CLI->init();

my %options = %{ $cli->options };

if ($options{init}) {

    my $organisation = $options{organisation};

    $cli->do_transaction(
        sub {
            my $self   = shift;
            my $schema = shift;

            if (!defined $organisation) {
                $organisation = _get_default_group_name($schema) // 'Mintlab';
            }

            my $parent = _create_or_update_group($schema,
                ucfirst($organisation));

            my $fo = _create_or_update_group($schema, 'Frontoffice', $parent);
            my $bo = _create_or_update_group($schema, 'Backoffice', $parent);

            map {
                _create_or_update_role($schema, ucfirst($_), $parent,)
                } qw(
                Administrator
                Zaaksysteembeheerder
                Zaaktypebeheerder
                Zaakbeheerder
                Contactbeheerder
                Basisregistratiebeheerder
                Wethouder
                Directielid
                Afdelingshoofd
                Kcc-medewerker
                Zaakverdeler
                Behandelaar
                Gebruikersbeheerder
                Documentintaker
            ), 'BRP externe bevrager';

            my $ldap_auth = $schema->resultset("Interface")
                ->search_active({ module => 'authldap' })->first;

            my $admin_role = $schema->resultset('Roles')
                ->search({ name => 'Administrator' })->first;

            create_subject(
                $schema, $ldap_auth, 'admin', 'admin',
                $parent, [$admin_role]
            );

            my $beheerder_role = $schema->resultset('Roles')
                ->search({ name => 'Zaaksysteembeheerder' })->first;

            create_subject($schema, $ldap_auth, 'beheerder', 'admin',
                $parent, [$beheerder_role]);

            my $behandelaar = $schema->resultset('Roles')
                ->search({ name => 'Behandelaar' })->first;
            create_subject($schema, $ldap_auth, 'backoffice', 'admin',
                $bo, [$behandelaar]);

            create_subject($schema, $ldap_auth, 'frontoffice', 'admin',
                $fo, [$behandelaar]);

        }
    );

}
else {

    my $organisation = $options{organisation};
    my $group        = $options{group};
    my $role         = $options{role};

    $cli->do_transaction(
        sub {
            my $self   = shift;
            my $schema = shift;

            if (!defined $organisation) {
                $organisation = _get_default_group_name($schema);
                if (!$organisation) {
                    die "Organistion is not defined";
                }
            }

            my $parent = _create_or_update_group($schema,
                ucfirst($organisation));

            if ($group) {
                $parent
                    = _create_or_update_group($schema, ucfirst($group),
                    $parent);
            }

            if ($role) {
                $role = _create_or_update_role($schema, ucfirst($role),
                    $parent);
            }
        }
    );
}

sub _get_default_group_name {
    my $schema = shift;

    my $group = $schema->resultset('Groups')->search_rs(
        {
            id => {
                in => \
                    'SELECT id FROM groups WHERE path = array[groups.id]'
            }
        }, { order_by => 'id' }
    )->first;
    return $group ? $group->name : undef;
}

sub _create_or_update_group {
    my ($schema, $name, $parent) = @_;

    my $org = $schema->resultset('Groups')->search({ name => $name })
        ->first;

    return $org if $org;

    return $schema->resultset('Groups')->create_group(
        {
            name        => $name,
            description => $name,
            $parent ? (parent_group_id => $parent->id) : (),
        }
    );
}

sub _create_or_update_role {
    my ($schema, $name, $parent) = @_;

    my $role
        = $schema->resultset('Roles')->search({ name => $name })->first;
    return $role if $role;

    return $schema->resultset('Roles')->create_role(
        {
            name            => $name,
            parent_group_id => $parent->id,
            description     => "Systeemgroep: $name",
            system_role     => 1,
        }
    );
}

sub create_subject {
    my ($schema, $interface, $username, $password, $group, $roles) = @_;

    my $ldap_data = {
        cn              => $username,
        sn              => $username,
        displayname     => $username,
        givenname       => $username,
        initials        => $username,
        mail            => 'devnull@zaaksysteem.nl',
        telephonenumber => '0612345678',
    };


    my $ue = $schema->resultset('UserEntity')->search_rs(
        {
            source_identifier   => $username,
            source_interface_id => $interface->id,
            date_deleted        => undef,
        }
    )->first;

    my $subject;
    if (!$ue) {
        $subject = $schema->resultset('Subject')->create(
            {
                subject_type => 'employee',
                properties   => $ldap_data,
                username     => $username,
            }
        );

        $ue = $schema->resultset('UserEntity')->create(
            {
                source_interface_id => $interface->id,
                source_identifier   => $username,
                subject_id          => $subject->id,
            }
        );
        $subject->update_password($ue, { password => $password });
    }
    else {
        $subject = $ue->subject_id;
    }

    $subject->group_ids([$group->id]);
    if (@$roles) {
        $subject->role_ids([map { $_->id } grep { defined $_ } @$roles]);
    }

    $subject->update;
}

1;

__END__

=head1 NAME

setup_groups_and_roles.pl - Setup groups and roles for an instance

=head1 SYNOPSIS

setup_groups_and_roles.pl OPTIONS

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * organization

Organization name, to use as primary root of the Organization Tree. Required.

=item * group

A group name to be added

=item * role

A role name to be added

=item * init

Give this a true value and we initialize the groups, roles and users for you.
If you do not give a organisation name we default to Mintlab. The users
C<admin>, C<beheerder>, C<backoffice> and C<frontoffice> are created for you.
If a user already exists, we skip the creation and just put them in the correct group
and add it to the default role.
When this option is supplied, the C<group> and C<role> options are ignored.

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014,2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
