#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

docker build -t registry.gitlab.com/zaaksysteem/zaaksysteem-perl -f docker/Dockerfile.perl .
docker push registry.gitlab.com/zaaksysteem/zaaksysteem-perl