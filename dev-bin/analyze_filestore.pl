#!/usr/bin/env perl

use strict;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::CLI;
use Zaaksysteem::Tools;
use File::UStore;
use File::Find;
use File::Spec::Functions qw[catdir];

use Number::Bytes::Human qw[format_bytes];

my $cli = try {
    Zaaksysteem::CLI->init;
} catch {
    die $_;
};

$cli->do_transaction(sub {
    my $self = shift;
    my $schema = shift;

    my $customer_config = $self->zs_config->get_customer_config($self->hostname);

    my $filestore_path = catdir($customer_config->{ basedir }, 'storage');

    my $files = $schema->resultset('Filestore');

    printf "Analyzing %d filestore entries...\n", $files->count
        if $self->verbose;

    # Look at all this fantastic hardcoding...
    # We need to move the same magic from Sysin::Backend::Filestore::Component
    # to the resultset probably
    my $store = File::UStore->new(
        path => $filestore_path,
        prefix => 'zs_',
        depth => 5
    );

    my @dangling_files;
    my $wasted_space = 0;

    find(sub {
        return unless -f;

        my ($prefix, $uuid) = $_ =~ m[^(zs_)(.*)$];

        unless ($prefix) {
            printf "Found file artifact %s\n", $File::Find::name;
            return;
        }

        my $count = $files->search({ uuid => $uuid })->count;

        unless ($count) {
            printf "Found dangling file %s\n", $File::Find::name;
            $wasted_space += -s $_;
            push @dangling_files, $uuid;
        }

        if ($count > 1) {
            printf "Found multiple filestore rows for %s\n", $uuid;
            return;
        }

        printf "File %s is OK\n", $File::Find::name
            if $self->verbose;
    }, $filestore_path);

    printf "\n%s reclaimable in dangling files\n", format_bytes($wasted_space)
        if $self->verbose;
});

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
