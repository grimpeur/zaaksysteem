package Zaaksysteem::Backend::Message::Roles::Test;
use TestSetup;

=head1 NAME

Zaaksysteem::Backend::Message::Roles::Test - Zaaksysteem Message Test role

=head1 DESCRIPTION

This role is only included in the tests regarding the CRUD for DB::Message.

=head1 METHODS

=head2 create_message

=cut

sub create_message {
    my $self        = shift;
    my %opts        = @_;

    my $case        = $zs->create_case_ok();
    my $subject     = $zs->create_subject_ok();

    return $schema->resultset('Message')->message_create({
        event_type      => 'case/pip/feedback',
        message         => 'A whole lotta text',
        subject_id      => $subject->betrokkene_identifier,
        case_id         => $case->id,
        %opts
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
