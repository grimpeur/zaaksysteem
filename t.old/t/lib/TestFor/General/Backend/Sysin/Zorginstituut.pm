package TestFor::General::Backend::Sysin::Zorginstituut;
use base qw(ZSTest);

use TestSetup;

=head1 NAME

TestFor::General::Backend::Sysin::C2GO - A C2GO unit tester

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut


use DateTime;
use DateTime::Format::Strptime;
use JSON::XS;
use HTTP::Response;
use Hash::Merge qw( merge );

use Zaaksysteem::Backend::Sysin::C2GO::Model;
use Zaaksysteem::Backend::Sysin::Acknowledge::Model;
use Zaaksysteem::Backend::Sysin::iJW::Model;
use Zaaksysteem::Backend::Sysin::iWMO::Model;
use Zaaksysteem::Profile;

sub _interface_ok {

    my %hash = (
        module           => 'zorginstituut',
        interface_config => {
            type             => 'wmo301',
            aanbieder        => 'c2go',
            gemeentecode     => '363',
            endpoint_oauth   => 'foo',
            endpoint_message => 'bar',
            berichtcode      => 414,
            timeout          => 10,
        },
    );

    my $merged = merge({@_}, \%hash);
    return $zs->create_interface_ok(%$merged)->discard_changes;
}

sub _get_time_from_frontend {
    return DateTime::Format::Strptime->new(
        pattern   => "%d-%m-%Y",
        time_zone => "Europe/Amsterdam",
        locale    => 'nl_NL',
    )->format_datetime(DateTime->now());
}

sub _create_case_with_mapping {
    my %opts = @_;

    my $zt = $opts{casetype} // $zs->create_zaaktype_predefined_ok;

    my $mapping = $zs->set_attribute_mapping_on_interface_ok(
        casetype   => $zt,
        interface  => $opts{interface},
        attributes => $opts{mapping},
    );

    my $case = $zs->create_case_ok(zaaktype => $zt)->discard_changes;
    foreach my $k (values %{$mapping}) {
        my $value;
        if (exists $opts{values}{ $k->magic_string }) {
            my $type = $opts{values}{ $k->magic_string }{type};
            $k->update({ value_type => $type });
            $value = $opts{values}{ $k->magic_string }{value};
            $case->update_attribute(
                bibliotheek_kenmerk => $k,
                value               => $value,
            );
        }
    }
    return $case;
}

sub _test_301 {
    my %opts = @_;

    my $start_case;
    if ($opts{two_messages}) {
        my ($attributes, $attribute_values) = _get_attribute_and_values($opts{type});
        $start_case = _create_case_with_mapping(
            interface => $opts{interface},
            mapping   => $attributes,
            values    => $attribute_values,
        );
    }

    my ($attributes, $attribute_values) = _get_attribute_and_values($opts{type}, $start_case);
    my $case = _create_case_with_mapping(
        interface => $opts{interface},
        mapping   => $attributes,
        values    => $attribute_values,
    );

    my %params = (case_id => $case->id);

    my $code = $opts{interface}->get_interface_config->{berichtcode};

    with_stopped_clock {
        no warnings qw(once redefine);

        my $answer = $opts{interface}->model->instance->instance->spoofmode_302('writer', {});

        local *Zaaksysteem::Backend::Sysin::C2GO::Model::call = sub {
            return $answer;
        };

        lives_ok(
            sub {
                $opts{interface}->model->send_301_message(
                    case          => $case,
                    identificatie => '123456789',
                    berichtcode   => $code,
                );
            },
            "send_301_message"
        );

        my ($pt, $record) = $zs->process_interface_trigger_ok(
            interface => $opts{interface},
            trigger   => 'PostStatusUpdate',
            params    => \%params
        );

        if ($record->is_error) {
            diag $record->output;
            return fail("Something did not go as expected");
        }

        my $expect = { case_id => $case->id, code => $code, type => "$opts{type}301", %{$opts{expect} // {}} };
        if (!%{$opts{expect} // {}}) {
            $expect->{send} = 1;
        }

        my $output = decode_json($record->output);
        my $ok = cmp_deeply($output, $expect, "Got correct answer");
        if (!$ok) {
            diag explain $output;
        }
        return $ok;
    };
}

sub zs_c2go_base : Tests {
    $zs->txn_ok(
        sub {
            my $interface = _interface_ok();

            my $instance = Zaaksysteem::Backend::Sysin::iWMO::Model->new(
                interface => $interface,
                schema    => $zs->schema
            );

            my $c2go = Zaaksysteem::Backend::Sysin::C2GO::Model->new(
                schema    => $zs->schema,
                interface => $interface,
                username  => 'foo',
                password  => 'bar',
                tenant    => '666',
                timeout   => 1,
                instance  => $instance,
            );
            isa_ok($c2go, "Zaaksysteem::Backend::Sysin::C2GO::Model");

            {
                no warnings qw(redefine once);
                local *LWP::UserAgent::post = sub {
                    return HTTP::Response->new(200, 'OK', ['Content-Type' => 'application/json'], '{ "access_token" : "Some kind of token" }');
                };

                $c2go->get_oauth_token;
                is($c2go->token, "Some kind of token");
            }

            {
                # Test if we set the correct header information
                my $request;
                no warnings qw(redefine once);
                local *LWP::UserAgent::post = sub {
                    my $self = shift;
                    $request = HTTP::Request::Common::POST(@_);
                    return HTTP::Response->new(200, 'OK', ['Content-Type' => 'application/json'], '{ "Content" : "Some kind of message" }');
                };
                my $answer = $c2go->call(
                    endpoint => 'foo',
                    message  => "foo'bar'baz"
                );
                is_deeply($answer, { Content => "Some kind of message" });

                is($request->headers->header('authorization'), "Bearer " . $c2go->token, "Correct header information");
            }

        },
        "ZS C2GO tests"
    );
}

sub zs_acknowledge_livetest : Tests {
    my $self = shift;
    if (!$ENV{ZS_DEVELOPER_ACKNOWLEDGE}) {
        $self->builder->skip("ZS_DEVELOPER_ACKNOWLEDGE is not set, skipping tests",);
        return;
    }
    $zs->txn_ok(
        sub {
            my $interface = _interface_ok(
                interface_config => {
                    endpoint_message => $ENV{ZS_DEVELOPER_ACKNOWLEDGE_URL},
                    timeout          => 10,
                }
            );

            my $instance = Zaaksysteem::Backend::Sysin::iWMO::Model->new(
                interface => $interface,
                schema    => $zs->schema,
            );

            my $ack = Zaaksysteem::Backend::Sysin::Acknowledge::Model->new(
                schema    => $zs->schema,
                interface => $interface,
                username  => $ENV{ZS_DEVELOPER_ACKNOWLEDGE_USERNAME},
                password  => $ENV{ZS_DEVELOPER_ACKNOWLEDGE_PASSWORD},
                instance  => $instance,
                namespace => 'http://wup.swvo.nl',
            );
            isa_ok($ack, "Zaaksysteem::Backend::Sysin::Acknowledge::Model");

            my ($attributes, $attribute_values) = _get_attribute_and_values('wmo');
            my $case = _create_case_with_mapping(
                interface => $interface,
                mapping   => $attributes,
                values    => $attribute_values,
            );

            my $answer = $ack->send_301_message(
                case          => $case,
                berichtcode   => 414,
            );

            diag explain $answer;
        },
        "Acknowledge live tests"
    );

}

sub _get_attribute_and_values {
    my ($prefix, $case) = @_;
    my @attributes = qw(
        aanbieder
        algemeen_commentaar

        beschikkingsafgiftedatum
        beschikkingsingangsdatum
        beschikkingseinddatum
        beschikkingsnummer

        toegewezen_product_categorie
        toegewezen_product_code
        toegewezen_product_commentaar
        toegewezen_product_omvang_eenheid
        toegewezen_product_omvang_frequentie
        toegewezen_product_omvang_volume
        toegewezen_product_ingangsdatum
        toegewezen_product_toewijzingsdatum
        toegewezen_product_einddatum
        toegewezen_product_reden_intrekking

        toegewezen_product_categorie_1
        toegewezen_product_code_1
        toegewezen_product_commentaar_1
        toegewezen_product_omvang_eenheid_1
        toegewezen_product_omvang_frequentie_1
        toegewezen_product_omvang_volume_1
        toegewezen_product_ingangsdatum_1
        toegewezen_product_toewijzingsdatum_1
        toegewezen_product_einddatum_1
        toegewezen_product_reden_intrekking_1
    );

    my $now        = _get_time_from_frontend;
    my %values = (
        # OLVG
        aanbieder                            => { type => 'numeric', value => '02010009', },

        $case ? (
            beschikkingsnummer    => { type => 'numeric', value => $case->id },
            beschikkingseinddatum => { type => 'date',    value => $now, },
            toegewezen_product_reden_intrekking  => { type => 'text',    value => 'some text', },
        ) : (),

        beschikkingsingangsdatum             => { type => 'date',    value => $now, },
        beschikkingsafgiftedatum             => { type => 'date',    value => $now, },

        toegewezen_product_code              => { type => 'numeric', value => '001', },
        toegewezen_product_categorie         => { type => 'numeric', value => '001', },
        toegewezen_product_ingangsdatum      => { type => 'date',    value => $now, },
        toegewezen_product_omvang_frequentie => { type => 'numeric', value => '1', },
        toegewezen_product_omvang_volume     => { type => 'numeric', value => '1', },
        toegewezen_product_omvang_eenheid    => { type => 'numeric', value => '01', },
        toegewezen_product_toewijzingsdatum  => { type => 'date',    value => $now, },

        toegewezen_product_code_1              => { type => 'numeric', value => '001', },
        toegewezen_product_categorie_1         => { type => 'numeric', value => '001', },
        toegewezen_product_ingangsdatum_1      => { type => 'date',    value => $now, },
        toegewezen_product_omvang_frequentie_1 => { type => 'numeric', value => '1', },
        toegewezen_product_omvang_volume_1     => { type => 'numeric', value => '1', },
        toegewezen_product_omvang_eenheid_1    => { type => 'numeric', value => '01', },
        toegewezen_product_toewijzingsdatum_1  => { type => 'date',    value => $now, },
    );

    my %attributes;
    my %attribute_values;
    foreach (@attributes) {
        my $a = "${prefix}_$_";
        $attributes{$_} = $a;
        $attribute_values{$a} = $values{$_} if defined $values{$_};
    }

    return (\%attributes, \%attribute_values);
}

sub zs_c2go_livetest : Tests {
    my $self = shift;
    if (!$ENV{ZS_DEVELOPER_C2GO}) {
        $self->builder->skip("ZS_DEVELOPER_C2GO is not set, skipping tests",);
        return;
    }
    $zs->txn_ok(
        sub {
            my $interface = _interface_ok(
                interface_config => {
                    username                    => $ENV{ZS_DEVELOPER_C2GO_USERNAME},
                    password                    => $ENV{ZS_DEVELOPER_C2GO_PASSWORD},
                    tenant                      => $ENV{ZS_DEVELOPER_C2GO_TENANT},
                    endpoint_oauth              => $ENV{ZS_DEVELOPER_C2GO_OAUTH},
                    endpoint_message            => $ENV{ZS_DEVELOPER_C2GO_MESSAGE},
                    use_zorginstituut_converter => 1,
                    zorginstituut_username      => $ENV{ZS_DEVELOPER_ZORGINSTITUUT_USERNAME},
                    zorginstituut_password      => $ENV{ZS_DEVELOPER_ZORGINSTITUUT_PASSWORD},
                    zorginstituut_endpoint      => 'https://www.istandaarden.nl/services/soap',
                },
            );
            my $c2go = $interface->model;

            isa_ok($c2go, "Zaaksysteem::Backend::Sysin::C2GO::Model");

            my ($attributes, $attribute_values) = _get_attribute_and_values('wmo');
            my $case = _create_case_with_mapping(
                interface => $interface,
                mapping   => $attributes,
                values    => $attribute_values,
            );

            my $token = $c2go->get_oauth_token;
            is($c2go->token, $token, "Token is set");

            $c2go->send_301_message(
                case          => $case,
                identificatie => '123456789',
                berichtcode   => 414,
            );
        },
        "ZS C2GO tests"
    );
}

sub zs_c2go_wmo_via_interface : Tests {
    $zs->txn_ok(
        sub {

            my $interface = _interface_ok();
            my $c2go      = $interface->model;
            isa_ok($c2go, "Zaaksysteem::Backend::Sysin::C2GO::Model");

            _test_301(
                type      => 'wmo',
                interface => $interface,
            );
        },
        "WMO301 interface integration tests",
    );
}

sub zs_wmo_two_msg : Tests {
    $zs->txn_ok(
        sub {

            my $interface = _interface_ok();
            my $c2go      = $interface->model;
            isa_ok($c2go, "Zaaksysteem::Backend::Sysin::C2GO::Model");

            _test_301(
                type         => 'wmo',
                interface    => $interface,
                two_messages => 1,
            );

            {
                my $counter = 0;
                no warnings qw(redefine once);
                local *Zaaksysteem::Backend::Sysin::Zorginstituut::Model::encode_301_message = sub {
                    $counter++;
                    return 1;
                };
                _test_301(
                    type         => 'wmo',
                    interface    => $interface,
                    two_messages => 1,
                );

                is($counter, 4, "Encode is called twice and not once");
            }

        },
        "WMO301 interface integration tests",
    );
}

sub zs_c2go_jw_via_interface : Tests {
    $zs->txn_ok(
        sub {

            my $interface = _interface_ok(
                interface_config => {
                    berichtcode  => '436',
                    spoofmode => 1,
                }
            );
            my $c2go = $interface->model;
            isa_ok($c2go, "Zaaksysteem::Backend::Sysin::C2GO::Model");

            _test_301(
                type      => 'jw',
                interface => $interface,
                expect => {
                   errors => undef,
                   type => 'jw301',
                   valid => 1
                }
            );


        },
        "Interface integration tests",
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
