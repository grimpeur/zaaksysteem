package TestFor::General::BR::Controlpanel;
use base qw(ZSTest);

use TestSetup;


use Zaaksysteem::BR::Controlpanel;

sub _init {
    return Zaaksysteem::BR::Controlpanel->new(
        schema  => $zs->schema,
        bedrijf => $zs->create_bedrijf_ok(),
    );
}

sub _create_fqdn {
    return Zaaksysteem::TestUtils::generate_random_string()
        . ".saas.saassysteem.nl";
}


sub _check_permission_for_behandelaar {
    my $self    = shift;
    my $uuid    = shift;

    my $behandelaar_role = $self->{_behandelaar_role};
    if (!$behandelaar_role) {
        my ($behandelaar) = grep ({ $_->system_role && $_->name eq 'Behandelaar' } @{ $schema->resultset('Roles')->get_all_cached() });
        $self->{_behandelaar_role} = $behandelaar_role = $behandelaar;
    }

    is($schema->resultset('ObjectAclEntry')->search(
        {
            'object_uuid'   => $uuid,
            'entity_type'   => 'role',
            'entity_id'     => $behandelaar_role->id,
        }
    )->count, 2, 'Found read/write permissions for role: Behandelaar');
}

sub zs_br_controlpanel : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {
            my $bedrijf = $zs->create_bedrijf_ok();
            my $br = Zaaksysteem::BR::Controlpanel->new_from_betrokkene(
                schema        => $zs->schema,
                betrokkene_id => $bedrijf->bid,
            );

            isa_ok($br, "Zaaksysteem::BR::Controlpanel");
            $br->controlpanel;
        },
        "New from betrokkene",
    );

    $zs->txn_ok(
        sub {
            my $br = _init();

            my $naw = $br->naw;
            isa_ok($naw, "Zaaksysteem::Object::Types::Naw");

            my $cp = $br->controlpanel();

            isa_ok($cp, "Zaaksysteem::Object::Types::Controlpanel");
            is($cp->id, undef, "Unsaved controlpanel object");

            ok($br->save(), "Save went succesful");

            $self->_check_permission_for_behandelaar($br->_controlpanel->id);

            $naw = $br->naw;
            isnt($naw->id, undef, "Saved Naw object");
            $self->_check_permission_for_behandelaar($naw->id);

            my $cp_2 = $br->controlpanel;
            is_deeply(
                $cp_2->TO_JSON->{values},
                $cp->TO_JSON->{values},
                "After save we have the same object"
            );

            throws_ok(
                sub {
                    $br->controlpanel($cp);
                },
                qr#controlpanel/object/owner/unique#,
                "Cannot add another unsaved controlpanel when its saved"
            );

            lives_ok(
                sub {
                    $br->controlpanel($cp_2);
                },
                "Setting a saved control panel does work"
            );

        },
        "Get control panel from empty customer"
    );

    $zs->txn_ok(
        sub {
            my $br = _init();
            my $cp = $br->controlpanel(
                {
                    template      => 'foo',
                    customer_type => 'government',

                    read_only      => 1,
                }
            );
            isa_ok($cp, "Zaaksysteem::Object::Types::Controlpanel");
            is($cp->id, undef, "Unsaved controlpanel object");

            ok($br->save(), "Save went succesful");

            my $cp_2 = $br->controlpanel;
            is_deeply(
                $cp_2->TO_JSON->{values},
                $cp->TO_JSON->{values},
                "After save we have the same object"
            );
        },
        "Get controlpanel from hash"
    );
}

sub zs_br_controlpanel_instance : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {
            my $br   = _init();
            my $fqdn = _create_fqdn;
            my $cp   = $br->controlpanel();

            $cp->shortname('bar');
            $br->controlpanel($cp);

            my $instance = $br->create_instance(
                {
                    fqdn     => $fqdn,
                    label    => $fqdn,
                    otap     => 'development',
                    template => 'foo',
                }
            );

            isa_ok($instance, "Zaaksysteem::Object::Types::Instance");
            is($instance->template, 'foo', "Template is foo");

            $br->save();


            my @uuids = @{ $br->get_instance_uuids };
            is(@uuids, 1, "One instance");

            foreach my $uuid (@uuids) {
                $self->_check_permission_for_behandelaar($uuid);

                my $instance = $br->get_instance_by_uuid($uuid);
                my $fqdn     = _create_fqdn;

                my $now = DateTime->now();

                my %args = (
                    fqdn                  => $fqdn,
                    label                 => 'Some label',
                    deleted               => 1,
                    disabled              => 1,
                    network_acl           => [],
                    provisioned_on        => $now,
                    delete_on             => $now,
                    database_provisioned  => $now,
                    filestore_provisioned => $now,
                );

                my $copy
                    = $br->update_instance($uuid, { %args, protected => 1 });
                $br->save();
                my $saved = $br->get_instance_by_uuid($uuid);
                is_deeply(
                    $saved->TO_JSON->{values},
                    $copy->TO_JSON->{values},
                    "instance updated correctly"
                );
            }

            my ($uuid) = @uuids;
            my $second = Zaaksysteem::BR::Controlpanel->new(
                schema  => $zs->schema,
                bedrijf => $br->bedrijf,
            );

            $instance = $second->get_instance_by_uuid($uuid);
            is($instance->id, $uuid, "Got the correct instance");
            isnt($instance->fqdn, $fqdn, "FQDN is not the same");
            is($instance->template, $cp->template, "Template is the same");

            $br->delete_instance($uuid);
            $br->save;
            throws_ok(
                sub {
                    $br->get_instance_by_uuid($uuid);
                },
                qr#controlpanel/instance/uuid/404#,
                "Deleted instance with uuid $uuid"
            );
        },
        "Instance logic"
    );
}

sub zs_br_controlpanel_host : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {
            my $br   = _init;
            my $fqdn = _create_fqdn;
            my $cp   = $br->controlpanel;

            my $host = $br->create_host(
                {
                    fqdn       => $fqdn,
                    label      => $fqdn,
                    api_domain => $fqdn,
                    ip         => '127.0.0.1',
                }
            );
            isa_ok($host, "Zaaksysteem::Object::Types::Host");
            is($host->id, undef, "Unsaved host object");

            $br->save();

            my @uuids = @{ $br->get_host_uuids };
            is(@uuids, 1, "One host");

            foreach my $uuid (@uuids) {
                $self->_check_permission_for_behandelaar($br->_controlpanel->id);

                my $instance = $br->get_host_by_uuid($uuid);
                my $fqdn     = _create_fqdn();

                my $now = DateTime->now();

                my %args = (
                    fqdn     => $fqdn,
                    label    => 'Some label',
                    ssl_key  => 'foo',
                    ssl_cert => 'foo',
                    disabled => 1,
                );

                my $copy = $br->update_host($uuid, {%args});

                $br->save();
                my $saved = $br->get_host_by_uuid($uuid);
                is_deeply(
                    $saved->TO_JSON->{values},
                    $copy->TO_JSON->{values},
                    "instance updated correctly"
                );
            }

            my ($uuid) = @uuids;
            my $second = Zaaksysteem::BR::Controlpanel->new(
                schema  => $zs->schema,
                bedrijf => $br->bedrijf,
            );

            $host = $second->get_host_by_uuid($uuid);
            is($host->id, $uuid, "Got the correct host");
            isnt($host->fqdn, $fqdn, "FQDN is not the same");

            $br->delete_host($uuid);
            $br->save;
            throws_ok(
                sub {
                    $br->get_host_by_uuid($uuid);
                },
                qr#controlpanel/host/uuid/404#,
                "Deleted host with uuid $uuid"
            );
        },
        "Host logic"
    );
}

sub zs_br_controlpanel_shortname : Tests {
    $zs->txn_ok(
        sub {
            my $br = _init();
            my $cp = $br->controlpanel();

            $cp->shortname('fubar');
            lives_ok(
                sub {
                    $br->assert_shortname_change($cp);
                    $br->controlpanel($cp);
                },
                "short name change without save"
            );

            $br->save;

            $cp = $br->controlpanel;

            $cp->shortname('lives');
            lives_ok(
                sub {
                    $br->assert_shortname_change($cp);
                    $br->controlpanel($cp);
                },
                "Short name can be changed"
            );
            my $fqdn = _create_fqdn;
            my $instance = $br->create_instance(
                {
                    fqdn     => $fqdn,
                    label    => $fqdn,
                    otap     => 'development',
                    template => 'foo',
                }
            );
            $br->save();
            my ($uuid) = $br->get_instance_uuids();

            $cp->shortname('die');
            throws_ok(
                sub {
                    $br->assert_shortname_change($cp);
                },
                qr#controlpanel/shortname#,
                "Unable to change shortname when an instance is found"
            );

            $br->delete_instance($uuid);

            my $host = $br->create_host(
                {
                    fqdn       => $fqdn,
                    label      => $fqdn,
                    api_domain => $fqdn,
                    ip         => '127.0.0.1',
                }
            );
            $br->save;

            $cp->shortname('fubar');
            throws_ok(
                sub {
                    $br->assert_shortname_change($cp);
                },
                qr#controlpanel/shortname#,
                "Unable to change shortname when a host is found"
            );

            ($uuid) = $br->get_host_uuids();
            $br->delete_host($uuid);
            $br->save;

            lives_ok(
                sub {
                    $br->assert_shortname_change($cp);
                    $br->controlpanel($cp);
                },
                "Short name can be changed"
            );
        },
        "Short name logic"
    );
}


1;

__END__

=head1 NAME

TestFor::General::BR::Controlpanel - Control panel business rule tester

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
