BEGIN;

ALTER TABLE object_mutation DROP CONSTRAINT object_mutation_object_uuid_fkey;
ALTER TABLE object_mutation ADD CONSTRAINT object_mutation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES object_data (uuid) ON DELETE CASCADE;

COMMIT;
