BEGIN;

ALTER TABLE searchable ADD COLUMN search_order TEXT;

UPDATE bedrijf SET search_order = search_term;
UPDATE bibliotheek_categorie SET search_order = search_term;
UPDATE bibliotheek_kenmerken SET search_order = search_term;
UPDATE bibliotheek_notificaties SET search_order = search_term;
UPDATE bibliotheek_sjablonen SET search_order = search_term;
UPDATE file SET search_order = search_term;
UPDATE natuurlijk_persoon SET search_order = search_term;
UPDATE object_bibliotheek_entry SET search_order = search_term;
UPDATE zaak SET search_order = search_term;
UPDATE zaaktype SET search_order = search_term;

COMMIT;
