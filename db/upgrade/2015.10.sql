BEGIN;
    /* db/upgrade/2015.10/1000-interface-name-length.sql */

    ALTER TABLE interface ALTER COLUMN name TYPE TEXT;

    /* db/upgrade/2015.10/1001-nobody_and_system_user.sql */

    ALTER TABLE subject ADD COLUMN nobody BOOLEAN NOT NULL DEFAULT false;
    ALTER TABLE subject ADD COLUMN system BOOLEAN NOT NULL DEFAULT false;

    UPDATE subject SET system = true WHERE username IN ('admin', 'beheerder');

    /* db/upgrade/2015.10/1002-object_relation.sql */

    CREATE TABLE object_relation (
        id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
        name TEXT NOT NULL,
        object_type TEXT NOT NULL,
        object_uuid UUID REFERENCES object_data (uuid),
        object_embedding TEXT
    );

    ALTER TABLE object_relation ADD CONSTRAINT object_relation_ref_xor_embed CHECK (
        object_uuid IS NULL != object_embedding IS NULL
    );

    /* db/upgrade/2015.10/1002-subject_types.sql */
    ALTER TABLE subject DROP CONSTRAINT subject_subject_type_check;
    ALTER TABLE subject ADD  CONSTRAINT subject_subject_type_check
        CHECK (subject_type IN ('natuurlijk_persoon', 'bedrijf', 'employee'));

    /* db/upgrade/2015.10/1003-external_templates.sql */

    ALTER TABLE bibliotheek_sjablonen ADD COLUMN interface_id INTEGER;
    ALTER TABLE bibliotheek_sjablonen ADD COLUMN template_uuid UUID;

    ALTER TABLE ONLY bibliotheek_sjablonen
        ADD CONSTRAINT bibliotheek_sjablonen_interface_id_fkey FOREIGN KEY (interface_id)
        REFERENCES interface(id);

    ALTER TABLE transaction ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
    ALTER TABLE transaction_record ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
    ALTER TABLE interface ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;

    /* db/upgrade/2015.10/1003-zaaktype_notificaties_fkey.sql */

    ALTER TABLE zaaktype_notificatie ADD CONSTRAINT zaaktype_notificatie_bibliotheek_notificaties_id_fkey FOREIGN KEY (bibliotheek_notificaties_id) REFERENCES bibliotheek_notificaties(id);

    /* db/upgrade/2015.10/1004-add_veldoptie.sql */

    ALTER TABLE bibliotheek_kenmerken DROP CONSTRAINT bibliotheek_kenmerken_value_type_check;
    ALTER TABLE bibliotheek_kenmerken ADD CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK (value_type = ANY (ARRAY['text_uc'::text, 'checkbox'::text, 'richtext'::text, 'date'::text, 'file'::text, 'bag_straat_adres'::text, 'email'::text, 'valutaex'::text, 'bag_openbareruimte'::text, 'text'::text, 'bag_openbareruimtes'::text, 'url'::text, 'valuta'::text, 'option'::text, 'bag_adres'::text, 'select'::text, 'valutain6'::text, 'valutaex6'::text, 'valutaex21'::text, 'image_from_url'::text, 'bag_adressen'::text, 'valutain'::text, 'calendar'::text, 'calendar_supersaas'::text, 'bag_straat_adressen'::text, 'googlemaps'::text, 'numeric'::text, 'valutain21'::text, 'textarea'::text, 'bankaccount'::text, 'subject'::text, 'geolatlon'::text]));

    /* db/upgrade/2015.10/1004-bibliotheek_kenmerken-public_label.sql */

    ALTER TABLE bibliotheek_kenmerken ADD COLUMN naam_public TEXT;

    /* db/upgrade/2015.10/1004-indexes.sql */
    CREATE INDEX transaction_external_transaction_id_idx ON transaction(external_transaction_id);

    /* db/upgrade/2015.10/1005-subject_types_pt2.sql */
    ALTER TABLE subject DROP CONSTRAINT subject_subject_type_check;

    UPDATE subject SET subject_type = 'person' WHERE subject_type = 'natuurlijk_persoon';
    UPDATE subject SET subject_type = 'company' WHERE subject_type = 'bedrijf';

    ALTER TABLE subject ADD  CONSTRAINT subject_subject_type_check
        CHECK (subject_type IN ('person', 'company', 'employee'));

    /* db/upgrade/2015.10/1006-stuf_config_items.sql */

    INSERT INTO config (parameter, value, advanced) VALUES ('disable_stuf_client_certificate_checks', '0', true);

    /* db/upgrade/2015.10/1007-stuf_config_items2.sql */

    INSERT INTO config (parameter, value, advanced) VALUES ('enable_stufzkn_simulator', '0', true);

    /* db/upgrade/2015.10/1021-rc1-remove_beheer_plugins.sql */

    DROP TABLE beheer_plugins;

    /* db/upgrade/2015.10/1021-rc2-indexes_for_uuids.sql */
-- Add indexes when they do not exist yet. Long but safe version.

DO $$
BEGIN

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'bibliotheek_sjablonen_interface_id_idx'
    ) THEN

    CREATE INDEX bibliotheek_sjablonen_interface_id_idx ON bibliotheek_sjablonen (interface_id);
END IF;

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'bibliotheek_sjablonen_template_uuid_idx'
    ) THEN

    CREATE INDEX bibliotheek_sjablonen_template_uuid_idx ON bibliotheek_sjablonen (template_uuid);
END IF;

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'transaction_uuid_idx'
    ) THEN

    CREATE INDEX transaction_uuid_idx ON transaction (uuid);
END IF;

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'transaction_record_uuid_idx'
    ) THEN

    CREATE INDEX transaction_record_uuid_idx ON transaction_record (uuid);
END IF;

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'interface_uuid_idx'
    ) THEN

    CREATE INDEX interface_uuid_idx ON interface (uuid);
END IF;

END$$;

    /* db/upgrade/2015.10/1022-rc3-logging_restricted.sql */

    ALTER TABLE logging ADD restricted BOOLEAN DEFAULT FALSE NOT NULL;
    CREATE INDEX logging_restricted_idx ON logging(restricted);

COMMIT;
