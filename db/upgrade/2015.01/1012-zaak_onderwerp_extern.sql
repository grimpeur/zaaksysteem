BEGIN;

ALTER TABLE zaak ADD COLUMN onderwerp_extern TEXT;
ALTER TABLE zaaktype_definitie ADD COLUMN extra_informatie_extern TEXT;

-- Dit is veilig, column type is nu varchar, casten naar text werkt altijd.
ALTER TABLE zaak ALTER COLUMN onderwerp TYPE TEXT USING onderwerp::TEXT;
ALTER TABLE zaaktype_definitie ALTER COLUMN extra_informatie TYPE TEXT USING extra_informatie::TEXT;

COMMIT;
