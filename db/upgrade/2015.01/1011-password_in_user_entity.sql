BEGIN;

ALTER TABLE user_entity ADD COLUMN password character varying(255);

COMMIT;
