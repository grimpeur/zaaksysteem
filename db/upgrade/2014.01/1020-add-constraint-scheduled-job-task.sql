BEGIN;

alter table scheduled_jobs add CONSTRAINT scheduled_jobs_task_check CHECK (((task)::text ~ '(case/update_kenmerk)'::text));

COMMIT;