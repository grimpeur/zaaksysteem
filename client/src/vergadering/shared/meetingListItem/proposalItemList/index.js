import angular from 'angular';
import template from './template.html';
import angularUiRouterModule from 'angular-ui-router';
import seamlessImmutable from 'seamless-immutable';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import rwdServiceModule from '../../../../shared/util/rwdService';
import appServiceModule from '../../appService';
import auxiliaryRouteModule from '../../../../shared/util/route/auxiliaryRoute';
import getAttributes from '../../getAttributes';
import includes from 'lodash/includes';
import isArray from 'lodash/isArray';
import isString from 'lodash/isString';
import assign from 'lodash/assign';
import shortid from 'shortid';
import get from 'lodash/get';
import find from 'lodash/find';
import sortByOrder from 'lodash/orderBy';
import first from 'lodash/first';
import merge from 'lodash/merge';
import uniq from 'lodash/uniq';
import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.meetingListItem.proposalItemList', [
			composedReducerModule,
			rwdServiceModule,
			appServiceModule,
			angularUiRouterModule,
			auxiliaryRouteModule
		])
		.directive('proposalItemList',
			[ '$sce', '$location', '$stateParams', 'dateFilter', '$state', 'composedReducer', 'rwdService', 'appService', 'auxiliaryRouteService',
			( $sce, $location, $stateParams, dateFilter, $state, composedReducer, rwdService, appService, auxiliaryRouteService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					proposals: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						attrReducer,
						columnReducer,
						sortReducer,
						proposalReducer,
						sortedProposalReducer,
						attachmentsReducer,
						smallProposalReducer,
						sort = {},
						appConfigReducer;

					appConfigReducer = composedReducer( { scope: $scope }, ctrl.appConfig )
						.reduce( config => config );

					// This reducer outputs the relevant attributes that are displayed in the proposals list column header.
					attrReducer = composedReducer( { scope: $scope }, appConfigReducer, $state)
						.reduce( ( config, state ) => {

							let attributes = getAttributes(config).voorstel || [],
								docCol = {
									external_name: 'voorstelbijlagen',
									internal_name: {
										searchable_object_id: '$proposalAttachments',
										searchable_object_label: 'Bijlagen'
									},
									checked: 1
								};

							return state.current.name === 'meetingList' ?
								[
									{
										external_name: 'voorstelnummer',
										internal_name: {
											searchable_object_id: '$index',
											searchable_object_label: ''
										},
										checked: 1
									}
								].concat(attributes)
								.concat(docCol)
								: attributes.concat(docCol);

						});

					// Outputs an object that contains the key in a proposal object that needs to be sorted on + the order.
					sortReducer = composedReducer({ scope: $scope }, ( ) => sort, attrReducer)
						.reduce( ( userSort, attributes ) => {

							let finalSort,
								sortKey;

							if (userSort.key) {
								finalSort = userSort;
							} else {

								let firstCol = find(attributes, ( attr ) => attr.external_name.indexOf('voorstel') === 0),
									key = firstCol.internal_name.searchable_object_id;

								finalSort = seamlessImmutable({
									key,
									reversed: false
								});

							}

							switch ( finalSort.key ) {
								case 'zaaknummer':
								sortKey = 'instance.number';
								break;
								case 'zaaktype':
								sortKey = 'instance.casetype.instance.name';
								break;
								case 'vertrouwelijkheid':
								sortKey = 'instance.confidentiality';
								break;
								case 'resultaat':
								sortKey = 'instance.outcome.instance.result';
								break;
								default:
								sortKey = `instance.attributes.${finalSort.key}`;
							}

							finalSort = finalSort.merge({
								resolve: sortKey
							});

							return finalSort;

						});
					
					// Outputs the columns in the table header.
					columnReducer = composedReducer({ scope: $scope }, attrReducer, sortReducer)
						.reduce( ( attributes, sortOptions ) => {

							return seamlessImmutable(attributes)
								.filter( n => n.external_name.indexOf('voorstel') === 0 && n.checked === 1)
								.map( attr => {

									let isSortedOn = sortOptions.key === get(attr, 'internal_name.searchable_object_id'),
										isSortReversed = isSortedOn && sortOptions.reversed,
										colName = attr.external_name,
										label = attr.internal_name.searchable_object_label_public || attr.internal_name.searchable_object_label,
										wideCol = false,
										mediumCol = false,
										smallCol = false,
										colIcon;

									switch (colName) {
										case 'voorstelnummer':
										label = '';
										smallCol = true;
										break;

										case 'voorstelnotitie':
										label = '';
										smallCol = true;
										break;

										case 'voorstelzaaknummer':
										label = '#';
										mediumCol = true;
										break;

										case 'voorstelonderwerp':
										wideCol = true;
										break;

										case 'voorstelaanvrager':
										label = 'Aanvrager naam';
										break;

										case 'voorstelbijlagen':
										colIcon = 'file-document';
										label = '';
										smallCol = true;
										break;
									}

									return attr.merge({
										icon: isSortReversed ?
											'chevron-up'
											: (isSortedOn ? 'chevron-down' : ''),
										classes: {
											'sorted-on': isSortedOn,
											'sort-reversed': isSortReversed
										},
										colClass: {
											'wide': wideCol,
											'medium': mediumCol,
											'small': smallCol
										},
										label,
										colIcon
									});

								});
						});
					
					ctrl.getConfigColumns = columnReducer.data;

					// In this reducer we calculate the number of relevant attachments that are present on a case.
					attachmentsReducer = composedReducer({ scope: $scope }, ctrl.proposals, appConfigReducer)
						.reduce( (proposals, config) => {

							// First we get the relevant attachment attributes that have been configured in the
							// currently active koppelprofiel.
							let attachmentAttributes = config.instance.interface_config.attribute_mapping
								.filter( attribute => attribute.external_name.indexOf('voorstelbijlage') !== -1 && attribute.internal_name )
								.map( attribute => attribute.internal_name.searchable_object_id);

							// Then we map over all proposals, calculate how many values are present for those attributes in every proposal
							return proposals.map( proposal => {

								let files = attachmentAttributes
									.map( label => {
										return proposal.instance.attributes[label] ? proposal.instance.attributes[label] : [];
									})
									.reduce( (acc, val) => {
										return acc.concat(val);
									}, []);

								// And return the original proposal object with an extra key '$proposalAttachments' stuck on.
								// This allows easier displaying of values in the fieldReducer and allows us to make use of
								// the sorting logic we already have for attribute values.
								return merge(
									{},
									proposal,
									{
										instance: {
											attributes: {
												$proposalAttachments: uniq(files).length
											}
										}
									}
								);

							});

						});

					// We use this reducer to sort proposals based on a sort object that consists of a key and an order
					sortedProposalReducer = composedReducer( { scope: $scope }, attachmentsReducer, ctrl.getConfigColumns, sortReducer, ( ) => appService.state().grouped, $stateParams)
						.reduce( ( proposals, columns, sortOptions, isGrouped, stateParams ) => {

							let sortedProposals,
								state = stateParams.meetingType === 'open' ? ['open', 'new'] : ['resolved'];

							if (!isGrouped) {
								sortedProposals = proposals.filter( proposal => {

									return includes(state, proposal.instance.status) ? proposal : null;

								});
							} else {
								sortedProposals = proposals;
							}

							if (sortOptions && sortOptions.key) {
								sortedProposals = seamlessImmutable(sortByOrder(sortedProposals,
									( proposal ) => get(proposal, sortOptions.resolve),
									sortOptions.reversed ? 'desc' : 'asc'
								));
							}

							return sortedProposals;

						});

					// We use this reducer to build the proposals list that is displayed on bigger viewports.
					proposalReducer = composedReducer({ scope: $scope }, sortedProposalReducer, ctrl.getConfigColumns )
						.reduce( ( sortedProposals, columns ) => {

							let mutableCols = columns.asMutable(),
								formattedProposals;

							formattedProposals =
								sortedProposals.asMutable().map( proposal => {

									return assign(
										{
											$id: shortid(),
											id: proposal.reference,
											casenumber: proposal.instance.number,
											casetype: proposal.instance.casetype.instance.name,
											classes: {
												closed: !!(proposal.instance.status === 'resolved')
											}
										},
										{
											values: mutableCols.map( column => {

												let columnName = column.internal_name.searchable_object_id,
													value = isArray(proposal.instance.attributes[columnName]) ? first(proposal.instance.attributes[columnName]) : proposal.instance.attributes[columnName],
													tpl;

												if ( isArray(value) ) {

													if (value.length > 1) {
														tpl = '<ul>';

														value.map( ( el ) => {
															tpl += `<li>${el}</li>`;
														});

														tpl += '</ul>';
													} else {
														tpl = `${value.join()}`;
													}

												} else if ( isString(value) && value.split(' ').length > 20 ) {

													let resultArray = value.split(' ');
													
													if (resultArray.length > 20) {
														resultArray = resultArray.slice(0, 20);
														tpl = `${resultArray.join(' ')}...`;
													}

												} else {

													tpl = String(value || '-');

												}

												switch (columnName) {

													case '$index':
													tpl = `<span class="proposal__number">${value}</span>`;
													value = Number(value);
													break;

													case 'zaaknummer':
													tpl = String(proposal.instance.number || '');
													break;

													case 'zaaktype':
													tpl = String(proposal.instance.casetype.instance.name || '-');
													break;

													case 'vertrouwelijkheid':
													tpl = String(proposal.instance.confidentiality.mapped || '-');
													break;

													case 'aanvrager_naam':
													tpl = String(`${proposal.instance.requestor.instance.subject.instance.first_names} ${proposal.instance.requestor.instance.subject.instance.surname}` || '-');
													break;

													case 'resultaat':
													tpl = String(proposal.instance.result || '-');
													break;

													case '$proposalAttachments':
													value = proposal.instance.attributes.$proposalAttachments;
													tpl = String(value);
													break;

													case 'proposalNote':
													tpl = proposal.notes ? '<i class="mdi mdi-note-plus"></i>' : '';
													break;

												}

												if (column.attribute_type === 'file') {
													tpl = `<a href="/download/${value.id}">Download</a>`;
												}

												if (includes(column.external_name, 'datum')) {
													tpl = String(dateFilter( value, 'dd MMM yyyy') || '-');
												}

												return {
													id: shortid(),
													name: columnName,
													template: $sce.trustAsHtml(tpl),
													value
												};

											})
										}
									);
								});

							return formattedProposals;
						});

					ctrl.getProposals = proposalReducer.data;

					// For smaller viewports
					smallProposalReducer = composedReducer({ scope: $scope }, sortedProposalReducer, ctrl.getConfigColumns)
						.reduce( ( proposals, columns ) => {

							return proposals.map( proposal => {

								let descriptionCol = find(columns, ( col ) => col.external_name === 'voorstelonderwerp' ),
									value = proposal.instance.attributes[descriptionCol.internal_name.searchable_object_id] || proposal.instance.casetype.instance.name || '-';

								if (isArray(value)) {
									value = first(value);
								}

								if (value.split(' ').length > 20) {
									
									let resultArray = value.split(' ');
									
									if (resultArray.length > 20) {
										resultArray = resultArray.slice(0, 20);

										value = `${resultArray.join(' ')}...`;

									}
								}

								return {
									$id: shortid(),
									id: proposal.reference,
									casenumber: proposal.instance.number,
									subject: `${proposal.instance.number}: ${value}`,
									agendapoint_number: first(proposal.instance.attributes.$index),
									number_of_documents: proposal.instance.attributes.$proposalAttachments,
									notes: proposal.notes || false,
									link: $state.href(getDetailState(), { zaakID:  proposal.instance.number }), //eslint-disable-line
									classes: {
										closed: !!(proposal.instance.status === 'resolved')
									}
								};

							});

						});

					ctrl.getSmallProposals = smallProposalReducer.data;

					// Function to sort. Modifies 'sort' variable which in turn gets picked up by sortReducer.
					ctrl.sortByColumn = ( column ) => {

						let sortOptions = sortReducer.data(),
							key = sortOptions.key,
							reversed = sortOptions.reversed,
							colId = get(column, 'internal_name.searchable_object_id');

						if (sortOptions.key === colId) {
							reversed = !reversed;
						} else {
							reversed = false;
							key = colId;
						}

						sort = seamlessImmutable({ key, reversed });
					};

					let getDetailState = ( ) => {
						return auxiliaryRouteService.append($state.current, 'proposalDetail');
					};

					ctrl.handleProposalClick = ( proposal ) => {
						$state.go(getDetailState(), { zaakID: proposal.casenumber });
					};

					ctrl.getViewSize = ( ) => {
						if ( includes( rwdService.getActiveViews(), 'small-and-down') ) {
							return 'small';
						}
						return 'wide';
					};

					ctrl.isGrouped = ( ) => appService.state().grouped;

				}],
				controllerAs: 'proposalItemList'

			};
		}
		])
		.name;
