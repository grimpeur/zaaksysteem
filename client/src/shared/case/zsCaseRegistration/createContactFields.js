export default ( ) => {
	return [
		{
			name: '$email',
			label: 'E-mailadres',
			template: 'email',
			description: 'E-mailadres van aanvrager'
		},
		{
			name: '$landline',
			label: 'Vast telefoonnummer',
			template: 'text',
			description: 'Telefoonnummer van aanvrager'
		},
		{
			name: '$mobile',
			label: 'Mobiel telefoonnummer',
			template: 'text',
			description: 'Mobiel nummer van aanvrager'
		}
	];
};
