import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import vormFieldsetModule from './../../vorm/vormFieldset';
import registrationFormCompilerModule from './registrationFormCompiler';
import seamlessImmutable from 'seamless-immutable';
import composedReducerModule from './../../api/resource/composedReducer';
import vormAllocationPickerModule from './../../zs/vorm/vormAllocationPicker';
import preventNavigationModule from './../../util/preventNavigation';
import zsCasePauseApplicationModule from './../zsCasePauseApplication';
import zsCaseReuseValuesModule from './../zsCaseReuseValues';
import snackbarServiceModule from './../../ui/zsSnackbar/snackbarService';
import ruleEngine from './../../zs/case/ruleEngine/v1';
import attrToVorm from './../../zs/vorm/attrToVorm';
import reduceSteps from './reduceSteps';
import reduceButtons from './reduceButtons';
import parseAttributeValues from './parseAttributeValues';
import formatAttributeValues from './formatAttributeValues';
import getApiValues from './getApiValues';
import formatAsApiSubject from './formatAsApiSubject';
import vormValidatorModule from './../../vorm/util/vormValidator';
import zsScrollFadeModule from './../../ui/zsScrollFade';
import vormValidateAttributes from './../../zs/case/validateAttributes/vormValidateAttributes';
import defaultMessages from './../../vorm/util/vormValidator/messages';
import getRoleValue from './../../../intern/views/case/zsCaseAddSubject/getRoleValue';
import getCaseCreateSnack from './../../../intern/getCaseCreateSnack';
import calendarModule from './../../vorm/types/calendar';
import inputModule from './../../vorm/types/input';
import selectModule from './../../vorm/types/select';
import formModule from './../../vorm/types/form';
import radioModule from './../../vorm/types/radio';
import checkboxListModule from './../../vorm/types/checkboxList';
import vormObjectSuggestModule from './../../object/vormObjectSuggest';
import textareaModule from './../../vorm/types/textarea';
import richTextModule from './../../vorm/types/richText';
import mapModule from './../../vorm/types/map';
import textblockModule from './../../vorm/types/textblock';
import appointmentModule from './../../vorm/types/appointment';
import map from 'lodash/map';
import first from 'lodash/first';
import pickBy from 'lodash/pickBy';
import keyBy from 'lodash/keyBy';
import isEqual from 'lodash/isEqual';
import mapValues from 'lodash/mapValues';
import flatten from 'lodash/flatten';
import merge from 'lodash/fp/merge';
import assign from 'lodash/fp/assign';
import get from 'lodash/get';
import findIndex from 'lodash/findIndex';
import isEmpty from 'lodash/isEmpty';
import find from 'lodash/find';
import reject from 'lodash/reject';
import omit from 'lodash/omit';
import identity from 'lodash/identity';
import includes from 'lodash/includes';
import last from 'lodash/last';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsCaseRegistration', [
		composedReducerModule,
		angularUiRouterModule,
		registrationFormCompilerModule,
		vormFieldsetModule,
		vormAllocationPickerModule,
		preventNavigationModule,
		zsCasePauseApplicationModule,
		zsCaseReuseValuesModule,
		vormValidatorModule,
		snackbarServiceModule,
		zsScrollFadeModule,
		inputModule,
		checkboxListModule,
		radioModule,
		selectModule,
		textareaModule,
		richTextModule,
		mapModule,
		calendarModule,
		textblockModule,
		appointmentModule,
		vormObjectSuggestModule,
		formModule
	])
		.component('zsCaseRegistration', {
			template,
			bindings: {
				casetype: '<',
				recipient: '<',
				requestor: '<',
				channelOfContact: '<',
				rules: '<',
				step: '<',
				values: '<',
				user: '<',
				documents: '<'
			},
			controller: [
				'$scope', '$timeout', '$document', '$state', '$http', 'composedReducer', 'registrationFormCompiler', 'preventNavigation', 'vormValidator', 'snackbarService',
				function ( scope, $timeout, $document, $state, $http, composedReducer, registrationFormCompiler, preventNavigation, vormValidator, snackbarService ) {

				let ctrl = this;

				ctrl.$onInit = ( ) => {

					let registrationPhaseReducer,
						settingsReducer,
						casePropertiesReducer,
						fieldReducer,
						defaultsReducer,
						valueReducer,
						ruleResultReducer,
						finalValueReducer,
						defaultAllocationReducer,
						values = seamlessImmutable(ctrl.values || {}),
						touched = seamlessImmutable({}),
						displayAllValidation = false,
						stepsReducer,
						validityReducer,
						displayedValidityReducer,
						currentGroupReducer,
						detailFieldReducer,
						buttonReducer,
						messagesReducer,
						lastStep,
						collapsed = true,
						submitting = false;


					let getCurrentIndex = ( ) => {
						return findIndex(stepsReducer.data(), group => group.name === currentGroupReducer.data().name);
					};

					registrationPhaseReducer = composedReducer({ scope }, ( ) => ctrl.casetype)
						.reduce(casetype => {

							let registrationPhase = first(casetype.instance.phases),
								processedFields = registrationPhase.fields.map( field => {

								// Since no magic_string is provided through backend for text_block attributes
								// (they are not first class citizens apparently) we need to stick a magic_string
								// value on it manually.
								if (field.type === 'text_block') {
									return merge(
										field,
										{
											magic_string: `text_block_${field.id}`,
											default_value: field.properties.text_content
										}
									);
								}
								return field;
							});

							return merge( registrationPhase, { fields: processedFields });
						});

					settingsReducer = composedReducer({ scope }, ( ) => ctrl.casetype)
						.reduce(casetype => casetype.instance.settings);

					casePropertiesReducer = composedReducer({ scope }, ( ) => ctrl.casetype)
						.reduce(casetype => casetype.instance.properties);

					fieldReducer = composedReducer( { scope }, registrationPhaseReducer, ( ) => ctrl.requestor)
						.reduce(( phase, requestor ) => {

							let idToNameMapping = mapValues(keyBy(phase.fields, 'id'), 'magic_string');

							return keyBy(
								seamlessImmutable(phase.fields)
									.asMutable( { deep: true } )
									.map(attr => {
										let field = attrToVorm(
											attr,
											{
												include: {
													dateRange: idToNameMapping
												}
											}
										);

										if (field.template === 'calendar') {
											field = merge(
												field,
												{
													data: {
														provider: {
															requestor: requestor.instance.old_subject_identifier
														}
													}
												}
											);
										} else if (field.template === 'text_block') {
											field = merge(
												field,
												{
													name: `text_block_${field.id}`,
													hideLabel: true
												}
											);
										} else if (field.type === 'geolatlon') {
											field = merge(
												field,
												{
													data: {
														onFeaturesSelect:( features ) => {

															let attributeId = get(attr, 'properties.map_wms_feature_attribute_id'),
																attribute = find(phase.fields, { catalogue_id: attributeId }),
																val;

															if (attribute) {
																
																if (features.length) {
																	val = last(features);
																} else {
																	val = null;
																}

																ctrl.handleChange(attribute.magic_string, val);

															}

														}
													}
												}
											);
										} else if (field.type === 'appointment') {
											field = merge(
												field,
												{
													data: {
														provider: {
															requestor: requestor.reference,
															appointmentInterfaceUuid: attr.properties.appointment_interface_uuid,
															locationId: attr.properties.location_id,
															productId: attr.properties.product_id
														}
													}
												}
											);
										} else if (field.data && field.data.options) {

											field = assign(
												field,
												{
													data: assign(field.data, {
														options: field.data.options.filter(opt => opt.active)
													})
												}
											);
										}

										return field;
									})
									.map(field => {

										let mapped = field;

										switch (field.$attribute.type) {
											case 'file':
											mapped = merge(field, {
												data: {
													target: '/api/v1/case/prepare_file',
													transform: [ '$file', '$data', ( $file, $data ) => {

														let files =
															map($data.result.instance.references, ( filename, reference ) => {
																return {
																	filename,
																	reference
																};
															});

														return files[0];

													}]
												}
											});
											break;
										}

										return mapped;

									}),
								'name'
							);
						});


					// values can only be set once, so no need to wrap it in a getter
					defaultsReducer = composedReducer({ scope }, settingsReducer, fieldReducer, registrationPhaseReducer, ( ) => ctrl.requestor, ( ) => ctrl.documents)
						.reduce(( settings, fields, registrationPhase, requestor, documents ) => {

							let allocation,
								email,
								mobile,
								landline,
								defaults;

							defaults = seamlessImmutable({})
								.merge(
									parseAttributeValues(
										fields,
										mapValues(
											keyBy(
												registrationPhase.fields.filter(field => !isEmpty(field.default_value)),
												'magic_string'
											),
											'default_value'
										)
									)
								);

							if (settings.allow_take_on_create) {
								allocation = {
									type: 'me',
									data: {
										me: true
									}
								};
							} else if (settings.allow_assign_on_create) {
								allocation = {
									type: 'org-unit',
									data: {
										unit: String(registrationPhase.route.group.instance.group_id),
										role: String(registrationPhase.route.role.instance.role_id)
									}
								};
							}

							if (settings.intake_show_contact_info && requestor) {
								email = get(requestor, 'instance.subject.instance.email_address');
								mobile = get(requestor, 'instance.subject.instance.mobile_phone_number');
								landline = get(requestor, 'instance.subject.instance.phone_number');
							}

							if (documents) {

								defaults = defaults.merge({
									$files: documents.map(
										file => {

											let originDate = get(file, 'metadata_id.origin_date', null);

											if (originDate) {
												originDate = new Date(
													...originDate.split('-')
														.map(( pt, index ) => {
															// month is zero-indexed
															return index === 1 ? pt - 1 : pt;
														})
												);
											}

											return {
												reference: file.filestore_id.uuid,
												name: file.name,
												extension: file.extension,
												origin: get(file, 'metadata_id.origin', 'Inkomend'),
												origin_date: originDate,
												description: get(file, 'metadata_id.description', '')
											};
										}
									)
								});

							}

							defaults = defaults.merge({ $allocation: allocation, $email: email, $mobile: mobile, $landline: landline, $confidentiality: 'public' });

							return defaults;

						});

					valueReducer = composedReducer({ scope }, ( ) => values)
						.reduce( vals => vals );

					ruleResultReducer = composedReducer( { scope }, fieldReducer, defaultsReducer, valueReducer, ( ) => ctrl.rules, ( ) => ctrl.channelOfContact, ( ) => ctrl.requestor, casePropertiesReducer)
						.reduce(( fieldsByName, defaults, vals, rules, channelOfContact, requestor, casetypeProperties ) => {

							let attrValues = parseAttributeValues(fieldsByName, defaults.merge(vals));

							return ruleEngine(
								rules,
								{
									attributes: attrValues,
									channel_of_contact: channelOfContact,
									requestor,
									casetypeProperties
								}
							);

						});

					defaultAllocationReducer = composedReducer( { scope }, ruleResultReducer, registrationPhaseReducer)
						.reduce(( ruleResult, registrationPhase ) => {

							return {
								type: 'org-unit',
								data: ruleResult.allocation ?
									ruleResult.allocation
									: {
										unit: String(registrationPhase.route.group.instance.group_id),
										role: String(registrationPhase.route.role.instance.role_id)
									}
							};

						});

					composedReducer({ scope, mode: 'hot' }, fieldReducer, ruleResultReducer)
						.reduce(( fieldsByName, ruleResult ) => {

							let prefill =
								formatAttributeValues(fieldsByName, ruleResult.prefill);

							return ruleResult.allocation ?
								assign(
									prefill,
									{
										$allocation:{
											type: 'org-unit',
											data: ruleResult.allocation
										}
									}
								)
								: prefill;

						})
						.subscribe(( prefill, previous = { }) => {

							// If there is a difference between the prefilled values object
							// and the previously state of the values object AND the currently
							// calculated prefilled values object is bigger than the previous
							// state, then update values object.

							if (
								!isEqual(prefill, previous)
								&& (Object.keys(prefill).length > Object.keys(previous).length)
							) {

								values = values.merge(
									pickBy(
										assign(prefill, previous),
										( value, name ) => prefill[name] !== previous[name]
									)
								);
							}

						});

					finalValueReducer = composedReducer( { scope }, fieldReducer, valueReducer, ruleResultReducer, defaultsReducer)
						.reduce(( fieldsByName, vals, ruleResult, defaults ) => {

							let merged,
								resultValues;
								
							resultValues = pickBy(ruleResult.values, ( value, key ) => {
								return !!ruleResult.disabled[key];
							});

							merged = defaults.merge(vals)
								.merge(
									formatAttributeValues(
										fieldsByName,
										pickBy(ruleResult.values, ( value, key ) => {
											return !!ruleResult.disabled[key];
										})
									)
								);

							map(resultValues, ( value, key) => {
								values = values.merge({ [key]: value });
							});

							return merged;

						});

					stepsReducer = composedReducer({ scope }, registrationPhaseReducer, settingsReducer, fieldReducer, ( ) => ctrl.requestor, ( ) => ruleResultReducer.data().hidden, ( ) => ruleResultReducer.data().disabled, ( ) => ruleResultReducer.data().pause_application, ( ) => ctrl.step, ( ) => lastStep, ( ) => ctrl.documents)
						.reduce( ( ...rest ) => {

							return reduceSteps(...rest, $state);

						});

					currentGroupReducer = composedReducer( { scope }, stepsReducer, ( ) => ctrl.step)
						.reduce( ( steps, selectedStep ) => {
							let group;

							group = selectedStep ?
								steps.filter(step => step.name === selectedStep)[0]
								: steps[0];

							return group;
						});

					validityReducer = composedReducer( { scope }, currentGroupReducer, finalValueReducer)
						.reduce(( group, vals ) => {

							let messages =
								assign(
									defaultMessages,
									{
										telephone: 'Er is geen geldig telefoonnummer ingevuld (10 t/m 15 cijfers en + is toegestaan)'
									}
								);

							let validation = vormValidateAttributes(
								vormValidator,
								flatten(map(group.fieldsets, 'fields')),
								vals,
								messages,
								null,
								( field, value, v ) => {

									let empty = isEmpty(value);

									switch (field.name) {
										case '$landline':
										case '$mobile':
										if (!empty
											&& !(/^\+?\d{10,15}$/.test(value))) {
											v.telephone = messages.telephone || true;
										}

										break;
									}

									return v;

								}
							);

							return validation;

						});

					displayedValidityReducer = composedReducer( { scope }, validityReducer, ( ) => touched, ( ) => displayAllValidation)
						.reduce( ( validity, touchedFields, displayValidation ) => {

							let validations = validity.validations;

							if (!displayValidation) {
								validations = mapValues(
									validations,
									( validationList, name ) => {
										return !touched[name] ?
											validationList.map(validation => {
												return omit(validation, 'required');
											})
											: validationList;
									}
								);
							}

							return validations;

						});

					buttonReducer = composedReducer( { scope }, stepsReducer, currentGroupReducer, ( ) => submitting)
						.reduce(( steps, currentGroup, isSubmitting ) => {

							return reduceButtons(steps, currentGroup)
								.map(button => {
									return isSubmitting ?
										assign(button, { disabled: true })
										: button;
								});

						});

					detailFieldReducer = composedReducer( { scope }, ( ) => ctrl.requestor, ( ) => ctrl.recipient, ( ) => ctrl.channelOfContact, ( ) => ctrl.documents)
						.reduce(( requestor, recipient, channelOfContact, documents ) => {

							return [
								{
									name: 'requestor',
									label: 'Aanvrager',
									value: get(requestor, 'instance.display_name', 'Geen')
								},
								{
									name: 'recipient',
									label: 'Ontvanger',
									value: get(recipient, 'instance.display_name', 'Geen')
								},
								{
									name: 'channel_of_contact',
									label: 'Contactkanaal',
									value: channelOfContact
								},
								get(documents, 'length') ?
								{
									name: 'document',
									// leave singular for now, once multiple documents are supported, change to plural
									label: 'Document',
									value: documents.map(document => `${document.name}${document.extension}`).join(', ')
								}
								: null
							].filter(identity);

						});

					messagesReducer = composedReducer({ scope }, finalValueReducer, ( ) => ctrl.step)
						.reduce( ( vals, step ) => {

							let messages;

							messages = step === 'bevestig' ?
								map(vals.$files, file => {

									return get(vals[file.case_document], 'reference') === file.reference ?
										null
										: {
											icon: 'alert-circle',
											type: 'warning',
											name: file.reference,
											label: `Let op: het bestand ${file.name} is niet gekoppeld aan een zaakdocument. Het komt wel in de documentenlijst terecht.`
										};

								}).filter(identity)
								: [];

							return messages;

						});

					ctrl.loadPreviousValues = ( caseValues ) => {
						if (caseValues) {
							values = values.merge(caseValues);
	
							$timeout( ( ) => {
								snackbarService.info('De waarden van de vorige zaak zijn ingevuld. U kunt nu wijzingen aanbrengen.');
							}, 2500);

						}
					};

					ctrl.handleChange = ( name, value ) => {

						let valueToMerge = value;

						if (
							name === '$allocation'
							&& get(valueToMerge, 'type') === 'org-unit'
							&& get(finalValueReducer.data(), '$allocation.type') !== 'org-unit'
						) {
							valueToMerge = defaultAllocationReducer.data();
						}

						if (name === '$files') {

							values = values.merge(
								mapValues(
									keyBy(
										value.map(
											file => {

												return {
													name: file.case_document,
													file: {
														filename: file.name + file.extension,
														reference: file.reference
													}
												};
											}
										),
										'name'
									),
									'file'
								)
							);
						} else if (
							includes(
								map(values.$files, 'case_document'),
								name
							)
						) {
							values = values.merge({
								$files: values.$files.map(
									file => {

										if (file.case_document === name) {
											return assign(file, { case_document: null });
										}

										return file;

									}
								)
							});
						}

						values = values.merge({ [name]: valueToMerge });

						touched = touched.merge({ [name]: true });

						// remove hidden fields from touched object

						touched = seamlessImmutable(
							pickBy(touched.asMutable(), ( val, key ) => !ruleResultReducer.data().hidden[key])
						);

						// set last step to current to prevent issues w/ changed data and disappearing steps

						lastStep = ctrl.getCurrentGroup().name;

					};

					ctrl.submit = ( ) => {

						let vals = ctrl.getValues(),
							data,
							assignee;

						assignee = (!vals.$allocation || vals.$allocation.type === 'org-unit' || !get(vals, '$allocation.data.me', true) ) ?
							null
							: {
								type: 'assignee',
								subject: {
									type: 'subject',
									reference: vals.$allocation.type === 'me' ?
										ctrl.user.uuid
										: vals.$allocation.data.uuid
								},
								role: 'Behandelaar',
								magic_string_prefix: 'behandelaar'
							};

						data = {
							casetype_id: ctrl.casetype.reference,
							source: ctrl.channelOfContact,
							requestor: formatAsApiSubject(ctrl.requestor),
							values: getApiValues(
								pickBy(vals, ( value, key) => !!fieldReducer.data()[key]),
								fieldReducer.data()
							),
							open: get(vals.$allocation, 'type') === 'me',
							route: get(vals.$allocation, 'type') === 'org-unit' ?
								{
									group_id: Number(vals.$allocation.data.unit),
									role_id: Number(vals.$allocation.data.role)
								}
								: ( get(vals.$allocation, 'type') === 'coworker' && get(vals.$allocation, 'data.changeDept') ) ?
								{
									group_id: Number(vals.$allocation.data.route),
									role_id: Number(vals.$allocation.data.role)
								}
								: null,
							files: map(
								vals.$files,
								( file ) => {

									let original = find(ctrl.documents, d => d.filestore_id.uuid === file.reference);

									return {
										reference: file.reference,
										name: file.name,
										metadata: assign(original.metadata_id, {
											origin: file.origin,
											origin_date: file.origin_date ?
												`${file.origin_date}`
												: null,
											description: file.description
										})
									};

								}
						),
							subjects: map(
								vals.$related_subjects,
								( subject ) => {

									let type;

									type = {
										natuurlijk_persoon: 'person',
										bedrijf: 'company'
									}[subject.related_subject.type];

									return {
										subject: {
											type,
											reference: subject.related_subject.data.uuid
										},
										role: getRoleValue(subject),
										magic_string_prefix: subject.magic_string_prefix,
										pip_authorized: subject.pip_authorized,
										send_auth_notification: subject.notify_subject
									};
								}
							).concat(
								assignee
							).filter(identity),
							confidentiality: vals.$confidentiality
						};

						if (ctrl.recipient) {
							data = merge(
								data,
								{
									recipient: formatAsApiSubject(ctrl.recipient)
								}
							);
						}

						if (vals.$landline || vals.$mobile || vals.$email ) {
							data = merge(
								data,
								pickBy({
									phone_number: vals.$landline,
									mobile_number: vals.$mobile,
									email_address: vals.$email
								}, identity)
							);
						}

						submitting = true;

						snackbarService.wait(
							'Uw zaak wordt geregistreerd',
							{
								promise: $http({
									url: '/api/v1/case/create',
									method: 'POST',
									data
								}),
								catch: ( ) => 'Zaak kon niet worden aangemaakt. Neem contact op met uw beheerder voor meer informatie',
								then: ( response ) => {

									let assigneeReference = get(response, 'data.result.instance.assignee.reference'),
										caseNumber = get(response, 'data.result.instance.number'),
										status = get(response, 'data.result.instance.status'),
										userIsAssignee;

									userIsAssignee = assigneeReference === ctrl.user.uuid;

									touched = seamlessImmutable({});

									if (userIsAssignee) {
										$state.go('case', { caseId: caseNumber });
									} else {
										$state.go('home');
									}

									return getCaseCreateSnack($state, { caseNumber, userIsAssignee, status });

								}
							}
						)
							.catch(( ) => {
								submitting = false;
							});

					};

					ctrl.cancel = ( ) => {
						$state.go('home');
					};

					ctrl.goToFirstInvalid = ( ) => {

						let field,
							validations = ctrl.getValidations(),
							el;

						displayAllValidation = true;

						field = find(
							flatten(map(ctrl.getCurrentGroup().fieldsets, 'fields')),
							( f ) => {

								let validation = validations[f.name];

								return reject(validation, isEmpty).length > 0;
							}
						);

						el = $document[0].querySelector(`[data-name="${field.name}"]`);

						el.scrollIntoView();

					};

					ctrl.advance = ( ) => {

						let index = getCurrentIndex();

						if (!ctrl.isValid() && !ctrl.skipRequired) {

							ctrl.goToFirstInvalid();

						} else if (index === ctrl.getSteps().length - 1) {
							ctrl.submit();
						} else {
							$state.go($state.current.name, { step: ctrl.getSteps()[index + 1].name }, { inherit: true });
						}

					};

					ctrl.back = ( ) => {

						let index = getCurrentIndex();

						if (index === 0) {
							ctrl.cancel();
						} else {
							$state.go($state.current.name, { step: ctrl.getSteps()[index - 1].name }, { inherit: true });
						}


					};

					ctrl.handleButtonClick = ( name ) => {

						let el = $document[0].querySelector('[name="case-register-top"]');

						$timeout( ( ) => {
							el.scrollIntoView();
						}, 0);

						displayAllValidation = false;

						if (name === 'next') {
							ctrl.advance();
						} else {
							ctrl.back();
						}

					};

					ctrl.getValues = finalValueReducer.data;

					ctrl.getAttrValues = composedReducer({ scope }, fieldReducer, finalValueReducer)
						.reduce( (fieldsByName, vals ) => {

							return seamlessImmutable(
								pickBy(vals, ( value, key ) => !!fieldsByName[key])
							);

						}).data;

					ctrl.getSteps = stepsReducer.data;

					ctrl.getButtons = buttonReducer.data;

					ctrl.getCurrentGroup = currentGroupReducer.data;

					ctrl.getValidations = ( ) => get(validityReducer.data(), 'validations');

					ctrl.getDisplayedValidations = displayedValidityReducer.data;

					ctrl.isValid = ( ) => get(validityReducer.data(), 'valid', false);

					ctrl.canSkip = ( ) => includes(get(ctrl.user, 'capabilities'), 'case_registration_allow_partial') || includes(get(ctrl.user, 'capabilities'), 'admin');

					ctrl.getDetailFields = detailFieldReducer.data;

					ctrl.getMessages = messagesReducer.data;

					ctrl.isCollapsed = ( ) => collapsed;

					ctrl.toggleCollapse = ( ) => {
						collapsed = !collapsed;
					};

					ctrl.getFields = fieldReducer.data;

					ctrl.getPauseApplicationData = ( ) => ctrl.getCurrentGroup().paused;

					preventNavigation(scope, ( event, to, toParams ) => {
						let shouldPrevent = !isEmpty(touched.asMutable())
							&& !toParams.ignoreUnsavedChanges;

						return shouldPrevent;
					});

					currentGroupReducer.subscribe( currentGroup => {

						// set last step to current step if greater than last step
						if (
							getCurrentIndex()
							> findIndex(ctrl.getSteps(), group => group.name === lastStep)
						) {
							lastStep = currentGroup.name;
						}
						
					});
					
				};

				ctrl.getCompiler = ( ) => registrationFormCompiler;

			}]
		})
		.name;
