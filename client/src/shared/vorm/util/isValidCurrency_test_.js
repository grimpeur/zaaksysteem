import { expect } from 'chai';
import { isValidCurrency } from './isValidCurrency';

describe('The `isValidCurrency` function', () => {
	it('expects a string argument', () => {
		[ undefined, null, true, false, 1, {}, [], () => {} ].forEach(value => {
			expect(isValidCurrency(value)).to.equal(false);
		});
	});

	it('does not accept a point as decimal mark', () => {
		expect(isValidCurrency('1.1')).to.equal(false);
	});

	it('does not accept a trailing comma', () => {
		expect(isValidCurrency('1,')).to.equal(false);
	});

	it('accepts integers', () => {
		expect(isValidCurrency('1')).to.equal(true);
	});

	it('accepts one or two digits after the decimal mark', () => {
		expect(isValidCurrency('1,1')).to.equal(true);
		expect(isValidCurrency('1,11')).to.equal(true);
});

	it('does not accept more than two digits after the decimal mark', () => {
		expect(isValidCurrency('1,115')).to.equal(false);
	});
});
