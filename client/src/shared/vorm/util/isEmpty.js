export default ( value ) => {
	return value === ''
		|| value === null
		|| value === undefined
		|| (typeof value === 'number' && isNaN(value));
};
