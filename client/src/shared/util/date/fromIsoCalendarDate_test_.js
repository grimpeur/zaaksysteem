import { expect } from 'chai';
import toIsoCalendarDate from './toIsoCalendarDate';
import fromIsoCalendarDate from './fromIsoCalendarDate';

/**
 * @test {fromIsoCalendarDate}
 */
describe('The `fromIsoCalendarDate` function', () => {
	it('returns a `Date` object', () => {
		const date = fromIsoCalendarDate('2000-01-01');

		expect(date).to.be.an.instanceof(Date);
	});

	it('is complementary to the `toIsoCalendarDate` function', () => {
		const dateString = '2000-01-01';
		const dateObject = fromIsoCalendarDate(dateString);

		expect(toIsoCalendarDate(dateObject)).to.equal(dateString);
	});
});
