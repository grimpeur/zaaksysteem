import { expect } from 'chai';
import getYearMonthNumber from './getYearMonthNumber';

/**
 * @test {getYearMonthNumber}
 */
describe('The `getYearMonthNumber` function', () => {
	it('returns a `Number`', () => {
		const now = new Date();

		expect(getYearMonthNumber(now)).to.be.a('number');
	});

	it('adds the actual month number', () => {
		const first = getYearMonthNumber(new Date(2000, 8, 1));
		const second = getYearMonthNumber(new Date(2000, 9, 1));

		expect(first).to.equal(200009);
		expect(second).to.equal(200010);
	});

	it('compares dates as numbers', () => {
		const first = getYearMonthNumber(new Date(2000, 8, 1));
		const second = getYearMonthNumber(new Date(2000, 9, 1));

		expect(second > first).to.equal(true);
	});
});
