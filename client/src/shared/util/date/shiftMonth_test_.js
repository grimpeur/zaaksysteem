import { expect } from 'chai';
import toIsoCalendarDate from './toIsoCalendarDate';
import shiftMonth from './shiftMonth';

describe('The `shiftMonth` function', () => {
	it('shifts to the last day of a future month', () => {
		const testDate = new Date('2017-12-31');
		const offsetDate = shiftMonth(testDate, 2);

		expect(toIsoCalendarDate(offsetDate)).to.equal('2018-02-28');
	});

	it('shifts to the last day of a past month', () => {
		const testDate = new Date('2018-01-31');
		const offsetDate = shiftMonth(testDate, -2);

		expect(toIsoCalendarDate(offsetDate)).to.equal('2017-11-30');
	});
});
