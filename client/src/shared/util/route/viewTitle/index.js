import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import isArray from 'lodash/isArray';
import get from 'lodash/get';
import find from 'lodash/find';
import getActiveStates from './../getActiveStates';

export default
	angular.module('viewTitle', [
		angularUiRouter
	])
		.factory('viewTitle', [ '$rootScope', '$document', '$injector', '$state', ( $rootScope, $document, $injector, $state ) => {

			let currentTitle;

			let getTitle = ( ) => {

				let current = $state.$current,
					tree = getActiveStates(current),
					stateWithTitle = find(tree.concat().reverse(), state => !!state.title),
					title = get(stateWithTitle, 'title', '');

				if (typeof title === 'function' || isArray(title)) {
					title = $injector.invoke(title, null, current.locals.globals);
				}

				return title;

			};

			$rootScope.$on('$stateChangeSuccess', ( ) => {
				currentTitle = getTitle();
				$document.find('title').text(currentTitle);
			});

			return {
				get: ( ) => currentTitle
			};

		}])
		.name;
