import angular from 'angular';
import propCheck from './../../../util/propCheck';
import merge from 'lodash/merge';
import assign from 'lodash/assign';
import result from 'lodash/result';
import mapValues from 'lodash/mapValues';
import pickBy from 'lodash/pickBy';
import identity from 'lodash/identity';
import get from 'lodash/get';

let templates = {
	'natuurlijk_persoon_description': '{{object.adres_id.straatnaam}} {{object.adres_id.huisnummer || \'\'}} {{object.adres_id.huisletter || \'\'}} {{object.adres_id.huisnummertoevoeging || \'\'}}, {{object.adres_id.postcode}} {{object.adres_id.woonplaats}}'
};

export default
	angular.module('shared.object.zsObjectSuggest.objectSuggestionService', [
	])
		.factory('objectSuggestionService', [ '$interpolate', ( $interpolate ) => {

			templates = mapValues(templates, ( tpl ) => {
				return tpl
					.replace(/{{/g, $interpolate.startSymbol())
					.replace(/}}/g, $interpolate.endSymbol());
			});

			let transformers =
				{
					natuurlijk_persoon: ( item ) => {

						let birthDate = item.object.geboortedatum ? ` (${item.object.geboortedatum})` : '',
							description = $interpolate(templates.natuurlijk_persoon_description, false, null, true)(pickBy(item, identity));

						return {
							label: item.object.decorated_name + birthDate,
							description,
							link: `/betrokkene/${item.object.id}/?gm=1&type=natuurlijk_persoon`
						};
					},
					bedrijf: ( item ) => {

						let description =
							[
								[ 'straatnaam', 'huisnummer', 'huisnummertoevoeging', 'huisletter' ],
								[ 'postcode' ],
								[ 'woonplaats' ],
								[ 'adres_buitenland1' ],
								[ 'adres_buitenland2' ],
								[ 'adres_buitenland3' ]
							].map(
								section => {
									return section
										.map(key => item.object[`vestiging_${key}`])
										.filter(identity).join(' ');
								}
							)
								.filter(identity)
								.join(', ');

						return {
							label: item.object.handelsnaam,
							description,
							link: `/betrokkene/${item.object.id}/?gm=1&type=bedrijf`
						};

					},
					case: ( item ) => {

						return {
							id: get(item, 'object.case_id'),
							label: `${item.object.id}: ${item.object.zaaktype_node_id.titel}`,
							description: get(item, 'object.description', ''),
							link: `/intern/zaak/${item.object.id}`,
							type: 'case'
						};
					},
					zaak: ( item ) => transformers.case(item),
					casetype: ( item ) => {

						return {
							id: get(item, 'object.casetype_id'),
							label: get(item, 'object.zaaktype_node_id.titel'),
							description: get(item, 'object.description', ''),
							data: merge({}, item.object, {
								values: {
									trigger: result(item, 'object.zaaktype_node_id.trigger')
								}
							}),
							type: 'casetype'
						};
					},
					zaaktype: ( item ) => transformers.casetype(item),
					file: ( item ) => {
						return {
							link: `/intern/zaak/${item.object.case_id}/documenten/`
						};
					},
					attribute: ( item ) => {

						return {
							id: item.id,
							label: item.label,
							data: item,
							type: 'attribute'
						};

					},
					defaults: ( item ) => {

						return {
							id: item.id,
							name: item.id,
							label: item.label,
							type: item.object_type,
							description: '',
							data: item.object,
							link: `/object/${item.id}`
						};
					}
				};

			let handlers = {
				casetype: {
					request: ( query/*, type*/ ) => {
						
						if (query.length < 3) {
							return null;
						}

						return {
							url: '/objectsearch/',
							params: {
								query,
								object_type: 'zaaktype'
							}
						};
					}
				},
				case: {
					request: ( query/*, type*/ ) => {

						if (!query) {
							return null;
						}

						return {
							url: '/objectsearch/',
							params: {
								query,
								object_type: 'zaak'
							}
						};

					}
				},
				objecttypes: {
					request: ( query/*, type*/ ) => {
						let params = {};

						if (query) {
							params.query = query;
						} else {
							params.all = 1;
						}

						return {
							url: '/objectsearch/objecttypes',
							params
						};
					}
				},
				object: {
					request: ( query ) => {

						return query ?
							{
								url: '/objectsearch/objects',
								params: {
									query
								}
							}
							: null;

					}
				},
				medewerker: {
					request: ( query/*type*/ ) => {

						if (query.length < 3) {
							return null;
						}

						return {
							url: '/objectsearch/contact/medewerker',
							params: {
								query
							}
						};
					}
				},
				bag: {
					request: ( query ) => {

						if (!query) {
							return null;
						}

						return {
							url: '/objectsearch/bag',
							params: {
								query
							}
						};

					}
				},
				'bag-street': {
					request: ( query ) => {

						if (!query) {
							return null;
						}

						return {
							url: '/objectsearch/bag-street',
							params: {
								query
							}
						};

					}
				},
				attribute: {
					request: ( query ) => {

						if (!query) {
							return null;
						}

						return {
							url: '/objectsearch/attributes',
							params: {
								query
							}
						};

					}
				},
				defaults: {
					request: ( query, type ) => {

						if (query.length < 3) {
							return null;
						}

						return {
							url: '/objectsearch',
							params: {
								query,
								object_type: type
							}
						};
					},
					reduce: ( data, type ) => {

						return (data || [])
							.map(( item ) => {
								let transformedItem,
									transformer = transformers[item.object_type || type];

								transformedItem = transformers.defaults(item);

								if (transformer) {
									transformedItem = assign(transformedItem, transformer(item));
								}

								return transformedItem;
							});

					}
				}
			};

			let getHandler = ( type ) => {
				return handlers[type] || handlers.defaults;
			};

			return {
				getRequestOptions: ( preferredQuery, type ) => {

					let handler,
						opts,
						query = preferredQuery;

					propCheck.throw(
						propCheck.shape({
							query: propCheck.string.optional,
							type: propCheck.string.optional
						}),
						{ query, type }
					);

					handler = getHandler(type);

					if (!query) {
						query = '';
					}

					opts = (typeof handler.request === 'function') ?
						handler.request(query, type)
						: handlers.defaults.request(query, type);

					return opts;

				},
				reduce: ( data, type ) => {

					let handler,
						res;

					propCheck.throw(
						propCheck.shape({
							type: propCheck.string.optional,
							data: propCheck.any.optional
						}),
						{ data, type }
					);

					handler = getHandler(type);

					res = (typeof handler.reduce === 'function') ?
						handler.reduce(data, type)
						: handlers.defaults.reduce(data, type);

					return res;
				}
			};

		}])
		.name;
