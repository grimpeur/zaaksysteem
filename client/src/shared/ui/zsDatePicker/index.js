import angular from 'angular';
import once from 'lodash/once';

export default
	angular.module('zsDatePicker', [
	])
		.directive('zsDatePicker', [ '$q', 'dateFilter', ( $q, dateFilter ) => {

			let load = once(
				( ) => {
					return $q( ( resolve ) => {
						require(
							[ 'pikaday', './getDatepickerOptions', './styles.scss' ],
							( ...rest ) => {
								resolve(rest);
							});
					});
				}
			);

			return {
				restrict: 'A',
				require: 'ngModel',
				link: ( scope, element, attrs, ngModel ) => {

					let picker;

					let setDatePicker = ( value ) => {

						let val = value || null;

						if (picker && dateFilter(picker.getDate(), 'dd-MM-yyyy') !== dateFilter(val, 'dd-MM-yyyy')) {
							picker.setDate(val, true);
						}

					};

					let oldValue = '';

					ngModel.$parsers.unshift(( val ) => {
						let value = val;

						if (val && val.match(/^\d{1,2}-\d{1,2}-\d{4}$/)) {

							let values = val.split('-');

							value = new Date(values[2], values[1] - 1, values[0]);

							value = dateFilter(value, 'yyyy-MM-dd');
						}

						if (value !== oldValue) {
							oldValue = value;
							setDatePicker(value);
						}

						return value;
					});

					ngModel.$formatters.unshift(( val ) => {
						let value = val ?
							dateFilter(val, 'dd-MM-yyyy')
							: val;

						return value;
					});

					load().then(( libs ) => {
						const [ Pikaday, getDatePickerOptions ] = libs;
						const options = getDatePickerOptions(element[0]);

						picker = new Pikaday(options);

						if (ngModel.$modelValue) {
							setDatePicker(ngModel.$modelValue);
						}

					});

				}
			};

		}])
		.name;
