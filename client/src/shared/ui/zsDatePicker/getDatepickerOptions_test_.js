import { expect } from 'chai';
import getDatepickerOptions from './getDatepickerOptions';

// NB: just use date
const toDateObject = dateString => new Date(dateString);

/**
 * @test {getDatepickerOptions}
 */
describe('The `getDatepickerOptions` function', () => {

	describe('uses the `min` property of its first parameter', () => {
		it('to set `minDate` and `defaultDate` to undefined if it is not set', () => {
			const options = getDatepickerOptions({
				max: '9999-12-31'
			}, toDateObject);

			expect(options.minDate).to.equal(undefined);
			expect(options.defaultDate).to.equal(undefined);
		});

		it('to set `minDate` to a date if it is set', () => {
			const options = getDatepickerOptions({
				min: '1900-01-01',
				max: '9999-12-31'
			}, toDateObject);

			expect(options.minDate).to.be.an.instanceof(Date);
		});

		it('to set `defaultDate` to `undefined` if it is in the `minDate`/`maxDate` range', () => {
			const date = new Date();
			const nextYear = String(date.getFullYear() + 1);
			const prevYear = String(date.getFullYear() - 1);
			const options = getDatepickerOptions({
				min: `${prevYear}-01-01`,
				max: `${nextYear}-01-01`
			}, toDateObject);

			expect(options.defaultDate).to.equal(undefined);
		});

		it('to set `defaultDate` to `minDate` if `minDate` is in a future month', () => {
			const nextYear = ((new Date()).getFullYear() + 1);
			const options = getDatepickerOptions({
				min: `${nextYear}-01-01`,
				max: `${nextYear}-01-31`
			}, toDateObject);

			expect(options.defaultDate).to.equal(options.minDate);
		});

		it('to set `defaultDate` to `maxDate` if `maxDate` is in a past month', () => {
			const prevYear = ((new Date()).getFullYear() - 1);
			const options = getDatepickerOptions({
				min: `${prevYear}-01-01`,
				max: `${prevYear}-01-31`
			}, toDateObject);

			expect(options.defaultDate).to.equal(options.maxDate);
		});

	});

});
