import { expect } from 'chai';
import { weekdaysShort } from './i18nConstants';

/**
 * @test {weekdaysShort}
 */
describe('The `weekdaysShort` constant', () => {
	it('is an array that is mapped from the first two letters of each `weekdays` entry',
		() => {
			expect(weekdaysShort[0]).to.equal('Zo');
			expect(weekdaysShort[1]).to.equal('Ma');
			expect(weekdaysShort[2]).to.equal('Di');
			expect(weekdaysShort[3]).to.equal('Wo');
			expect(weekdaysShort[4]).to.equal('Do');
			expect(weekdaysShort[5]).to.equal('Vr');
			expect(weekdaysShort[6]).to.equal('Za');
		});
});
