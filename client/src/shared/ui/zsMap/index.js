import angular from 'angular';
import sessionServiceModule from './../../user/sessionService';
import userSettingsModule from './../../user/userSettings';
import composedReducerModule from './../../api/resource/composedReducer';
import resourceModule from './../../api/resource';
import zsDropdownMenuModule from './../zsDropdownMenu';
import snackbarServiceModule from './../zsSnackbar/snackbarService';
import shortid from 'shortid';
import get from 'lodash/get';
import first from 'lodash/head';
import each from 'lodash/each';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import omitBy from 'lodash/omitBy';
import assign from 'lodash/assign';
import values from 'lodash/values';
import defaults from 'lodash/defaults';
import map from 'lodash/map';
import find from 'lodash/find';
import debounce from 'lodash/debounce';
import flatten from 'lodash/flatten';
import take from 'lodash/take';
import isEqual from 'lodash/isEqual';
import takeRight from 'lodash/takeRight';
import includes from 'lodash/includes';
import once from 'lodash/once';
import keys from 'lodash/keys';
import pickBy from 'lodash/pickBy';
import identity from 'lodash/identity';
import componentTemplate from './template.html';
import popupTemplate from './popup-template.html';
import adjustEngine from 'adjust-engine';
import seamlessImmutable from 'seamless-immutable';
import measureLayerFactory from './measureLayerFactory';
import filter from 'lodash/filter';
import './styles.scss';

export default
	angular.module('zsMap', [
		sessionServiceModule,
		userSettingsModule,
		composedReducerModule,
		zsDropdownMenuModule,
		resourceModule,
		snackbarServiceModule
	])
		.directive('zsMap', [ '$rootScope', '$q', '$window', '$document', '$timeout', 'sessionService', 'userSettings', 'composedReducer', 'resource', 'snackbarService', ( $rootScope, $q, $window, $document, $timeout, sessionService, userSettings, composedReducer, resource, snackbarService ) => {

			const PROJECTION = 'EPSG:28992';

			let load = once(
				( ) => {

					let userResource,
						settingResource,
						capabilitiesResource,
						mapSettingsResource;

					userResource = sessionService.createResource($rootScope);

					settingResource = userSettings.createResource($rootScope);

					capabilitiesResource = resource(
							{
								url: 'https://geodata.nationaalgeoregister.nl/wmts/brtachtergrondkaart?request=GetCapabilities',
								// CORS request gets denied if custom headers or credentials are set
								headers: {
									'X-Client-Type': null
								},
								withCredentials: false
							},
							{
								scope: $rootScope,
								cache: {
									every: 24 * 60 * 60 * 1000
								}
							}
						);

					mapSettingsResource = resource(
							( ) => {
							return userResource.data() ?
								'/api/v1/map/ol_settings'
								: null;
							},
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => first(data));


					return $q.all([
						$q( ( resolve/*, reject*/ ) => {

							$rootScope.$evalAsync( ( ) => {
								require( [ 'openlayers', 'proj4' ], ( ...rest ) => {
									resolve(rest);
								});
							});

						}),
						settingResource.asPromise().then( ( ) => settingResource),
						mapSettingsResource.asPromise().then(( ) => mapSettingsResource),
						capabilitiesResource.asPromise().then(( ) => capabilitiesResource)
					]);
				}
			);

			return {
				restrict: 'E',
				template: componentTemplate,
				scope: {
					markers: '&',
					onMapClick: '&',
					onMarkerClick: '&',
					enableMouseWheel: '&',
					popupMode: '&',
					center: '&',
					zoom: '&',
					availableOptions: '&',
					markerStyle: '&',
					featureLayers: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						loading = true,
						id = shortid().replace(/^\d+/, '');

					ctrl.isLoading = ( ) => loading;
					ctrl.getId = ( ) => id;

					load()
						.then(( args ) => {
							return $timeout(angular.noop)
								.then(( ) => args);
						})
						.then( ( args ) => {

							let [ libs, settingResource, mapSettingsResource, capabilitiesResource ] = args,
								OpenLayers = libs[0],
								proj4 = libs[1],
								layerReducer,
								featureLayerReducer,
								layersById = {},
								currentLayers = {},
								layerVisibilityReducer,
								buttonReducer,
								initialPromise,
								centerReducer,
								markerReducer,
								markerSource,
								markerStyle,
								mapComponent,
								interactions,
								highlighted = [],
								measureLayer,
								popupLayer = angular.element(element[0].querySelector('.popup-layer')),
								settings = seamlessImmutable({ fullscreen: false, measureLine: false, measureLayer: false });

							let getHighlighted = ( ) => {
								return ctrl.popupMode() === 'always' ?
									markerSource.getFeatures()
									: highlighted;
							};

							let positionPopups = ( ) => {
								let adjuster = adjustEngine({
										attachment: 'bottom center',
										target: 'top center',
										flip: true,
										offset: {
											x: 0,
											y: -25
										}
									});

								each(getHighlighted(), ( feature, index ) => {

									let popup = popupLayer.children().eq(index),
										point = mapComponent.getPixelFromCoordinate(feature.getGeometry().getCoordinates()),
										pos,
										el = element[0].querySelector(`#${id}`),
										rect = el.getBoundingClientRect(),
										left,
										top;

									if (point) {
										left = rect.left + point[0];
										top = rect.top + point[1];
									}

									pos = adjuster(popup[0].getBoundingClientRect(), { left, top, width: 0, height: 0, right: left, bottom: top }, { left: 0, top: 0, width: $window.innerWidth, height: $window.innerHeight, right: $window.innerWidth, bottom: $window.innerHeight });

									popup.css({
										top: `${pos.top}px`,
										left: `${pos.left}px`
									});

								});
							};

							layerReducer = composedReducer({ scope, compare: true, mode: 'hot' }, mapSettingsResource)
								.reduce( ( mapSettings ) => {

									let availableLayers =
										get(
											mapSettings,
											'instance.wms_layers',
											[
												{
													instance: {
														active: true,
														id: 'gemeenten',
														label: 'Gemeentegrenzen',
														url: 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms'
													}
												},
												{
													instance: {
														active: true,
														id: 'provincies',
														label: 'Provinciegrenzen',
														url: 'https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms'
													}
												},
												{
													instance: {
														active: true,
														id: 'pand',
														label: 'BAG-objecten',
														url: 'https://geodata.nationaalgeoregister.nl/bag/wms'
													}
												}
											]
										)
											.filter(layer => layer.instance.active);

									return keyBy(availableLayers, layer => layer.instance.label);

								});

							featureLayerReducer = composedReducer( { scope }, ( ) => layersById, layerReducer, ctrl.featureLayers)
								.reduce( ( indexedLayers, layers, featureLayerIds ) => {

									return map(featureLayerIds, featureLayer => {

										let matchingLayer = find(layers, layer => layer.instance.id === featureLayer);

										return matchingLayer ?
											indexedLayers[matchingLayer.instance.label]
											: null;

									}).filter(identity);

								});

							layerVisibilityReducer = composedReducer({ scope, compare: true, mode: 'hot' }, settingResource, ( ) => layersById, ( ) => settings, featureLayerReducer)
								.reduce( ( currentSettings, layers, localSettings, featureLayers ) => {

									let layerSettings =
										defaults(
											{},
											get(first(currentSettings), 'pdokSettings', {}),
											{
												Markers: true,
												'Nationaal Register': true,
												Measure: localSettings.measureLine || localSettings.measureArea
											}
										);

									return mapValues(layers, ( value, key ) => {
										
										let isAlwaysVisible = includes(featureLayers, value);

										return isAlwaysVisible || !!layerSettings[key];
									});
								});

							buttonReducer = composedReducer({ scope }, layerVisibilityReducer, ( ) => settings, ctrl.availableOptions(), featureLayerReducer)
								.reduce( ( layerVisibility, currentSettings, options, featureLayers ) => {

									let buttons = [
										{
											id: shortid(),
											name: 'area',
											type: 'button',
											icon: 'crop-free',
											label: 'Meet oppervlakte',
											click: ( ) => {

												settings = settings.merge( { measureArea: !settings.measureArea, measureLine: false });
												
											},
											style: {
												active: settings.measureArea
											}
										},
										{
											id: shortid(),
											name: 'ruler',
											type: 'button',
											icon: 'ruler',
											label: 'Meet afstanden',
											click: ( ) => {

												settings = settings.merge( { measureArea: false, measureLine: !settings.measureLine });

											},
											style: {
												active: settings.measureLine
											}
										},
										{
											id: shortid(),
											name: 'fullscreen',
											type: 'button',
											icon: currentSettings.fullscreen ?
												'fullscreen-exit'
												: 'fullscreen',
											label: currentSettings.fullscreen ?
												'Verlaat volledige weergave'
												: 'Volledige weergave',
											click: ( ) => {
												settings = settings.merge({ fullscreen: !settings.fullscreen });
												$timeout( ( ) => {

													mapComponent.updateSize();

												}, 0, false);

											}
										},
										{
											id: shortid(),
											name: 'layers',
											type: 'dropdown',
											icon: 'layers',
											label: 'Kaartlagen',
											options: map(layerVisibility, ( value, key ) => {

												let isAlwaysVisible = includes(featureLayers, value);

												return {
													name: key,
													label: key,
													iconClass: value ?
														'check'
														: '',
													disabled: isAlwaysVisible,
													click: ( ) => {
														userSettings.set(
															'pdokSettings',
															assign(
																{},
																userSettings.get('pdokSettings'),
																{
																	[key]: !value
																}
															)
														);
													}
												};

											})
										}

									];

									return options ? filter(buttons, ( button ) => options.indexOf(button.name) > -1 ) : buttons;

								});

							ctrl.getButtons = buttonReducer.data;

							ctrl.isFullscreen = ( ) => !!settings.fullscreen;

							let capabilities = capabilitiesResource.data(),
								projection,
								format,
								parsedCapabilities,
								layerOptions;

							markerStyle =
								new OpenLayers.style.Style({
									text: new OpenLayers.style.Text({
										text: '\uf34E',
										font: `normal normal normal ${ctrl.markerStyle() ? get(ctrl.markerStyle(), 'size') : '26'}px/1 "Material Design Icons"`,
										textBaseline: 'Bottom',
										fill: new OpenLayers.style.Fill({
											color: ctrl.markerStyle() ? get(ctrl.markerStyle(), 'fill') : '#e60000' // default is red
										}),
										stroke: new OpenLayers.style.Stroke({
											color: ctrl.markerStyle() ? get(ctrl.markerStyle(), 'stroke') : '#ff9999', // default slightly lighter red
											width: ctrl.markerStyle() ? get(ctrl.markerStyle(), 'width') : 1
										})
									}),
									image: new OpenLayers.style.Circle({
										radius: 5,
										stroke: new OpenLayers.style.Stroke({
											color: '#000'
										}),
										fill: new OpenLayers.style.Fill({
											color: '#fff'
										})
									})
								});

							format = new OpenLayers.format.WMTSCapabilities();

							parsedCapabilities = format.read(capabilities);

							// from https://oegeo.wordpress.com/2008/05/20/note-to-self-the-one-and-only-rd-projection-string/
							proj4.defs(
								PROJECTION,
								'+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.999908 +x_0=155000 +y_0=463000 +ellps=bessel +units=m +towgs84=565.2369,50.0087,465.658,-0.406857330322398,0.350732676542563,-1.8703473836068,4.0812 +no_defs'
							);

							OpenLayers.proj.setProj4(proj4);

							projection = OpenLayers.proj.get(PROJECTION);
							
							let boundingBox =
								find(
									parsedCapabilities.Contents.Layer,
									{ Identifier: 'brtachtergrondkaart' }
								).WGS84BoundingBox;

							projection.setExtent(
								flatten(
									proj4(PROJECTION, take(boundingBox, 2))
										.concat(proj4(PROJECTION, takeRight(boundingBox, 2)))
								)
							);

							markerSource = new OpenLayers.source.Vector({
								features: []
							});

							measureLayer = measureLayerFactory(OpenLayers);

							layerOptions =
								OpenLayers.source.WMTS.optionsFromCapabilities(
									format.read(capabilities),
									{
										layer: 'brtachtergrondkaart',
										matrixSet: PROJECTION,
										format: 'image/png',
										isBaseLayer: true
									}
								);

							assign(
								layersById,
								{
									'Nationaal Register':
										new OpenLayers.layer.Tile({
											projection,
											extent: projection.getExtent(),
											source: new OpenLayers.source.WMTS(layerOptions)
										}),
									Markers:
										new OpenLayers.layer.Vector({
											source: markerSource
										}),
									Measure: measureLayer.getLayer()
								}
							);


							centerReducer = composedReducer( { scope, mode: 'subscription' }, ctrl.center, mapSettingsResource, ctrl.markers)
								.reduce(( preferredCenter, mapSettings, markers ) => {

									let center;

									if (markers && markers.length === 1) {
										center = [ get(first(markers), 'coordinates.lng'), get(first(markers), 'coordinates.lat') ];
									} else {
										center = preferredCenter
											|| get(mapSettings, 'instance.map_center', '52.278,5.163').split(',').reverse();
									}

									return center;

								});

							mapComponent = new OpenLayers.Map({
								target: id,
								logo: false,
								interactions: OpenLayers.interaction.defaults({ mouseWheelZoom: ctrl.enableMouseWheel() || false }),
								layers: values(layersById),
								view: new OpenLayers.View({
									projection,
									center: proj4(PROJECTION, centerReducer.data()),
									zoom: ctrl.zoom() ? ctrl.zoom() : 6,
									extent: projection.getExtent()
								})
							});

							centerReducer.subscribe( center => {

								if (mapComponent) {
									mapComponent.getView().setCenter(proj4(PROJECTION, center));
								}

							});

							measureLayer.setMapComponent(mapComponent);

							interactions = [ 'pointerMove', 'singleClick' ]
								.map(( type ) => {

									return new OpenLayers.interaction.Select({
										condition: OpenLayers.events.condition[type],
										addCondition: ( ) => {

											let shouldHandle =
												type === 'singleClick' ?
													ctrl.popupMode() === 'click'
													: !!(!ctrl.popupMode() || ctrl.popupMode() === 'hover');

											return !shouldHandle;

										},
										layers: [ layersById.Markers ]
									});

								});

							interactions.forEach(interaction => {
								
								mapComponent.addInteraction(interaction);

								interaction.on('select', event => {

									if (!isEqual(highlighted, event.selected)) {
										scope.$evalAsync(( ) => {
											highlighted = event.selected;
										});
									}

								});

							});

							layerReducer.subscribe( layers => {

								layersById = omitBy(layersById, ( value, key ) => !!currentLayers[key]);

								each(currentLayers, ( layer => {
									mapComponent.removeLayer(layer);
								}));

								currentLayers = mapValues(layers, ( layerData ) => {

									return new OpenLayers.layer.Tile({
										source: new OpenLayers.source.TileWMS({
											url: layerData.instance.url,
											matrixSet: PROJECTION,
											params: {
												LAYERS: layerData.instance.id,
												TILED: true,
												transparent: true
											},
											serverType: 'mapserver'
										})
									});

								});

								each(currentLayers, layer => {

									mapComponent.addLayer(layer);

									layer.setVisible(false);

								});

								layersById = assign({}, layersById, currentLayers);


							});

							layerVisibilityReducer.subscribe( visibility => {

								each(visibility, ( visible, layerId ) => {

									let layer = layersById[layerId];

									layer.setVisible(visible);

									if (layerId === 'Measure') {

										if (visible) {
											measureLayer.enable();
										} else {
											measureLayer.disable();
										}
									}

								});

							});

							composedReducer( { scope, mode: 'hot' }, ( ) => settings)
								.subscribe( ( ) => {

									measureLayer.setMeasureMode(settings.measureLine ? 'line' : 'area');

								});

							let getFeatureURL = ( pixel ) => {

								let coord = mapComponent.getCoordinateFromPixel(pixel);

								return featureLayerReducer.data()
									.map(layer => {

										let source = layer.getSource(),
											url,
											layerId = keys(pickBy(layersById, l => l === layer))[0],
											config = layerReducer.data()[layerId];

										url = source.getGetFeatureInfoUrl(
											coord,
											mapComponent.getView().getResolution(),
											PROJECTION,
											{ INFO_FORMAT: 'text/xml' }
										);

										return {
											layer: config,
											url
										};

									})
									.filter(identity);

							};

							mapComponent.on('singleclick', ( event ) => {

								let coord = mapComponent.getCoordinateFromPixel(event.pixel),
									latLng = proj4(PROJECTION).inverse(coord).reverse();

								if (!mapComponent.hasFeatureAtPixel(event.pixel)) {

									if (!measureLayer.isEnabled()) {

										// prevent measure clicks changing the marker

										scope.$evalAsync(( ) => {

											// causes vorm/types/map to set the value for
											// this field, which in turns causes ctrl.value
											// to be set, which causes the markerReducer
											// to trigger which sets the marker.
											ctrl.onMapClick({
												$lat: latLng[0],
												$lng:latLng[1]
											});

										});
									}

								} else {
									ctrl.onMarkerClick({ $lat: latLng[0], $lng: latLng[1], $event: event });
								}
								
							});

							mapComponent.on('postrender', positionPopups);
							mapComponent.on('change:size', positionPopups);

							$window.addEventListener('resize', ( ) => {
								debounce(( ) => {
									mapComponent.updateSize();
								}, 250, { leading: true, trailing: true });
							});

							// Uncertain as to why highlighted is a dependency for this reducer. Leads to side effects when hovering over marker it seems.
							markerReducer = composedReducer({ scope, compare: true, mode: 'hot' }, ctrl.markers, ctrl.popupMode, initialPromise, /*( ) => highlighted,*/ layerVisibilityReducer)
								.reduce( markers => {

									return markers;

								});

							markerReducer.subscribe( markers => {

								let features,
									toAdd = markers || [],
									center = markers && markers.length === 1 ? [ get(first(markers), 'coordinates.lng'), get(first(markers), 'coordinates.lat') ] : null;

								if (!layerVisibilityReducer.data().Markers) {
									toAdd = [];
								}
								if (center) {
									mapComponent.getView().setCenter(proj4(PROJECTION, center));
								}

								interactions.forEach(( interaction ) => {
									interaction.getFeatures().clear();
								});

								markerSource.clear();

								features = toAdd.map( marker => {

									let coordinate =
											proj4(PROJECTION, [ marker.coordinates.lng, marker.coordinates.lat ]),
										feature = new OpenLayers.Feature(
											assign(
												{
													geometry: new OpenLayers.geom.Point(coordinate)
												},
												marker
											)
										);

									feature.setStyle(markerStyle);

									return feature;

								});
							
								markerSource.addFeatures(features);

								interactions.forEach(( interaction ) => {
									interaction.getFeatures().extend(features);
								});

								if (markers && markers.length === 1) {

									// Need to wrap this in a timeout to avoid side effects with
									// values not being set due to previously scheduled onMapClicks
									$timeout( ( ) => {

										// Zoom in to point
										mapComponent.getView().setZoom(10);

										// Since we've zoomed and centered the map on the current marker,
										// we can attempt to get a feature for the center of the map
										let featureURL = getFeatureURL([element[0].clientWidth / 2, element[0].clientHeight / 2]);

										if (featureURL) {
											ctrl.onMapClick({
												$lat: center[1],
												$lng: center[0],
												$features: featureURL
											});
										}

									}, 0);

								}

							});

							composedReducer( { scope, mode: 'hot' }, markerReducer, ctrl.popupMode, ( ) => highlighted)
								.subscribe( ( ) => {

									while (popupLayer.children().length) {
										popupLayer.children().eq(0).remove();
									}

									getHighlighted().forEach(( feature, index ) => {

										let popup,
											properties = feature.getProperties(),
											marker = markerReducer.data()[index];


										popup = marker.template ?
											angular.element(marker.template)
											: angular.element(popupTemplate);

										popup[0].querySelector('.map-popup-label').textContent = properties.title;
										popup[0].querySelector('.map-popup-description').innerHTML = get(properties, 'description', '');

										popupLayer.append(popup);

									});

									positionPopups();

								});

							let onKeyUp = ( event ) => {

								if (event.keyCode === 27 && ctrl.isFullscreen()) {

									event.stopPropagation();

									scope.$evalAsync( ( ) => {

										settings = settings.merge({ fullscreen: false });

									});

								}

							};

							$document.bind('keyup', onKeyUp);

							scope.$on('$destroy', ( ) => {

								if (mapComponent) {
									mapComponent.setTarget(null);
								}

								$document.unbind('keyup', onKeyUp);

							});


					})
					.catch( ( err ) => {

						console.error(err);

						snackbarService.error('Er ging iets fout bij het laden van de map. Neem contact op met uw beheerder voor meer informatie.');

					})
					.finally( ( ) => {

						loading = false;

					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
