import angular from 'angular';
import immutable from 'seamless-immutable';
import resourceModule from '../../../api/resource/index.js';
import { mockObject } from './../../../object/mock';
import zqlEscapeFilterModule from './../../../object/zql/zqlEscapeFilter';
import get from 'lodash/get';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import assign from 'lodash/assign';

export default
	angular.module('shared.ui.zsSpotEnlighter.savedSearchesService', [
			resourceModule,
			zqlEscapeFilterModule
		])
		.factory('savedSearchesService', [ '$rootScope', 'resource', 'zqlEscapeFilter', ( $rootScope, resource, zqlEscapeFilter ) => {

			let service = {},
				predefined;

			let zqlKeywords = [ 'SELECT', 'FROM', 'MATCHING', 'WHERE', 'NUMERIC ORDER BY', 'ORDER BY' ];

			predefined =
				immutable(
					[
						{
							id: 'mine',
							label: 'Mijn openstaande zaken'

						},
						{
							id: 'my-department',
							label: 'Mijn afdeling'
						},
						{
							id: 'all',
							label: 'Alle zaken'
						},
						{
							id: 'intake',
							label: 'Zaakintake'
						}
					]
						.map(( item ) => {
							return immutable(
								mockObject(
									'v0',
									{
										id: item.id,
										label: item.label,
										type: 'saved_search',
										values: {
											public: true,
											title: item.label,
											query: {
												predefined: true,
												objectType: {
													label: 'Zaak',
													id: 1,
													object_type: 'case'
												},
												zql: item.zql
											}
										}
									}
								)
							);
						})
				);

			let convertLegacyTemplate = ( str ) => {
				return str.replace(/<\[/g, '{{::').replace(/\]>/g, '}}');
			};

			// TODO: move parse to zqlParser

			let parse = ( zql ) => {

				let result =
					mapValues(
						keyBy(zqlKeywords),
						( key ) => {

							let match = zql.match(new RegExp(`${key}\\s+(.*?)\\s*(${zqlKeywords.join('|')}|$)`));

							if (match) {
								return match[1];
							}
							return '';
						}
					);

				return result;

			};

			let parseLegacyFilter = ( item ) => {
				return item.merge({
					values: {
						query: JSON.parse(convertLegacyTemplate(item.values.query))
					}
				}, { deep: true });
			};

			service.getRequestOptions = ( query ) => {
				let zql = 'SELECT {} FROM saved_search';

				if (query) {
					zql += ` MATCHING ${zqlEscapeFilter(query)}`;
				}

				return {
					url: '/api/object/search/',
					params: {
						zql
					}
				};
			};

			service.getZql = ( search, options = { }) => {

				let userData = options.user,
					zql,
					parts = {
						SELECT: '{}',
						FROM: options.from || search.values.query.objectType.object_type,
						WHERE: '',
						MATCHING: '',
						'ORDER BY': '',
						'NUMERIC ORDER BY': ''
					},
					searchZql = get(search, 'values.query.zql');

				if (searchZql) {
					parts = assign(parts, parse(searchZql));
				}

				if (options.columns) {
					parts.SELECT = options.columns.join(', ');
				}

				switch (search.id) {
					case 'mine':
					parts.WHERE = `(case.assignee.id = ${zqlEscapeFilter(userData.instance.logged_in_user.id)}) AND (case.status = "open")`;
					break;

					case 'my-department':
					parts.WHERE = `(case.route_ou = ${zqlEscapeFilter(get(userData, 'instance.logged_in_user.legacy.ou_id'))})`;
					break;

					case 'intake':
					parts.FROM += ' intake';
					break;
				}

				if ('matching' in options) {
					if (options.matching) {
						parts.MATCHING = zqlEscapeFilter(options.matching);
					} else {
						delete parts.MATCHING;
					}
				}

				if (options.sort) {
					let { by, order, type } = options.sort,
						del,
						add;

					if (by && order) {
						del = add = 'ORDER BY';
						if (type === 'numeric') {
							add = `NUMERIC ${add}`;
						} else {
							del = `NUMERIC ${del}`;
						}

						parts[add] = `${by} ${order.toUpperCase()}`;
						delete parts[del];
					}
				}

				zql = zqlKeywords.reduce( ( query, keyword ) => {

					let reduced = query;

					if (parts[keyword]) {
						reduced += `${keyword} ${parts[keyword]} `;
					}

					return reduced;

				}, '').trim();

				return zql;

			};

			service.getPredefinedSearches = ( ) => predefined;

			service.convertLegacyTemplate = convertLegacyTemplate;

			service.filter = ( data, query ) => {
				return predefined
					.filter( ( item ) => {
						return !query || item.label.toLowerCase().indexOf(query.toLowerCase()) !== -1;
					})
					.concat(
					(data || [])
						.map(parseLegacyFilter)
					);
			};

			service.parseLegacyFilter = parseLegacyFilter;

			return service;
		}])
		.name;
