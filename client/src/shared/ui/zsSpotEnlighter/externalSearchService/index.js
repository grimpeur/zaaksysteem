import angular from 'angular';
import first from 'lodash/head';
import get from 'lodash/get';
import resourceModule from '../../../api/resource/index.js';

export default
	angular.module('shared.ui.zsSpotEnlighter.externalSearchService', [
			resourceModule
		])
		.factory('externalSearchService', [ '$rootScope', '$timeout', 'resource', ( $rootScope, $timeout, resource ) => {

			let service = {},
				apiResource,
				conditionsMet,
				tokenRefresh = 3600 * 1000;

			apiResource = resource(
				{ url: '/api/v1/sysin/interface/get_by_module_name/next2know' },
				{ scope: $rootScope, cache: { every: tokenRefresh } }
			)
				.reduce( ( requestOptions, data ) => {

					return get(first(data), 'instance.interface_config');

				});

			apiResource.subscribe( ( data ) => {
				$timeout(( ) => {
					apiResource.reload();
				}, get(data, 'token.expires_in') * 1000);
			});

			// The ZS Backend will return a token for Next2Know even if the currently logged in ZS user is not known in
			// Next2Know. As a result this function will return true even if the currently logged in ZS user can't actually
			// search Next2Know. We solve this in zsSpotEnlighter/index.js.
			// More information: ZS-14874.
			conditionsMet = ( ) => apiResource.state() === 'resolved' && apiResource.data() !== undefined && apiResource.data().token !== null;

			service.getApiInfo = ( ) => {

				if (
					apiResource.state() === 'resolved'
					&& apiResource.data() !== undefined
				) {
					return apiResource.data();
				}

				return null;
			};

			// Search by keyword
			service.getRequestOptions = ( query ) => {

				if (
					query
					&& conditionsMet()
				) {

					return {
						url: `${apiResource.data().endpoint}/documents/_all/search`,
						params: {
							q: query,
							size: 50
						},
						headers: {
							Authorization: `Bearer ${apiResource.data().token.access_token}`,
							'X-Client-Type': undefined
						},
						withCredentials: false,
						timeout: 5000
					};
				}

				return null;
			};

			// Get single search result
			service.getDocumentRequestOptions = ( searchIndex, documentId ) => {

				if (
					searchIndex
					&& documentId
					&& conditionsMet()
				) {

					return {
						url: `${apiResource.data().endpoint}/documents/${searchIndex}/${documentId}`,
						headers: {
							Authorization: `Bearer ${apiResource.data().token.access_token}`,
							'X-Client-Type': undefined
						},
						withCredentials: false,
						timeout: 5000
					};
				}

				return null;
			};

			// Download a file from next2know
			service.getDownloadRequestOptions = ( filename ) => {

				if (
					filename
					&& conditionsMet()
				) {
					
					return {
						url: `${apiResource.data().endpoint}/files/${filename}`,
						headers: {
							Authorization: `Bearer ${apiResource.data().token.access_token}`,
							Accept: 'application/octet-stream',
							'X-Client-Type': undefined
						},
						withCredentials: false,
						responseType: 'arraybuffer',
						timeout: 5000
					};
				}

				return null;
			};

			// Get a thumbnail from next2know
			service.getDocumentThumbRequestOptions = ( searchIndex, documentId ) => {

				if (
					searchIndex
					&& documentId
					&& conditionsMet()
				) {
					
					return {
						url: `${apiResource.data().endpoint}/thumbnails/${searchIndex}/${documentId}`,
						headers: {
							Authorization: `Bearer ${apiResource.data().token.access_token}`,
							'X-Client-Type': undefined
						},
						withCredentials: false,
						timeout: 5000
					};
				}

				return null;
			};

			return service;
		}])
		.name;
