import angular from 'angular';
import composedReducerModule from './../../../api/resource/composedReducer';
import zsKeyboardNavigableListModule from './../../zsKeyboardNavigableList';
import controller from './../../zsSuggestionList/controller';
import suggestionTemplate from './suggestion-template.html';
import groupBy from 'lodash/groupBy';
import flatten from 'lodash/flatten';
import map from 'lodash/map';
import uniq from 'lodash/uniq';
import take from 'lodash/take';
import get from 'lodash/get';
import partition from 'lodash/partition';
import template from './template.html';

export default
	angular.module('shared.ui.zsSpotEnlighter.zsSpotEnlighterSuggestionList', [
		zsKeyboardNavigableListModule,
		composedReducerModule
	])
		.directive('zsSpotEnlighterSuggestionList', [ '$window', 'composedReducer', ( $window, composedReducer ) => {

			let el = angular.element(template)[0];

			angular.element(el.querySelector('.suggestion')).replaceWith(suggestionTemplate);

			return {
				restrict: 'E',
				template: el.outerHTML,
				scope: {
					keyInputDelegate: '&',
					suggestions: '&',
					onSelect: '&',
					label: '@',
					objectType: '&',
					externalSearchSettings: '&',
					externalSearchResultsCount: '&',
					onObjectTypeChange: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						groupReducer,
						loading = false,
						resultTypesReducer;

					controller.call(ctrl, $scope);

					resultTypesReducer = composedReducer({ scope: $scope }, ctrl.externalSearchSettings(), ctrl.externalSearchResultsCount )
						.reduce( ( externalSearchSettings, externalResultsTotalCount ) => {

							return [
								{
									name: 'internal',
									label: 'Gevonden in Zaaksysteem'
								},
								{
									name: 'external',
									label: get(externalSearchSettings, 'active') ? `Gevonden in externe bronnen (${externalResultsTotalCount})` : get(externalSearchSettings, 'errorMessage')
								}];
						});

					// we don't use ctrl.objectType() as a dependency for the reducer
					// because we want the groups to change only when the data does
					groupReducer = composedReducer( { scope: $scope }, ctrl.suggestions, resultTypesReducer, ctrl.externalSearchResultsCount)
						.reduce(( suggestions, resultTypes, externalResultsTotalCount ) => {

							let grouped,
								groups,
								groupTypes;

							grouped = groupBy(suggestions, 'type');

							groups = uniq(flatten(map(suggestions, 'type')))
								.map( type => {

									let items = grouped[type],
										truncated =
											ctrl.objectType() === 'all'
											&& items.length > 3
											&& type !== 'saved_search'
											&& type !== 'external-document';

									if (truncated) {
										items = take(items, 3);
									}

									return {
										type,
										items,
										truncated,
										click: ( ) => {

											loading = true;

											groupReducer.subscribe(( ) => {

												loading = false;

											}, { once: true });

											ctrl.onObjectTypeChange({ $type: type });
										}
									};

								});

							groupTypes = partition(groups, ( group ) => group.type !== 'external-document' );

							return resultTypes.map( (resultType, index) => {
								return {
										name: resultType.name,
										label: groupTypes[1].length ?
											resultType.label
											: '',
										groups: groupTypes[index],
										moreResults: !!(resultType.name === 'external' && externalResultsTotalCount > 50)
									};
							});

						});

					ctrl.getGroups = groupReducer.data;

					ctrl.isLoading = ( ) => loading;

					ctrl.handleActionClick = ( suggestion, action, event ) => {
						action.click(event);
						event.stopPropagation();
					};

					ctrl.openInNewWindow = ( link, event ) => {
						$window.open(link, '_new');
						event.preventDefault();
						event.stopPropagation();
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
