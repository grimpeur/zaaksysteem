import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './index.html';
import resourceModule from '../../../shared/api/resource';
import seamlessImmutable from 'seamless-immutable';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import externalSearchServiceModule from '../../../shared/ui/zsSpotEnlighter/externalSearchService';
import externaldocumentViewModule from './externaldocumentView';
import get from 'lodash/get';

export default
	angular.module('Zaaksysteem.intern.externaldocument.route', [
		angularUiRouterModule,
		resourceModule,
		snackbarServiceModule,
		externalSearchServiceModule,
		externaldocumentViewModule
	])
		.config([ '$stateProvider', '$urlMatcherFactoryProvider', ( $stateProvider, $urlMatcherFactoryProvider ) => {

			$urlMatcherFactoryProvider.strictMode(false);

			$stateProvider
				.state('externaldocument', {
					url: '/externaldocument/:next2knowSearchIndex/:next2knowdocumentId',
					template,
					controllerAs: 'vm',
					controller: [ '$scope', 'externalDocument', 'externalDocumentThumb', ( $scope, externalDocument, externalDocumentThumb ) => {

						$scope.document = externalDocument.data();
						$scope.thumbnail = externalDocumentThumb.data();

					}],
					resolve: {
						externalDocument: [ '$rootScope', '$stateParams', 'resource', '$q', 'externalSearchService', 'snackbarService', ( $rootScope, $stateParams, resource, $q, externalSearchService, snackbarService ) => {

							let externalDocumentResource = resource(
								( ) => {

									return externalSearchService.getDocumentRequestOptions($stateParams.next2knowSearchIndex, $stateParams.next2knowdocumentId);

								},
								{ scope: $rootScope, cache: { disabled: true } }
							)
								.reduce(( requestOptions, data ) => {

									return get(data, 'data') || seamlessImmutable([]);
								});

							return externalDocumentResource
								.asPromise()
								.then( ( ) => externalDocumentResource)
								.catch( ( err ) => {

									snackbarService.error('Het externe document kon niet geladen worden.');

									return $q.reject(err);
								});
						}],
						externalDocumentThumb: [ '$rootScope', '$stateParams', 'resource', '$q', 'externalDocument', 'externalSearchService', ( $rootScope, $stateParams, resource, $q, externalDocument, externalSearchService ) => {

							let externalDocumentThumbResource = resource(
								( ) => {

									return externalSearchService.getDocumentThumbRequestOptions($stateParams.next2knowSearchIndex, $stateParams.next2knowdocumentId);

								},
								{ scope: $rootScope, cache: { disabled: true } }
							)
								.reduce(( requestOptions, data ) => {

									return data !== null ? `data:image/png;base64,${get(data, 'data')}` : null;
								});

							return externalDocumentThumbResource
								.asPromise()
								.then( ( ) => {

									return externalDocumentThumbResource;

								});

						}]
					},
					title: [ ( ) => 'Extern zoekresultaat' ]
				});
		}])
		.name;
