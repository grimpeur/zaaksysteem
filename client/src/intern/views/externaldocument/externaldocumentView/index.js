import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import snackbarServiceModule from '../../../../shared/ui/zsSnackbar/snackbarService';
import externalSearchServiceModule from '../../../../shared/ui/zsSpotEnlighter/externalSearchService';
import get from 'lodash/get';
import './styles.scss';

export default
		angular.module('Zaaksysteem.intern.externaldocument.externalDocumentView', [
			composedReducerModule,
			snackbarServiceModule,
			externalSearchServiceModule
		])
		.directive('externalDocumentView', [ '$sce', '$document', '$window', '$http', 'composedReducer', 'externalSearchService', 'snackbarService', ( $sce, $document, $window, $http, composedReducer, externalSearchService, snackbarService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					document: '&',
					thumbnail: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						fieldReducer,
						mimeTypes = {
							doc: 'application/msword',
							docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
							dotx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
							gif: 'image/gif',
							jpg: 'image/jpeg',
							html: 'text/html',
							pdf: 'application/pdf',
							png: 'image/png',
							ppt: 'application/vnd.ms-powerpoint',
							pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
							rtf: 'application/rtf',
							tif: 'image/tiff',
							tiff: 'image/tiff',
							txt: 'text/plain',
							xls: 'application/vnd.ms-excel',
							xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
					};

					fieldReducer = composedReducer({ scope: $scope }, ctrl.document() )
						.reduce( doc => {

							// TMLO Fields:

							return [
								{
									label: 'Identificatiekenmerk',
									name: 'id',
									value: get(doc, 'Identificatiekenmerk') || '-'
								},
								{
									label: 'Aggregatieniveau',
									name: 'aggregationLevel',
									value: get(doc, 'Aggregatieniveau') || '-'
								},
								{
									label: 'Naam',
									name: 'name',
									value: get(doc, 'Identificatiekenmerk') || '-'
								},
								{
									label: 'Classificatie',
									name: 'classification',
									value: get(doc, 'Classificatie') || '-'
								},
								{

									label: 'Omschrijving',
									name: 'description',
									value: get(doc, 'Omschrijving') || '-'
								},
								{
									label: 'Eventplan',
									name: 'eventPlan',
									value: get(doc, 'Eventplan') || '-'
								},
								{
									label: 'Relatie',
									name: 'relation',
									value: get(doc, 'Relatie') || '-'
								},
								{
									label: 'Vertrouwelijkheid',
									name: 'confidentiality',
									value: get(doc, 'Vertrouwelijkheid') || '-'
								},
								{
									label: 'Openbaarheid',
									name: 'accessibility',
									value: get(doc, 'Openbaarheid') || '-'
								},
								{
									label: 'Vorm',
									name: 'form',
									value: get(doc, 'Vorm') || '-'
								},
								{
									label: 'Formaat',
									name: 'format',
									value: get(doc, 'Formaat') || '-'
								},
								{
									label: 'Fulltext',
									name: 'fulltext',
									value: get(doc, 'fulltext') || '-'
								},
								{
									label: 'Filename',
									name: 'filename',
									value: get(doc, 'Filename') || '-'
								}
							];

						});

					ctrl.getFields = fieldReducer.data;

					ctrl.getImage = ( ) => ctrl.thumbnail();

					ctrl.isDownloadable = ( ) => !!(get(ctrl.document(), 'Filename'));

					ctrl.downloadDocument = ( ) => {
						
						let filename = get(ctrl.document(), 'Filename'),
							ext = filename.split('.').pop();

						event.preventDefault();

						snackbarService.wait(
							'Het bestand wordt gedownload',
								{
									promise:
										$http(externalSearchService.getDownloadRequestOptions(filename))
										.then( response => {

											let URL = 'URL' in $window ? $window.URL : $window.webkitURL,
												file = new Blob([response.data], { type: mimeTypes[ext] }),
												blobUrl = URL.createObjectURL(file),
												anchor = angular.element('<a></a>');

											anchor.attr('href', blobUrl);
											anchor.attr('download', filename);

											$document.find('body').append(anchor);

											anchor[0].click();

											anchor.remove();

											URL.revokeObjectURL(blobUrl);

										}),
									then: ( ) => '',
									catch: ( ) => 'Het bestand kon niet worden gedownload. Neem contact op met uw beheerder voor meer informatie.'
								}
							);
					};

				}],
				controllerAs: 'externalDocumentView'

			};
		}
		])
		.name;
