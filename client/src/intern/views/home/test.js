import controllerModule from './controller.js';
import angular from 'angular';
import 'angular-mocks';
import mutationServiceModule from './../../../shared/api/resource/mutationService';
import cacherModule from './../../../shared/api/cacher';
import zsStorageModule from './../../../shared/util/zsStorage';
import resourceModule from './../../../shared/api/resource';
import assign from 'lodash/assign';
import first from 'lodash/head';
import reject from 'lodash/reject';
import template from './index.html';

describe('zsDashboard', ( ) => {

	let $window,
		addEventListenerFn,
		$rootScope,
		$compile,
		$httpBackend,
		$timeout,
		$controller,
		initialWidgetData,
		mutationService,
		resource,
		apiCacher,
		zsStorage,
		scope,
		initComponent,
		ctrl,
		createWidgetData;

	createWidgetData = (customAttributes) => {
		return customAttributes.map(( params ) => {
			return {
				instance: assign({
					data: { 'json_data': 'valid' },
					column: 0,
					row: 0,
					'size_x': 2,
					'size_y': 2,
					widget: 'test'
				}, params),
				reference: params.id,
				type: 'widget'
			};
		});
	};

	initialWidgetData = createWidgetData([ {
			column: 2,
			'size_x': 1,
			id: '2f6ff273-3d9d-401a-ba0b-b2c67b20f4a5'
		},
		{
			column: 0,
			'size_x': 2,
			id: '3b636fd2-7bd3-4e1d-a008-4b625e5ddf09'
		}
	]);

	initComponent = ( responseData ) => {

		scope = $rootScope.$new();

		$httpBackend.whenGET('/api/v1/dashboard/widget')
			.respond(responseData);

		scope.vm = $controller('DashboardController', {
			$scope: scope,
			$element: angular.element('<div></div>'),
			$stateParams: {

			},
			widgetResource: resource('/api/v1/dashboard/widget', { scope })
		});

		$compile(template)(scope);

		ctrl = scope.vm;

		$httpBackend.flush();
	};


	beforeEach(angular.mock.module(controllerModule, resourceModule, cacherModule, mutationServiceModule, zsStorageModule));
	beforeEach(angular.mock.inject([ '$window', '$rootScope', '$compile', '$httpBackend', '$timeout', '$controller', 'resource', 'apiCacher', 'mutationService', 'zsStorage', ( ...rest ) => {

		[ $window, $rootScope, $compile, $httpBackend, $timeout, $controller, resource, apiCacher, mutationService, zsStorage ] = rest;

		// gridster listens to a resize event and does not unregister itself
		// so we stub addEventListener to prevent real registration

		addEventListenerFn = $window.addEventListener;

		$window.addEventListener = ( ) => { };
		
	}]));

	afterEach(( ) => {

		$window.addEventListener = addEventListenerFn;

	});


	describe('without error', ( ) => {

		beforeEach(angular.mock.inject([() => {
			initComponent(initialWidgetData);
		}]));

		it('should have a controller', () => {

			expect(ctrl).toBeDefined();

		});

		it('should have angular gridster options', () => {

			expect(ctrl.gridsterOpts).not.toBeNull();

		});

		it('should have angular gridster', () => {

			expect(ctrl.customItemMap).not.toBeNull();

		});

		it('should have widget data', () => {

			expect(ctrl.getWidgets().length).toBe(initialWidgetData.length);

		});

		it('should update widget data', () => {

			let data = createWidgetData([ {
				column: 0,
				'size_x': 3,
				id: '2f6ff273-3d9d-401a-ba0b-b2c67b20f4a5'
			},
			{
				column: 2,
				'size_x': 1,
				id: '3b636fd2-7bd3-4e1d-a008-4b625e5ddf09'
			} ]);

			$httpBackend.whenPOST('/api/v1/dashboard/widget/bulk_update')
				.respond(data);

			ctrl.gridsterOpts.resizable.stop();

			$timeout.flush();

			$httpBackend.flush();

			expect(ctrl.getWidgets()[1].size_x).toBe(data[1].instance.size_x);
			
		});

		it('should add a widget', () => {
			let newWidgetData = createWidgetData([{
					name: 'Lijst met zaaktypen',
					column: 3,
					row: 0,
					'size_x': 1,
					'size_y': 1,
					widget: 'test',
					id: '051e88a1-1180-4210-b04e-a07347e096fb'
				}]);

			$httpBackend.whenPOST('/api/v1/dashboard/widget/create')
				.respond(initialWidgetData.concat(newWidgetData));

			ctrl.handleWidgetCreate(newWidgetData[0].instance);

			$timeout.flush();

			$httpBackend.flush();

			expect(ctrl.getWidgets().length).toBe(initialWidgetData.length + 1);
			
		});

		it('should remove a widget', () => {
			let removedElement = first(ctrl.getWidgets()),
				newData = reject(initialWidgetData, { reference: removedElement.id });

			expect(newData.length).toBeLessThan(initialWidgetData.length);

			$httpBackend.whenPOST(`/api/v1/dashboard/widget/${removedElement.id}/delete`)
				.respond(newData);

			expect(ctrl.getWidgets().length).toBe(initialWidgetData.length);

			ctrl.remove(removedElement);

			expect(ctrl.getWidgets().length).toBe(newData.length);
			
			$timeout.flush();

			$httpBackend.flush();

			expect(ctrl.getWidgets().length).toBe(newData.length);
			
		});

	});
	
	describe('with error', ( ) => {
		
		beforeEach(angular.mock.inject([() => {
			initComponent(initialWidgetData);
		}]));


		// angular gridster mutates the data so this is hard to do
		it('could not update widgetdata', ( ) => {

			let currentSizeX = ctrl.getWidgets()[0].size_x;

			$httpBackend.whenPOST('/api/v1/dashboard/widget/bulk_update')
				.respond(400, 'error message');

			ctrl.getWidgets()[0].size_x = currentSizeX + 1;

			ctrl.gridsterOpts.resizable.stop();

			expect(ctrl.getWidgets()[0].size_x).toBe(currentSizeX + 1);

			$timeout.flush();

			$httpBackend.flush();

			// check if the data is changed back to what is was before the change
			expect(ctrl.getWidgets()[0].size_x).toBe(currentSizeX);
		});


	});
	
	afterEach( ( ) => {

		scope.$destroy();

		apiCacher.clear();
		zsStorage.clear();
		mutationService.clear();

	});

});
