# ESDoc documentation

> Preliminary client documentation with [ESDoc](https://esdoc.org/). 
  Keep this isolated from the `../client` directory until Node.js is upgraded to version 6.

## Requirements

- nvm
- Node >= 6 (recommended: 6.10.1 LTS)

## Usage

    npm run doc

## TODO

Exporting arrow functions or anonymous function expressions doesn't fly
(this is is just as bad in a debugger of course),
ESDoc resorts to the file's base name in that case, which is usually 'index'.
