export default ( valueName ) => {

	let proposalItems = element.all(by.css('.proposal-detail-view__list .proposal-detail-view__item')),
		result,
		fieldValue;

	result = proposalItems.filter((elm) => {
		return elm.$('.proposal-detail-view__label').getText().then((text) => {
			return text === valueName;
		});
	}).first();

	fieldValue = result.$('.proposal-detail-view__value').getText();

	return fieldValue;

};
