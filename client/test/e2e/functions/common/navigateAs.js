import loginPage from './../intern/loginPage';
import xhrRequests from './auth/xhrRequests';
import navigateTo from './navigateTo';

export default ( username = 'admin', destination = '/intern/', wrongPassword ) => {

	xhrRequests.getUsername().then(loggedInUser => {

		browser.getCurrentUrl().then(locationFull => {

			let location = locationFull.replace('https://testbase-local.zaaksysteem.nl', '');

			if ( (loggedInUser !== username && loggedInUser !== undefined) || wrongPassword ) {

				loginPage.logout();

			}

			if ( (loggedInUser !== username || loggedInUser === undefined) || wrongPassword ) {

				if ( destination !== '/intern/') {

					navigateTo(destination);

				}

				loginPage.login(username, wrongPassword);

			} else if ( destination !== location ) {

				navigateTo(destination);

			}

		});

	});

};
