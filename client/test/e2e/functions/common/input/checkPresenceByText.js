export default ( textToLookUp, elementCss ) => {

	let elementsToCheck = element.all(by.css(elementCss));

	return elementsToCheck.filter(name => {
		return name.getText().then(text => {
			return text === textToLookUp;
		});
	}).then(filteredElements => {
		return filteredElements.length > 0;
	});
};
