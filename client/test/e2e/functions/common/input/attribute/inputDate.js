export default ( attribute, date ) => {

	let dateToInput = date ? date : '13-01-2010',
		dateSplit = dateToInput.split('-'),
		dayToInput = dateSplit[0],
		monthToInput = dateSplit[1],
		yearToInput = dateSplit[2];

	attribute.$('input').sendKeys(dayToInput);
	attribute.$('input').sendKeys(monthToInput);
	attribute.$('input').sendKeys(protractor.Key.TAB);
	attribute.$('input').sendKeys(yearToInput);

};
