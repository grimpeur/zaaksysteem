export default ( attribute, textToInput ) => {

	attribute.getAttribute('class').then((attributeClass) => {

		let inputText,
			i;

		if (attributeClass.includes('multiple')) {

			inputText = textToInput ? textToInput : ['this is random text\nand another line', 'and another text\nand another line'];

		} else {

			inputText = textToInput ? textToInput : ['this is random text\nand another line'];

		}

		for (i = 0; i < inputText.length; i++) {

			attribute.$(`li:nth-child(${i + 1}) textarea`).sendKeys(inputText[i]);

			if ( inputText.length - 1 > i ) {

				attribute.$('.vorm-field-add-button').click();

			}

		}
		
	});

};
