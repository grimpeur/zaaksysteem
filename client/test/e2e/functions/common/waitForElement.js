export default ( elementToWaitFor ) => {

	browser.driver.wait( () => {
		return browser.driver.isElementPresent(by.css(elementToWaitFor)).then( (el) => {
			return el === true;
		});
	});

};
