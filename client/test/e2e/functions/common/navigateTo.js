export default ( desiredDestination = '/intern/' ) => {

	let destination = Number.isInteger(desiredDestination) ? `/intern/zaak/${desiredDestination}` : desiredDestination;

	browser.get(destination);

};
