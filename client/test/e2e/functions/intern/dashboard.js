import selectFromSuggest from './../common/selectFromSuggest';
import first from 'lodash/first';

let dashboard = {

	resizeWidget: (widgetTitle, corner, x, y) => {

		//corner should be nw, ne, sw, se

		let widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')),
			cornerHandle = widget.$(`.handle-${corner}`);

		browser.driver.actions()
			.mouseMove(widget, { x: 0, y: 0 })
			.perform();

		browser.driver.actions()
			.mouseMove(widget, { x: 1, y: 1 })
			.perform();

		browser.driver.actions()
			.dragAndDrop(cornerHandle, { x, y })
			.mouseUp()
			.perform();

		},

	moveWidget: (widgetTitle, x, y) => {

		browser.driver.actions()
			.dragAndDrop(element(by.cssContainingText('.widget-header-title', widgetTitle)), { x, y })
			.mouseUp()
			.perform();

	},

	deleteWidget: (widgetTitle) => {

		element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')).$('.widget-header-remove-button').click();

	},

	acceptCase: (caseNumber) => {

		let caseRow = $(`[data-name="intake"] .table-row[href="/intern/zaak/${caseNumber}"]`);

		caseRow.$('.case-intake-action-take-on').click();

	},

	refuseCase: (caseNumber) => {

		let caseRow = $(`[data-name="intake"] .table-row[href="/intern/zaak/${caseNumber}"]`),
			confirmButton = $('.zs-modal-body .confirm-button');

		caseRow.$('.case-intake-action-reject').click();

		confirmButton.click();

	},

	addFavoriteCasetype: (casetype) => {

		let parentItem = $('.widget-favorite-suggest');

		selectFromSuggest(parentItem, casetype, first);

		browser.sleep(1000);

	},

	removeFavoriteCasetype: (casetype) => {

		let itemToRemove = element(by.cssContainingText('.widget-favorite-link', casetype)).element(by.xpath('..'));

		itemToRemove.$('.widget-favorite-remove').click();

	},

	startFavoriteCasetype: (casetype) => {

		element(by.cssContainingText('.widget-favorite-link', casetype)).click();

	},

	filterWidget: (widgetTitle, text) => {

		let widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')),
		widgetFilterButton = widget.$('.widget-header-filter button'),
		widgetFilter = widget.$('.widget-header-filter input');

		widgetFilterButton.click();

		widgetFilter.sendKeys(text);

	},

	navigateWidgetPages: (widgetTitle, navType, numberOfClicks = 1) => {

		let widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')),
			widgetPaginationButton = widget.$(`.pagination-buttons .pagination-button-${navType}`),
			i;

		for ( i = 0; i < numberOfClicks; i++ ) {

			widgetPaginationButton.click();

		}

	},

	setWidgetMaxResults: (widgetTitle, numberOfPages) => {

		let widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')),
		widgetPaging = widget.$('.pagination select');

		widgetPaging.sendKeys(numberOfPages);

	},

	compareWidgetCases: (widgetTitle, cases) => {

		let casesInWidget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')).all(by.css('.zs-table-row'));

		return casesInWidget.count().then((count) => {

			if ( count === cases.length ) {

				for ( let i = 0; i < cases.length; i++ ) {

					return casesInWidget.get(i).$('[column-id="case.number"]').getText().then((caseNumber) => {

						if ( caseNumber !== cases[i] ) {

							return false;

						}

						return true;

					});

				}

			} else {

				return false;

			}

		});

	},

	compareWidgetCasetypes: (casetypes) => {

		let casestypesInWidget = element.all(by.css('[data-name=""] .widget-favorite-link'));

		return casestypesInWidget.count().then((count) => {

			if ( count === casetypes.length ) {

				for ( let i = 0; i < casetypes.length; i++ ) {

					return casestypesInWidget.get(i).getText().then((casetypeName) => {

						if ( casetypeName !== casetypes[i] ) {

							return false;

						}

						return true;

					});

				}

			} else {

				return false;

			}

		});

	},

	getIntakeIconType: (caseNumber) => {

		return $(`.table-row[href="/intern/zaak/${caseNumber}"] zs-case-intake-action-list > zs-icon`).getAttribute('icon-type');

	}

};

module.exports = dashboard;
