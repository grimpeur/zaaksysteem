import getFieldSelector from './../common/getFieldSelector';
import inputSelectFirst from './../common/input/inputSelectFirst';
import getRandomBsn from './../common/getValue/getRandomBsn';
import getRandomKvk from './../common/getValue/getRandomKvk';
import inputFile from './../common/input/attribute/inputFile';
import inputDate from './../common/input/attribute/inputDate';
import mouseOut from './../common/mouseOut';

let plusMenu = {

	openAction: ( name ) => {

		browser.actions()
			.mouseMove(element(by.css('.top-bar-create-case')), { x: 0, y: 0 })
			.perform();
		browser.actions()
			.mouseMove(element(by.css('.top-bar-create-case')), { x: 1, y: 1 })
			.perform();

		return element(
			by.css(`zs-contextual-action-menu-button [data-name=${name}] button`)
		).click();

	},

	createCase: params => {

		let form = $('zs-contextual-action-menu form'),
			casetypeClose = '[data-name="casetype"] .mdi-close',
			requestorTypeClose = '[data-name="requestor_type"] .mdi-close',
			recipientClose = '[data-name="recipient"] .mdi-close';

		plusMenu.openAction('zaak');

		if ( params.rememberCasetype ) {

			form.$('.zs-switch-label').click();

		}

		if ( params.casetypeClear ) {

			form.$(casetypeClose).click();

		}

		if ( params.casetype ) {

			inputSelectFirst(form, 'casetype', params.casetype);

		}

		if ( params.requestorTypeClear ) {

			form.$(requestorTypeClose).click();

		}

		if ( params.requestorType ) {

			form.$(getFieldSelector('requestor_type'))
				.$(`input[type=radio][value=${params.requestorType}]`)
				.click();

		}

		if ( params.requestor ) {

			inputSelectFirst(form, 'requestor', params.requestor);

		}

		if ( params.requestorType === 'medewerker' ) {

			let recipientType = params.recipientType ? params.recipientType : 'medewerker';

			form.$(getFieldSelector('recipient_type'))
			.$(`input[type=radio][value=${recipientType}]`)
			.click();

			if ( params.recipientClear ) {

				form.$(recipientClose).click();

				inputSelectFirst(form, 'recipient', params.recipient);

			}

		}

		if ( params.source ) {

			form.$('[data-name="source"] select').sendKeys(params.source);

		}

		form.submit();

	},

	createContact: newContact => {

		let form = $('zs-contextual-action-form form'),
			fields = {};

		plusMenu.openAction('contact');

		fields.type = {
				type: 'radio',
				fieldname: 'betrokkene_type',
				value: newContact.type === undefined ? undefined : newContact.type
			};
			
		if (newContact.type === 'natuurlijk_persoon') {

			fields.bsn = {
				type: 'text',
				fieldname: 'np-burgerservicenummer',
				value: newContact.bsn === undefined ? getRandomBsn() : newContact.bsn
			};
			fields.firstName = {
				type: 'text',
				fieldname: 'np-voornamen',
				value: newContact.firstName
			};
			fields.surnamePrefix = {
				type: 'text',
				fieldname: 'np-voorvoegsel',
				value: newContact.surnamePrefix
			};
			fields.lastName = {
				type: 'text',
				fieldname: 'np-geslachtsnaam',
				value: newContact.lastName === undefined ? 'lastName' : newContact.lastName
			};
			fields.sex = {
				type: 'radio',
				fieldname: 'np-geslachtsaanduiding',
				value: newContact.sex === undefined ? 'M' : newContact.sex
			};
			fields.country = {
				type: 'list',
				fieldname: 'np-landcode',
				value: newContact.country
			};

			if ( newContact.country === undefined ) {

				fields.correspondenceAddress = {
					type: 'checkbox',
					fieldname: 'briefadres',
					value: newContact.correspondenceAddress
				};
				fields.streetName = {
					type: 'text',
					fieldname: 'np-straatnaam',
					value:	newContact.streetName !== undefined ?
							newContact.streetName
							: newContact.correspondenceAddress === undefined ? 'streetName' : undefined
				};
				fields.houseNumber = {
					type: 'text',
					fieldname: 'np-huisnummer',
					value:	newContact.houseNumber !== undefined ?
							newContact.houseNumber
							: newContact.correspondenceAddress === undefined ? '1' : undefined
				};
				fields.houseNumberExtra = {
					type: 'text',
					fieldname: 'np-huisnummertoevoeging',
					value: newContact.houseNumberExtra
				};
				fields.zipcode = {
					type: 'text',
					fieldname: 'np-postcode',
					value:	newContact.zipcode !== undefined ?
							newContact.zipcode
							: newContact.correspondenceAddress === undefined ? '1111AA' : undefined
				};
				fields.city = {
					type: 'text',
					fieldname: 'np-woonplaats',
					value:	newContact.city !== undefined ?
							newContact.city
							: newContact.correspondenceAddress === undefined ? 'city' : undefined
				};
				fields.withinCity = {
					type: 'checkbox',
					fieldname: 'np-in-gemeente',
					value: newContact.withinCity
				};

				if ( newContact.correspondenceAddress !== undefined ) {

					fields.correspondenceStreetName = {
						type: 'text',
						fieldname: 'np-correspondentie_straatnaam',
						value:	newContact.correspondenceAddress === undefined ?
								undefined
								: newContact.correspondenceStreetName === undefined ? 'streetName' : newContact.correspondenceStreetName
					};
					fields.correspondenceHouseNumber = {
						type: 'text',
						fieldname: 'np-correspondentie_huisnummer',
						value:	newContact.correspondenceAddress === undefined ?
								undefined
								: newContact.correspondenceHouseNumber === undefined ? '1' : newContact.correspondenceHouseNumber
					};
					fields.correspondenceHouseNumberExtra = {
						type: 'text',
						fieldname: 'np-correspondentie_huisnummertoevoeging',
						value:	newContact.correspondenceHouseNumberExtra
					};
					fields.correspondenceZipcode = {
						type: 'text',
						fieldname: 'np-correspondentie_postcode',
						value:	newContact.correspondenceAddress === undefined ?
								undefined
								: newContact.correspondenceZipcode === undefined ? '1111AA' : newContact.correspondenceZipcode
					};
					fields.correspondenceCity = {
						type: 'text',
						fieldname: 'np-correspondentie_woonplaats',
						value:	newContact.correspondenceAddress === undefined ?
								undefined
								: newContact.correspondenceCity === undefined ? 'city' : newContact.correspondenceCity
					};
				}

			} else {

				fields.foreignAddress1 = {
					type: 'text',
					fieldname: 'np-adres_buitenland1',
					value: newContact.foreignAddress1 === undefined ? 'foreignAddress1' : newContact.foreignAddress1
				};
				fields.foreignAddress2 = {
					type: 'text',
					fieldname: 'np-adres_buitenland2',
					value: newContact.foreignAddress2
				};
				fields.foreignAddress3 = {
					type: 'text',
					fieldname: 'np-adres_buitenland3',
					value: newContact.foreignAddress3
				};

			}

		} else {

			fields.country = {
				type: 'list',
				fieldname: 'vestiging_landcode',
				value: newContact.country
			};
			fields.tradeName = {
					type: 'text',
					fieldname: 'handelsnaam',
					value: newContact.tradeName === undefined ? 'tradeName' : newContact.tradeName
			};

			if ( newContact.country === undefined ) {

				fields.typeOfBusinessEntity = {
					type: 'list',
					fieldname: 'rechtsvorm',
					value: newContact.typeOfBusinessEntity === undefined ? 'Eenmanszaak' : newContact.typeOfBusinessEntity
				};
				fields.coc = {
					type: 'text',
					fieldname: 'dossiernummer',
					value: newContact.coc === undefined ? getRandomKvk() : newContact.coc
				};
				fields.establishmentNumber = {
					type: 'text',
					fieldname: 'vestigingsnummer',
					value: newContact.establishmentNumber
				};
				fields.streetName = {
					type: 'text',
					fieldname: 'vestiging_straatnaam',
					value: newContact.streetName === undefined ? 'streetName' : newContact.streetName
				};
				fields.houseNumber = {
					type: 'text',
					fieldname: 'vestiging_huisnummer',
					value: newContact.houseNumber === undefined ? '1' : newContact.houseNumber
				};
				fields.houseLetter = {
					type: 'text',
					fieldname: 'vestiging_huisletter',
					value: newContact.houseLetter
				};
				fields.houseNumberExtra = {
					type: 'text',
					fieldname: 'vestiging_huisnummertoevoeging',
					value: newContact.houseNumberExtra
				};
				fields.zipcode = {
					type: 'text',
					fieldname: 'vestiging_postcode',
					value: newContact.zipcode === undefined ? '1111AA' : newContact.zipcode
				};
				fields.city = {
					type: 'text',
					fieldname: 'vestiging_woonplaats',
					value: newContact.city === undefined ? 'city' : newContact.city
				};

			} else {

				fields.foreignAddress1 = {
					type: 'text',
					fieldname: 'vestiging_adres_buitenland1',
					value: newContact.foreignAddress1 === undefined ? 'foreignAddress1' : newContact.foreignAddress1
				};
				fields.foreignAddress2 = {
					type: 'text',
					fieldname: 'vestiging_adres_buitenland2',
					value: newContact.foreignAddress2
				};
				fields.foreignAddress3 = {
					type: 'text',
					fieldname: 'vestiging_adres_buitenland3',
					value: newContact.foreignAddress3
				};

			}

		}

		Object.keys(fields).forEach( key => {

			if ( fields[key].value !== undefined ) {

				switch (fields[key].type) {
					case 'radio':
						form.$(`[data-name="${fields[key].fieldname}"] [value="${fields[key].value}"]`).click();
						break;
					case 'list':
						form.$(`[data-name="${fields[key].fieldname}"] select`).sendKeys(fields[key].value);
						break;
					case 'checkbox':
						form.$(`[data-name="${fields[key].fieldname}"] input`).click();
						break;
					default:
						form.$(`[data-name="${fields[key].fieldname}"] input`).sendKeys(fields[key].value);
						break;
				}

			}

		});

		form.submit();

		browser.sleep(500);

		browser.get('/intern');

	},

	createContactMoment: newContactMoment => {

		let form = $('zs-contextual-action-form form');

		plusMenu.openAction('contact-moment');

		form.$(`[data-name="subject_type"] [value="${newContactMoment.type}"]`).click();

		inputSelectFirst(form, 'subject', '123456789');

		inputSelectFirst(form, 'case', '43');

		form.$('[data-name="message"] textarea').sendKeys(newContactMoment.message);

		form.$('[type="submit"]').click();

	},

	createWidget: ( type, name ) => {

		plusMenu.openAction('create_widget');

		$(`zs-dashboard-widget-create [data-name="${type}"]`).click();

		browser.waitForAngular();

		if ( type === 'Zoekopdracht') {

			let searches = element.all(by.css('zs-dashboard-widget-search-select-list-group li'));

			searches.filter((elm) => {
				return elm.getText().then((text) => {
					return text === `${name}`;
				});
			}).first().click();

		}

	},

	uploadDocument: documentName => {

		let form = $('zs-contextual-action-form');

		plusMenu.openAction('upload');

		inputFile(form, documentName);

		mouseOut();

	},

	useTemplate: (data = {}) => {

		let form = $('zs-case-template-generate form');

		plusMenu.openAction('sjabloon');

		if (data.template) {

			form.$('[data-name="template"] select').sendKeys(data.template);

		}

		if (data.name) {

			form.$('[data-name="name"] input').clear().sendKeys(data.name);

		}

		if (data.filetype) {

			form.$(`[data-name="filetype"] input[value="${data.filetype}"]`).click();

		}

		if (data.caseDocument) {

			form.$('[data-name="case_document"] select').sendKeys(data.caseDocument);

		}

		form.submit();

		mouseOut();

	},

	relateSubject: data => {

		let form = $('zs-case-add-subject form');

		plusMenu.openAction('betrokkene');

		if ( data.subjectType ) {

			let subjectType = data.subjectType === 'organisation' ? 'bedrijf' : 'natuurlijk_persoon';

			form.$(`[data-name="relation_type"] input[value="${subjectType}"]`).click();

		}

		inputSelectFirst(form, 'related_subject', data.subjectToRelate);

		if ( data.role ) {

			form.$('[data-name="related_subject_role"] select').sendKeys(data.role);

		}

		if ( data.roleOther ) {

			form.$('[data-name="related_subject_role_freeform"] input').sendKeys(data.roleOther);

		}

		if ( data.authorisation ) {

			form.$('[data-name="pip_authorized"] input').click();

		}

		if ( data.notification ) {

			form.$('[data-name="notify_subject"] input').click();

		}

		form.submit();

		mouseOut();

	},

	sendEmail: data => {

		let form = $('zs-case-send-email form');

		plusMenu.openAction('email');

		if ( data.template ) {

			form.$('[data-name="template"] select').sendKeys(data.template);

		}

		form.$('[data-name="recipient_type"] select').sendKeys(data.recipientType);

		if ( data.recipientEmployee ) {

			inputSelectFirst(form, 'recipient_medewerker', data.recipientEmployee);

		}

		if ( data.recipientOther ) {

			form.$('[data-name="recipient_address"] input').sendKeys(data.recipientOther);

		}

		if ( data.cc ) {

			form.$('[data-name="recipient_cc"] input').sendKeys(data.cc);

		}

		if ( data.bcc ) {

			form.$('[data-name="recipient_bcc"] input').sendKeys(data.bcc);

		}

		if ( data.subject ) {

			form.$('[data-name="email_subject"] input').sendKeys(data.subject);

		}

		if (data.content ) {

			form.$('[data-name="email_content"] textarea').sendKeys(data.content);
			
		}

		form.submit();

	},

	planCase: data => {

		let form = $('zs-case-plan form');

		plusMenu.openAction('geplande-zaak');

		inputSelectFirst(form, 'casetype', data.casetype);

		if ( data.copyRelations ) {

			form.$('[data-name="copy_relations"] input').click();

		}

		inputDate(form.$('[data-name="from"]'), data.from);

		if ( data.pattern ) {

			form.$('[data-name="has_pattern"] input').click();

			if ( data.count ) {

				form.$('[data-name="count"] input').clear().sendKeys(data.count);

			}

			if ( data.type ) {

				form.$('[data-name="type"] select').sendKeys(data.type);

			}

			if ( data.repeat ) {

				form.$('[data-name="repeat"] input').clear().sendKeys(data.repeat);

			}

		}

		form.submit();
		
		mouseOut();
		
	}

};

module.exports = plusMenu;
