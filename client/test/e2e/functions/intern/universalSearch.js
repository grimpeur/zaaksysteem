let universalSearch = {

	search: input => {

		$('zs-universal-search input').click();
		$('zs-universal-search input').sendKeys(input);

		browser.sleep(1000);

	},

	exit: ( ) => {

		$('zs-universal-search .spot-enlighter-back-button').click();

	},

	countResults: ( ) => {

		let suggestions = element.all(by.css('zs-universal-search .suggestion-list-item'));

		return suggestions.count().then(count => {

			return count;

		});

	}

};

module.exports = universalSearch;
