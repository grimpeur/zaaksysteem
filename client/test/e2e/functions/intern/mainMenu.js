let mainMenu = {

	toggle: ( ) => {

		$('.top-bar-menu-button').click();

	},

	toggleNotifications: ( ) => {

		let notificationButton = '[zs-tooltip="Notificaties"]',
			notifications = '.notifications';

		mainMenu.toggle();

		$(notifications).isDisplayed()
			.then(

				isDisplayed => {
					return isDisplayed ?
						isDisplayed
						: Promise.reject();
				},
				( ) => $(notificationButton).click()
				
			);

	}

};

module.exports = mainMenu;
