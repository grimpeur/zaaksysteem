import inputSelectFirst from './../common/input/inputSelectFirst';
import inputDate from './../common/input/attribute/inputDate';

let actionMenu = {

	toggle: ( ) => {

		$('.top-bar-action-container .top-bar-button').click();

		browser.waitForAngular();

	},

	selectAction: action => {

		let expandIcon = element.all(by.css('zs-contextual-setting-menu [icon-type="chevron-down"]')),
			actionButtons = element.all(by.css('zs-contextual-setting-menu li'));

			actionMenu.toggle();

			expandIcon.isDisplayed().then( isDisplayed => {
				if (isDisplayed) {
					expandIcon.click();
				}
			});

		actionButtons.filter((elm) => {
			return elm.getText().then((text) => {
				return text === action;
			});
		}).first().click();

	},

	//dashboard actions

	resetDashboard: ( ) => {

		actionMenu.selectAction('Terug naar standaardinstelling');

	},

	//caseview actions

	assignTo: ( type, data ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Toewijzing wijzigen');

		switch (type) {

			case 'group':
				$('[value="org-unit"]').click();
				$('[data-name="department-picker"] select').sendKeys(data.department);
				$('[data-name="role-picker"] select').sendKeys(data.role);
				break;

			case 'person':
				$('[value="assignee"]').click();
				inputSelectFirst(form, 'assignee', data.name);
				break;

			case 'self':
				$('[value="assign_to_self"]').click();
				break;

			default:
				break;

		}

		form.submit();

	},

	stall: ( type, termAmountType, termAmount ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Opschorten');

		$('[data-name="reason"] input').sendKeys('Reason');

		if (type === 'indeterminate') {

			$('[data-name="term"] [value="indeterminate"]').click();

		} else if ( type === 'determinate' ) {

			$('[data-name="term"] [value="determinate"]').click();

			$('[data-name="term_amount_type"] select').sendKeys(termAmountType);
			$('[data-name="term_amount"] input').clear().sendKeys(termAmount);

		}

		form.submit();

	},

	resume: ( data = {}) => {

		let form = $('zs-case-admin-view form'),
			startDateField = $('[data-name="start_date"]'),
			endDateField = $('[data-name="end_date"]');

		actionMenu.selectAction('Hervatten');

		$('[data-name="reason"] input').sendKeys('Reason');

		if (data.start) {
			inputDate(startDateField, data.start);
		}

		if (data.end) {
			inputDate(endDateField, data.end);
		}

		form.submit();

	},

	closeEarly: ( result ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Vroegtijdig afhandelen');

		$('[data-name="reason"] input').sendKeys('Reason');
		$(`[value="${result}"]`).click();

		form.submit();

	},

	changeTerm: ( type, data ) => {

		let form = $('zs-case-admin-view form'),
			dateField = $('[data-name="new_term_date"]');

		actionMenu.selectAction('Termijn wijzigen');

		$('[data-name="reason"] input').sendKeys('Reason');

		if ( type === 'fixed' ) {

			$('[value="fixedDate"]').click();

			inputDate(dateField, data.date);

		} else if ( type === 'term' ) {

			$('[value="changeTerm"]').click();

			$('[data-name="term_amount_type"] select').sendKeys(data.termType);
			$('[data-name="term_amount"] input').clear().sendKeys(data.termAmount);

		}

		form.submit();

	},

	relateCase: ( caseToRelateTo ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Zaak relateren');

		inputSelectFirst(form, 'case_to_relate', caseToRelateTo);

		form.submit();

	},

	duplicate: ( ) => {

		actionMenu.selectAction('Zaak kopiëren');

		$('.zs-modal-body .confirm-button').click();

	},

	relateObject: ( object ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Object relateren');

		inputSelectFirst(form, 'related_object', object);

		form.submit();

	},

	changeRequestor: ( type, requestor ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Aanvrager wijzigen');

		$('zs-case-admin-view [data-name="requestor"] .vorm-clear').click();
		$(`zs-case-admin-view [data-name="type"] [value=${type}]`).click();
		
		inputSelectFirst(form, 'requestor', requestor);

		form.submit();

	},

	changeCoordinator: ( username ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Coordinator wijzigen');

		inputSelectFirst(form, 'coordinator', username);

		form.submit();

	},

	changeDepartmentRole: ( department, role ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Afdeling en rol wijzigen');

		$('[data-name="department-picker"] select').sendKeys(department);
		$('[data-name="role-picker"] select').sendKeys(role);

		form.submit();

	},

	changeRegistrationDate: ( date ) => {

		let form = $('zs-case-admin-view form'),
			dateField = $('[data-name="registratiedatum"]');

		actionMenu.selectAction('Registratiedatum wijzigen');

		inputDate(dateField, date);

		form.submit();

	},

	changeTargetDate: ( date ) => {

		let form = $('zs-case-admin-view form'),
			dateField = $('[data-name="streefafhandeldatum"]');

		actionMenu.selectAction('Streefafhandeldatum wijzigen');

		inputDate(dateField, date);

		form.submit();

	},

	changeCompletionDate: ( date ) => {

		let form = $('zs-case-admin-view form'),
			dateField = $('[data-name="afhandeldatum"]');

		actionMenu.selectAction('Afhandeldatum wijzigen');

		inputDate(dateField, date);

		form.submit();

	},

	changeDestructionDate: ( type, updateWith ) => {

		let form = $('zs-case-admin-view form'),
			fixedSection = $('[data-name="vernietigingsdatum"]'),
			termSection = $('[data-name="vernietigingstermijn"]');

		actionMenu.selectAction('Vernietigingsdatum wijzigen');

		if ( type === 'fixed') {

			fixedSection.$('input[type="radio"]').click();

			inputDate($('[data-name="date"]'), updateWith);

		} else if ( type === 'term' ) {

			termSection.$('input[type="radio"]').click();

			termSection.$('select').sendKeys(updateWith);

		}

		form.submit();

	},

	changeAttributes: ( attribute, value ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Kenmerken wijzigen');

		inputSelectFirst(form, 'attributes', attribute);

		$('zs-case-admin-attribute-list .vorm-control').sendKeys(value);

		form.submit();

	},

	changePhase: ( phase ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Fase wijzigen');

		$('[data-name="phase"] select').sendKeys(phase);

		form.submit();

	},

	changeStatus: ( status ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Status wijzigen');

		$('[data-name="status"] select').sendKeys(status);

		form.submit();

	},

	changeAuth: ( department, role, authType ) => {

		let form = $('zs-case-admin-view form'),
			authLevel,
			i;

		switch (authType) {
			case 'manage':
				authLevel = '4';
				break;
			case 'handle':
				authLevel = '3';
				break;
			case 'access':
				authLevel = '2';
				break;
			case 'search':
				authLevel = '1';
				break;
			default:
				authLevel = '0';
				break;
		}

		actionMenu.selectAction('Rechten wijzigen');

		$('[data-name="department-picker"] select').sendKeys(department);
		$('[data-name="role-picker"] select').sendKeys(role);

		for (i = 1; i <= authLevel; i++) {
			$(`.capabilities-list li:nth-child(${i})`).click();
		}

		form.submit();

	},

	changeCasetype: ( casetype ) => {

		let form = $('zs-case-admin-view form');

		actionMenu.selectAction('Zaaktype wijzigen');

		inputSelectFirst(form, 'casetype', casetype);

		form.submit();

	}

};

module.exports = actionMenu;
