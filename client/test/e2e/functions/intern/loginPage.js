import e2epasswords from './../../e2epasswords.json';

let loginPage = {

	loginForm: $('#loginwrap form'),
	usernameField: $('#id_username'),
	passwordField: $('#id_password'),
	submit: $('form input[type=submit]'),

	getPassword: (username) => {

		return e2epasswords[username];

	},

	logout: () => {

		browser.get('/auth/logout');

	},

	login: (username = 'admin', wrongPassword) => {

		let password = wrongPassword ? wrongPassword : loginPage.getPassword(username);

		loginPage.usernameField.sendKeys(username);
		loginPage.passwordField.sendKeys(password);
		loginPage.submit.click();

	}

};

module.exports = loginPage;
