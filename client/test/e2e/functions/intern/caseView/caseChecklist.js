let caseChecklist = {

	create: ( itemText ) => {

		let checklistItemAdd = $('.checklist-item-add input');

		checklistItemAdd.sendKeys(itemText);
		checklistItemAdd.sendKeys(protractor.Key.ENTER);

	},

	delete: ( itemNumber ) => {

		$(`.check-list li:nth-child(${itemNumber}) .sidebar-item-action button`).click();

	},

	toggle: ( itemNumber ) => {

		$(`.check-list li:nth-child(${itemNumber}) input`).click();

	}

};

module.exports = caseChecklist;
