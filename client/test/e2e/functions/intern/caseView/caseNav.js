let caseNav = {

	openTab: ( tab ) => {

		$(`.case-navigation-list [data-name="${tab}"]`).click();

	},

	openPhaseActions: ( ) => {

		$('.phase-sidebar .phase-sidebar-header-inner li:nth-child(1)').click();

	},

	openChecklist: ( ) => {

		$('.phase-sidebar .phase-sidebar-header-inner li:nth-child(2)').click();

	},

	openPhase: ( phase ) => {

		$(`zs-case-phase-nav .phase-list-container div:nth-child(${phase})`).click();

	}

};

module.exports = caseNav;
