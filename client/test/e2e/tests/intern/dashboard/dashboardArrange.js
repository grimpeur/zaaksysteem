import navigateAs from './../../../functions/common/navigateAs';
import dashboard from './../../../functions/intern/dashboard';
import actionMenu from './../../../functions/intern/actionMenu';

describe('when logging in as Dashboard Arrange', ( ) => {

	beforeAll(( ) => {

		navigateAs('dashboardarrange');
	
	});

	describe('and when moving widget caseIntake to the position of myOpenCases', ( ) => {
	
		beforeAll(( ) => {
		
			dashboard.moveWidget('Zaakintake', 0, -600);

			browser.refresh();
		
		});
	
		it('the widget caseIntake should be above widget myOpenCases', ( ) => {

			$('[data-name="mine"]').element(by.xpath('..')).getCssValue('top').then((mineTop) => {

				$('[data-name="intake"]').element(by.xpath('..')).getCssValue('top').then((intakeTop) => {

					let mineTopInt = parseInt(mineTop.replace('px', ''), 10),
						intakeTopInt = parseInt(intakeTop.replace('px', ''), 10);

					expect(intakeTopInt).toBeLessThan(mineTopInt);

				});

			});

		});

		it('the widget myOpenCases should be below widget caseIntake', ( ) => {

			$('[data-name="mine"]').element(by.xpath('..')).getCssValue('top').then((mineTop) => {

				$('[data-name="intake"]').element(by.xpath('..')).getCssValue('top').then((intakeTop) => {

					let mineTopInt = parseInt(mineTop.replace('px', ''), 10),
						intakeTopInt = parseInt(intakeTop.replace('px', ''), 10);

					expect(mineTopInt).toBeGreaterThan(intakeTopInt);

				});

			});

		});
	
		afterAll(( ) => {
		
			actionMenu.resetDashboard();
		
		});
	
	});

	describe('and when resizing widget myOpenCases to be less wide and more tall', ( ) => {

		let myOpenCases = $('[data-name="mine"]').element(by.xpath('..')),
			intake = $('[data-name="intake"]').element(by.xpath('..'));
	
		beforeAll(( ) => {
		
			dashboard.resizeWidget('Mijn openstaande zaken', 'se', -600, 300);
		
		});

		it('the widget myOpenCases should be less wide', ( ) => {

			myOpenCases.getCssValue('width').then((mineCssWidth) => {

				intake.getCssValue('width').then((intakeCssWidth) => {

					let mineWidth = parseInt(mineCssWidth.replace('px', ''), 10),
						intakeWidth = parseInt(intakeCssWidth.replace('px', ''), 10);

					expect( mineWidth * 1.1 < intakeWidth ).toBe(true);

				});

			});

		});

		it('the widget myOpenCases should be taller', ( ) => {

			myOpenCases.getCssValue('height').then((mineCssHeight) => {

				intake.getCssValue('height').then((intakeCssHeight) => {

					let mineHeight = parseInt(mineCssHeight.replace('px', ''), 10),
						intakeHeight = parseInt(intakeCssHeight.replace('px', ''), 10);

					expect( mineHeight / 1.1 > intakeHeight ).toBe(true);

				});

			});

		});
	
		afterAll(( ) => {
		
			actionMenu.resetDashboard();
		
		});
	
	});

	describe('and when deleting widget myOpenCases', ( ) => {
	
		beforeAll(( ) => {
		
			dashboard.deleteWidget('Mijn openstaande zaken');
		
		});

		it('the widget myOpenCases should not be present', ( ) => {

			expect($('[data-name="mine"]').isPresent()).toBe(false);

		});
	
		afterAll(( ) => {
		
			actionMenu.resetDashboard();
		
		});
	
	});

});
