import navigateAs from './../../../functions/common/navigateAs';
import navigateTo from './../../../functions/common/navigateTo';
import actionMenu from './../../../functions/intern/actionMenu';
import caseMenu from './../../../functions/intern/caseView/caseMenu';
import caseNav from './../../../functions/intern/caseView/caseNav';
import getFutureDate from './../../../functions/common/getFutureDate';

describe('when assigning the case to a group', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 75);

		let data = {
			department: '-Zaakacties',
			role: 'Behandelaar'
		};

		actionMenu.assignTo('group', data);

	});

	it('the user should be redirected to the dashboard', ( ) => {

		expect($('.gridster-desktop').isPresent()).toBe(true);

	});

	describe('and when opening the case again', ( ) => {

		beforeAll(( ) => {

			navigateTo(75);

		});

		it('the department of the case should have changed to that of the group', ( ) => {

			expect(caseMenu.getAboutValue('Afdeling')).toEqual('Zaakacties');

		});

	});

});

describe('when assigning the case to a person', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 76);

		let data = {
			name: 'Toewijzing wijzigen',
			sendEmail: true,
			changeDepartment: true
		};

		actionMenu.assignTo('person', data);

	});

	it('the user should be redirected to the dashboard', ( ) => {

		expect($('.gridster-desktop').isPresent()).toBe(true);

	});

	describe('and when opening the case again', ( ) => {

		beforeAll(( ) => {

			navigateTo(76);

		});

		it('the department of the case should have changed to that of the person', ( ) => {

			expect(caseMenu.getAboutValue('Afdeling')).toEqual('Zaakacties');

		});

	});

});

describe('when assigning the case to myself', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 77);

		actionMenu.assignTo('self');

	});

	it('the user should not be redirected to the dashboard', ( ) => {

		expect($('.case-view').isPresent()).toBe(true);

	});

	describe('and when opening the case again', ( ) => {

		beforeAll(( ) => {

			navigateTo(77);

		});

		it('the department of the case should not have changed to my department', ( ) => {

			expect(caseMenu.getAboutValue('Afdeling')).toEqual('Backoffice');

		});

	});

});

describe('when stalling the case for an undetermined period of time', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 78);

		actionMenu.stall('indeterminate');

	});

	it('the case should have a stalled notifcation', ( ) => {

		expect($('[data-name="stalled"]').isPresent()).toBe(true);

	});

	it('the stalled notifcation should include that it was indeterminate', ( ) => {

		expect($('[data-name="stalled"]').getText()).toEqual('De zaak is opgeschort voor onbepaalde tijd.');

	});

});

describe('when stalling the case for a determined period of time', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 79);

		actionMenu.stall('determinate', 'Kalenderdagen', '2');

	});

	it('the case should have a stalled notifcation', ( ) => {

		expect($('[data-name="stalled"]').isPresent()).toBe(true);

	});

	it('the stalled notifcation should include the date till when it was stalled', ( ) => {

		let expectedDateText = getFutureDate(2);

		expect($('[data-name="stalled"]').getText()).toEqual(`De zaak is opgeschort tot ${expectedDateText}.`);

	});

});

describe('when resuming the case with the default inputs', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 80);

		actionMenu.resume();

	});

	it('the case should not have a stalled notifcation', ( ) => {

		expect($('[data-name="stalled"]').isPresent()).toBe(false);

	});

});

describe('when resuming the case with changed inputs', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 81);

		let data = {
			start: '13-01-2010',
			end: '15-01-2010'
		};

		actionMenu.resume(data);

	});

	it('the case should not have a stalled notifcation', ( ) => {

		expect($('[data-name="stalled"]').isPresent()).toBe(false);

	});

	it('the target date should have been updated', ( ) => {

		expect(caseMenu.getAboutValue('Streefafhandeldatum')).toEqual('28-04-2017');

	});

});

describe('when closing the case early', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 82);

		actionMenu.closeEarly('aangehouden');

	});

	it('the user should be redirected to the dashboard', ( ) => {

		expect($('.case-view').isPresent()).toBe(true);

	});

	it('the case should have a resolved notification', ( ) => {

		expect($('.case-message-list [data-name="resolved"]').isPresent()).toBe(true);

	});

});

describe('when setting a new target date for the case', ( ) => {

	beforeAll(( ) => {

		let data = {
			date: '13-01-2010'
		};

		navigateAs('admin', 83);

		actionMenu.changeTerm('fixed', data);

	});

	it('the target date of the case should have changed to the new target date', ( ) => {

		expect(caseMenu.getAboutValue('Streefafhandeldatum')).toEqual('13-01-2010');

	});

});

describe('when setting a new target date for the case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 84);

		let data = {
			termAmount: '2',
			termType: 'Kalenderdagen'
		};

		actionMenu.changeTerm('term', data);

	});

	it('the target date of the case should have changed to the new target date', ( ) => {

		expect(caseMenu.getAboutValue('Streefafhandeldatum')).toEqual('27-04-2017');

	});

});

describe('when relating a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 85);

		actionMenu.relateCase('84');

		caseNav.openTab('relations');

	});

	it('the case should have 76 as related case', ( ) => {

		expect($('.related-cases [href="/intern/zaak/84"]').isPresent()).toBe(true);

	});

});

describe('when duplicating the case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 86);

		actionMenu.duplicate();

		browser.sleep(2000);

		browser.ignoreSynchronization = true;

		caseNav.openTab('timeline');

		browser.sleep(4000);

	});

	it('the case should have a duplicate message in the timeline', ( ) => {

		expect(element.all(by.css('[data-event-type="case/duplicate"]')).count()).toEqual(1);

	});

	afterAll(( ) => {

		browser.ignoreSynchronization = false;

		navigateTo();

	});

});

describe('when relating an object', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 87);

		actionMenu.relateObject('Zaakactie object relateren');

		caseNav.openTab('relations');

	});

	it('the case should have the related object', ( ) => {

		expect($('.related_objects [href="/object/8c934ef8-c615-477e-8c64-6e12422ee778"]').isPresent()).toBe(true);

	});

});

describe('when changing the requestor of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 88);

		actionMenu.changeRequestor('natuurlijk_persoon', 'zaakacties');

	});

	it('the case should have a different requestor', ( ) => {

		expect(caseMenu.getAboutValue('Aanvrager')).toEqual('z. zaakacties');

	});

});

describe('when changing the coordinator of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 89);

		actionMenu.changeCoordinator('coordinatorwijzigen');

	});

	it('the case should have a different coordinator', ( ) => {

		expect(caseMenu.getAboutValue('Coordinator')).toEqual('Coordinator wijzigen');

	});

});

describe('when changing the department and role of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 90);

		actionMenu.changeDepartmentRole('-Zaakacties', 'Behandelaar');

	});

	it('the case should have a different department and role', ( ) => {

		expect(caseMenu.getAboutValue('Afdeling')).toEqual('Zaakacties');

	});

});

describe('when changing the registration date of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 91);

		actionMenu.changeRegistrationDate('13-01-2010');

	});

	it('the case should have a different registration date', ( ) => {

		expect(caseMenu.getAboutValue('Registratiedatum')).toEqual('13-01-2010');

	});

});

describe('when changing the target date of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 92);

		actionMenu.changeTargetDate('14-01-2010');

	});

	it('the case should have a different target date', ( ) => {

		expect(caseMenu.getAboutValue('Streefafhandeldatum')).toEqual('14-01-2010');

	});

});

describe('when changing the completion date of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 93);

		actionMenu.closeEarly('aangehouden');

		actionMenu.changeCompletionDate('15-01-2010');

	});

	it('the case should have a different completion date', ( ) => {

		expect(caseMenu.getAboutValue('Afhandeldatum')).toEqual('15-01-2010');

	});

});

describe('when changing the destruction date of a case to a specific date', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 94);

		actionMenu.changeDestructionDate('fixed', '16-01-2010');

	});

	it('the case should have a different destruction date', ( ) => {

		expect(caseMenu.getAboutValue('Uiterste vernietigingsdatum')).toEqual('16-01-2010');

	});

});

describe('when changing the destruction date of a case by letting it recalculate', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 95);

		actionMenu.changeDestructionDate('term', '5 jaar');

	});

	it('the case should have a different destruction date', ( ) => {

		let expectedDateText = getFutureDate(365 * 5);

		expect(caseMenu.getAboutValue('Uiterste vernietigingsdatum')).toEqual(expectedDateText);

	});

});

describe('when changing the attribute of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 96);

		actionMenu.changeAttributes('Terugkoppeling', 'Change attribute');

	});

	it('the case should have a different value in the attribute', ( ) => {

		expect($('[data-name="terugkoppeling"] textarea').getAttribute('value')).toEqual('Change attribute');

	});

});

describe('when changing the phase of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 97);

		$('.phase-advance-button').click();

		actionMenu.changePhase('Behandelen');

	});

	it('the case should have be in a different phase', ( ) => {

		expect($('.phase-list .active [href="/intern/zaak/97/fase/behandelen/"]').isPresent()).toBe(true);

	});

});

describe('when changing the status from closed to open', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 98);

		actionMenu.changeStatus('In behandeling');

		actionMenu.toggle();

	});

	it('the case should have a different status', ( ) => {

		expect($('[data-name="resolved"]').isPresent()).toBe(false);

	});

	describe('when changing the status from open to closed', ( ) => {

		beforeAll(( ) => {

			actionMenu.changeStatus('Afgehandeld');

		});

		it('the case should have a different status', ( ) => {

			expect($('[data-name="resolved"]').isPresent()).toBe(true);

		});

	});

});

describe('when changing the authentication settings of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 99);

		actionMenu.changeAuth('-Systeemrollen', 'Behandelaar', 'handle');

		actionMenu.selectAction('Rechten wijzigen');

	});

	it('the case should have different authentication settings', ( ) => {

		expect($('zs-case-admin-view [checked="checked"]').isPresent()).toBe(true);

	});

});

describe('when changing the casetype of a case', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 100);

		actionMenu.changeCasetype('Zaakacties zaaktype wijzigen');

	});

	it('the name of the casetype should be of a different casetype', ( ) => {

		expect($('.top-bar-current-location span').getText()).toEqual('Zaak 100: Zaakacties zaaktype wijzigen');

	});

	it('the attributes should be updated to those case should be of a different casetype', ( ) => {

		expect($('[data-name="terugkoppeling"]').isPresent()).toBe(false);
		expect($('[data-name="zaakacties"]').isPresent()).toBe(true);

	});

});
