import navigateAs from './../../../../../functions/common/navigateAs';
import inputBag from './../../../../../functions/common/input/attribute/inputBag';
import inputIban from './../../../../../functions/common/input/attribute/inputIban';
import inputCheckbox from './../../../../../functions/common/input/attribute/inputCheckbox';
import inputDate from './../../../../../functions/common/input/attribute/inputDate';
import inputEmail from './../../../../../functions/common/input/attribute/inputEmail';
import inputFile from './../../../../../functions/common/input/attribute/inputFile';
import inputMap from './../../../../../functions/common/input/attribute/inputMap';
import inputImageUrl from './../../../../../functions/common/input/attribute/inputImageUrl';
import inputNumeric from './../../../../../functions/common/input/attribute/inputNumeric';
import inputOption from './../../../../../functions/common/input/attribute/inputOption';
import inputRichtext from './../../../../../functions/common/input/attribute/inputRichtext';
import inputSelect from './../../../../../functions/common/input/attribute/inputSelect';
import inputText from './../../../../../functions/common/input/attribute/inputText';
import inputTextarea from './../../../../../functions/common/input/attribute/inputTextarea';
import inputUrl from './../../../../../functions/common/input/attribute/inputUrl';
import inputValuta from './../../../../../functions/common/input/attribute/inputValuta';
import waitForSave from './../../../../../functions/intern/caseView/waitForSave';

describe('when opening a case with all atrributes', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 48);

	});

	describe('when filling out an attribute of type bag address', ( ) => {

		let attribute = $('.attribute-type-bag_adres');

		beforeAll(( ) => {

			inputBag(attribute, ['6367DB 7'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list').getText()).toEqual('Voerendaal - Florinstraat 7');

		});

	});

	describe('when filling out an attribute of type bag addresses', ( ) => {

		let attribute = $('.attribute-type-bag_adressen');

		beforeAll(( ) => {

			inputBag(attribute, ['6367db 7', '6343PM 3'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list').getText()).toEqual('Voerendaal - Florinstraat 7\nKlimmen - Groeneweg 3');

		});

	});

	describe('when filling out an attribute of type street', ( ) => {

		let attribute = $('.attribute-type-bag_openbareruimte');

		beforeAll(( ) => {

			inputBag(attribute, ['Florinstraat'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list').getText()).toEqual('Florinstraat, Voerendaal');

		});

	});

	describe('when filling out an attribute of type streets', ( ) => {

		let attribute = $('.attribute-type-bag_openbareruimtes');

		beforeAll(( ) => {

			inputBag(attribute, ['Florinstraat', 'Groeneweg'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list').getText()).toEqual('Florinstraat, Voerendaal\nGroeneweg, Klimmen');

		});

	});

	describe('when filling out an attribute of type bag address', ( ) => {

		let attribute = $('.attribute-type-bag_straat_adres');

		beforeAll(( ) => {

			inputBag(attribute, ['Florinstraat 7'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list').getText()).toEqual('Voerendaal - Florinstraat 7');

		});

	});

	describe('when filling out an attribute of type streets', ( ) => {

		let attribute = $('.attribute-type-bag_straat_adressen');
	
		beforeAll(( ) => {

			inputBag(attribute, ['Florinstraat 7', 'Groeneweg 3'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list').getText()).toEqual('Voerendaal - Florinstraat 7\nKlimmen - Groeneweg 3');

		});

	});

	describe('when filling out an attribute of type bankaccount', ( ) => {

		let attribute = $('.attribute-type-bankaccount');

		beforeAll(( ) => {

			inputIban(attribute, 'NL39RABO0300065264' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('NL39RABO0300065264');

		});

	});

	describe('when filling out an attribute of type checkbox', ( ) => {

		let attribute = $('.attribute-type-checkbox');

		beforeAll(( ) => {

			inputCheckbox(attribute, [2, 4] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list label:nth-child(1) input[checked="checked"]').isPresent()).toBe(false);
			expect(attribute.$('.value-list label:nth-child(2) input[checked="checked"]').isPresent()).toBe(true);
			expect(attribute.$('.value-list label:nth-child(3) input[checked="checked"]').isPresent()).toBe(false);
			expect(attribute.$('.value-list label:nth-child(4) input[checked="checked"]').isPresent()).toBe(true);
			expect(attribute.$('.value-list label:nth-child(5) input[checked="checked"]').isPresent()).toBe(false);

		});

	});

	describe('when filling out an attribute of type date', ( ) => {

		let attribute = $('.attribute-type-date');

		beforeAll(( ) => {

			inputDate(attribute, '25-11-2013' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('2013-11-25');

		});

	});

	describe('when filling out an attribute of type email', ( ) => {

		let attribute = $('.attribute-type-email');

		beforeAll(( ) => {

			inputEmail(attribute, 'random@email.com' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('random@email.com');

		});

	});

	describe('when filling out an attribute of type file', ( ) => {

		let attribute = $('.attribute-type-file');

		beforeAll(( ) => {

			inputFile(attribute);

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list li:nth-child(1)').getText()).toEqual('text.txt');

		});

	});

	describe('when filling out an attribute of type geolatlon', ( ) => {

		let attribute = $('.attribute-type-geolatlon');

		beforeAll(( ) => {

			inputMap(attribute, ['Florinstraat 7'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list').getText()).toEqual('50.8779802,5.9403389');

		});

	});

	describe('when filling out an attribute of type googlemaps', ( ) => {

		let attribute = $('.attribute-type-googlemaps');

		beforeAll(( ) => {

			inputMap(attribute, ['Florinstraat 7'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list').getText()).toEqual('Florinstraat 7, 6367 DB Voerendaal, Nederland');

		});

	});

	describe('when filling out an attribute of type image', ( ) => {

		let attribute = $('.attribute-type-image_from_url');

		beforeAll(( ) => {

			inputImageUrl(attribute, 'https://wiki.zaaksysteem.nl/Bestand:Geactiveerd_contact.png' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('https://wiki.zaaksysteem.nl/Bestand:Geactiveerd_contact.png');

		});

		it('should have the display an image', ( ) => {

			expect(attribute.$('img').isPresent()).toBe(true);

		});

	});

	describe('when filling out an attribute of type numeric singular', ( ) => {

		let attribute = $('.attribute-type-numeric:not(.multiple)');

		beforeAll(( ) => {

			inputNumeric(attribute, ['2468'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('2468');

		});

	});

	describe('when filling out an attribute of type option', ( ) => {

		let attribute = $('.attribute-type-option');

		beforeAll(( ) => {

			inputOption(attribute, 3 );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('vorm-radio-group label:nth-child(3) input.ng-valid-parse').isPresent()).toBe(true);

		});

	});

	describe('when filling out an attribute of type numeric multiple', ( ) => {

		let attribute = $('.attribute-type-numeric.multiple');

		beforeAll(( ) => {

			inputNumeric(attribute, ['2', '4', '6', '8'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list li:nth-child(1) input').getAttribute('value')).toEqual('2');
			expect(attribute.$('.value-list li:nth-child(2) input').getAttribute('value')).toEqual('4');
			expect(attribute.$('.value-list li:nth-child(3) input').getAttribute('value')).toEqual('6');
			expect(attribute.$('.value-list li:nth-child(4) input').getAttribute('value')).toEqual('8');

		});

	});

	describe('when filling out an attribute of type richtext', ( ) => {

		let attribute = $('.attribute-type-richtext');

		beforeAll(( ) => {

			inputRichtext(attribute, 'this is my test text\nand another line' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list .ql-editor').getText()).toEqual('this is my test text\nand another line');

		});

	});

	describe('when filling out an attribute of type select', ( ) => {

		let attribute = $('.attribute-type-select:not(.multiple)');

		beforeAll(( ) => {

			inputSelect(attribute, [3] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list li:nth-child(1) select').getAttribute('value')).toEqual('string:Optie 3');

		});

	});

	describe('when filling out an attribute of type select', ( ) => {

		let attribute = $('.attribute-type-select.multiple');

		beforeAll(( ) => {

			inputSelect(attribute, [3, 5, 1] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list li:nth-child(1) select').getAttribute('value')).toEqual('string:Optie 3');
			expect(attribute.$('.value-list li:nth-child(2) select').getAttribute('value')).toEqual('string:Optie 5');
			expect(attribute.$('.value-list li:nth-child(3) select').getAttribute('value')).toEqual('string:Optie 1');

		});

	});

	describe('when filling out an attribute of type text singular', ( ) => {

		let attribute = $('.attribute-type-text:not(.multiple)');

		beforeAll(( ) => {

			inputText(attribute, ['this is my test text'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('this is my test text');

		});

	});

	describe('when filling out an attribute of type text multiple', ( ) => {

		let attribute = $('.attribute-type-text.multiple');

		beforeAll(( ) => {

			inputText(attribute, ['this', 'is', 'test', 'text'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list li:nth-child(1) input').getAttribute('value')).toEqual('this');
			expect(attribute.$('.value-list li:nth-child(2) input').getAttribute('value')).toEqual('is');
			expect(attribute.$('.value-list li:nth-child(3) input').getAttribute('value')).toEqual('test');
			expect(attribute.$('.value-list li:nth-child(4) input').getAttribute('value')).toEqual('text');

		});

	});

	describe('when filling out an attribute of type text uc singular', ( ) => {

		let attribute = $('.attribute-type-text_uc:not(.multiple)');

		beforeAll(( ) => {

			inputText(attribute, ['this is my test text'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('THIS IS MY TEST TEXT');

		});

	});

	describe('when filling out an attribute of type text uc multiple', ( ) => {

		let attribute = $('.attribute-type-text_uc.multiple');

		beforeAll(( ) => {

			inputText(attribute, ['this', 'is', 'test', 'text'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list li:nth-child(1) input').getAttribute('value')).toEqual('THIS');
			expect(attribute.$('.value-list li:nth-child(2) input').getAttribute('value')).toEqual('IS');
			expect(attribute.$('.value-list li:nth-child(3) input').getAttribute('value')).toEqual('TEST');
			expect(attribute.$('.value-list li:nth-child(4) input').getAttribute('value')).toEqual('TEXT');

		});

	});

	describe('when filling out an attribute of type textarea singular', ( ) => {

		let attribute = $('.attribute-type-textarea:not(.multiple)');

		beforeAll(( ) => {

			inputTextarea(attribute, ['this is my test text\nand another line'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list textarea').getAttribute('value')).toEqual('this is my test text\nand another line');

		});

	});

	describe('when filling out an attribute of type textarea multiple', ( ) => {

		let attribute = $('.attribute-type-textarea.multiple');

		beforeAll(( ) => {

			inputTextarea(attribute, ['this is my test text\nand another line', 'and another test text\nand another line'] );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list li:nth-child(1) textarea').getAttribute('value')).toEqual('this is my test text\nand another line');
			expect(attribute.$('.value-list li:nth-child(2) textarea').getAttribute('value')).toEqual('and another test text\nand another line');

		});

	});

	describe('when filling out an attribute of type url', ( ) => {

		let attribute = $('.attribute-type-url');

		beforeAll(( ) => {

			inputUrl(attribute, 'https://www.wikipedia.org' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('https://www.wikipedia.org');

		});
	
	});

	describe('when filling out an attribute of type valuta', ( ) => {

		let attribute = $('.attribute-type-valuta');

		beforeAll(( ) => {

			inputValuta(attribute, '12.34' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('12.34');

		});

	});

	describe('when filling out an attribute of type valuta ex 19', ( ) => {

		let attribute = $('.attribute-type-valutaex');

		beforeAll(( ) => {

			inputValuta(attribute, '23.45' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('23.45');

		});

		it('should display the taxed value', ( ) => {

			expect(attribute.$('zs-btw-display').getText()).toEqual('incl. btw: € 27,91');

		});

	});

	describe('when filling out an attribute of type valuta ex 21', ( ) => {

		let attribute = $('.attribute-type-valutaex21');

		beforeAll(( ) => {

			inputValuta(attribute, '34.56' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('34.56');

		});

		it('should display the taxed value', ( ) => {

			expect(attribute.$('zs-btw-display').getText()).toEqual('incl. btw: € 41,82');

		});

	});

	describe('when filling out an attribute of type valuta ex 6', ( ) => {

		let attribute = $('.attribute-type-valutaex6');

		beforeAll(( ) => {

			inputValuta(attribute, '45.67' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('45.67');

		});

		it('should display the taxed value', ( ) => {

			expect(attribute.$('zs-btw-display').getText()).toEqual('incl. btw: € 48,41');

		});

	});

	describe('when filling out an attribute of type valuta in 19', ( ) => {

		let attribute = $('.attribute-type-valutain');

		beforeAll(( ) => {

			inputValuta(attribute, '56.78' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('56.78');

		});

		it('should display the untaxed value', ( ) => {

			expect(attribute.$('zs-btw-display').getText()).toEqual('excl. btw: € 47,71');

		});

	});

	describe('when filling out an attribute of type valuta in 21', ( ) => {

		let attribute = $('.attribute-type-valutain21');

		beforeAll(( ) => {

			inputValuta(attribute, '67.89' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('67.89');

		});

		it('should display the untaxed value', ( ) => {

			expect(attribute.$('zs-btw-display').getText()).toEqual('excl. btw: € 56,11');

		});

	});

	describe('when filling out an attribute of type valuta in 6', ( ) => {

		let attribute = $('.attribute-type-valutain6');

		beforeAll(( ) => {

			inputValuta(attribute, '78.90' );

		});

		it('should have the given value', ( ) => {

			expect(attribute.$('.value-list input').getAttribute('value')).toEqual('78.90');

		});

		it('should display the untaxed value', ( ) => {

			expect(attribute.$('zs-btw-display').getText()).toEqual('excl. btw: € 74,43');

		});

	});

	afterAll(( ) => {

		waitForSave();

	});

});
