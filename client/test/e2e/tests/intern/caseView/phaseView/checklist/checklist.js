import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';
import caseChecklist from './../../../../../functions/intern/caseView/caseChecklist';

describe('when opening case 71', ( ) => {

	let sidebar = $('.phase-sidebar'),
		checklist = $('.phase-sidebar .check-list');

	beforeAll(( ) => {

		navigateAs('admin', 71);

	});

	describe('and when inspecting the checklist in the current phase', ( ) => {

		beforeAll(( ) => {

			caseNav.openChecklist();

		});

		it('the casetype checklist items should not be able to be deleted', ( ) => {

			expect(checklist.$('li:nth-child(1) .sidebar-item-action button').isDisplayed()).toBe(false);
			expect(checklist.$('li:nth-child(2) .sidebar-item-action button').isDisplayed()).toBe(false);
			expect(checklist.$('li:nth-child(3) .sidebar-item-action button').isDisplayed()).toBe(false);

		});

		it('the manual checklist items should be able to be deleted', ( ) => {

			expect(checklist.$('li:nth-child(4) .sidebar-item-action button').isDisplayed()).toBe(true);
			expect(checklist.$('li:nth-child(5) .sidebar-item-action button').isDisplayed()).toBe(true);
			expect(checklist.$('li:nth-child(6) .sidebar-item-action button').isDisplayed()).toBe(true);

		});

		it('the checklist item counter should be equal to the number of unchecked checklist items', ( ) => {

			expect(sidebar.$('.phase-sidebar-header-inner li:nth-child(2) .phase-sidebar-tab-counter').getText()).toEqual('4');

		});

		describe('and when deleting a checklist item', ( ) => {

			beforeAll(( ) => {

				caseChecklist.delete('6');

			});

			it('the deleted checklist item should not be present', ( ) => {

				element.all(by.css('li .sidebar-item-title')).getText().then( (itemTitle) => {
					expect(itemTitle).not.toContain('Handmatig Drie');
				});

			});

			afterAll(( ) => {

				caseChecklist.create('Handmatig Drie');

			});

		});

		describe('and when adding a checklist item', ( ) => {

			beforeAll(( ) => {

				caseChecklist.create('Automatisch Diakriët');

			});

			it('the added checklist item should be present', ( ) => {

				element.all(by.css('li .sidebar-item-title')).getText().then( (itemTitle) => {
					expect(itemTitle).toContain('Automatisch Diakriët');
				});

			});

			it('the added checklist item should be unchecked', ( ) => {

				expect(checklist.$('li:nth-child(7) input[checked="checked"]').isPresent()).toBe(false);

			});

			afterAll(( ) => {

				caseChecklist.delete('7');

			});

		});

		describe('and when checking checklist items', ( ) => {

			beforeAll(( ) => {

				caseChecklist.toggle(3);

				caseChecklist.toggle(4);

			});

			it('the checked checklist items should be checked', ( ) => {

				expect(checklist.$('li:nth-child(3) input[checked="checked"]').isPresent()).toBe(true);
				expect(checklist.$('li:nth-child(4) input[checked="checked"]').isPresent()).toBe(true);

			});

			it('the checklist counter should be updated', ( ) => {

				expect(sidebar.$('.phase-sidebar-header-inner li:nth-child(2) .phase-sidebar-tab-counter').getText()).toEqual('2');

			});

			describe('and when checking all checklist items', ( ) => {

				beforeAll(( ) => {

					caseChecklist.toggle(1);

					caseChecklist.toggle(6);

				});

				it('the checklist counter should be hidden', ( ) => {

					expect(sidebar.$('.phase-sidebar-header-inner li:nth-child(2) .phase-sidebar-tab-counter').isDisplayed()).toBe(false);

				});

				afterAll(( ) => {

					caseChecklist.toggle(1);

					caseChecklist.toggle(6);

				});

			});

			afterAll(( ) => {

				caseChecklist.toggle(3);

				caseChecklist.toggle(4);

			});

		});

		describe('and when unchecking checklist items', ( ) => {

			beforeAll(( ) => {

				caseChecklist.toggle(2);

				caseChecklist.toggle(5);

			});

			it('the unchecked checklist items should not be checked', ( ) => {

				expect(checklist.$('li:nth-child(2) input[checked="checked"]').isPresent()).toBe(false);
				expect(checklist.$('li:nth-child(5) input[checked="checked"]').isPresent()).toBe(false);

			});

			it('the checklist counter should be updated', ( ) => {

				expect(sidebar.$('.phase-sidebar-header-inner li:nth-child(2) .phase-sidebar-tab-counter').getText()).toEqual('6');

			});

			afterAll(( ) => {

				caseChecklist.toggle(2);

				caseChecklist.toggle(5);

			});

		});

		afterAll(( ) => {

			caseNav.openPhaseActions();

		});

	});

	describe('and when inspecting the checklist in the resolved phase', ( ) => {

		let checklistItemCheckboxes = element.all(by.css('.phase-sidebar .check-list input[type="checkbox"]')),
			checklistItemActions = element.all(by.css('.phase-sidebar .check-list .sidebar-item-action button'));

		beforeAll(( ) => {

			caseNav.openPhase(2);

			caseNav.openChecklist();

		});

		it('the checklist items should be disabled', ( ) => {

			checklistItemCheckboxes.each((checkbox) => {

				expect(checkbox.isEnabled()).toBe(false);

			});

		});

		it('the checklist items should not be deletable', ( ) => {

			checklistItemActions.each((actionButton) => {

				expect(actionButton.isDisplayed()).toBe(false);

			});

		});

		it('there should be no input to add checklist items with', ( ) => {

			expect($('.checklist-item-add').isDisplayed()).toBe(false);

		});

		afterAll(( ) => {

			caseNav.openPhase(3);

		});

	});

});
