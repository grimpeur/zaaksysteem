import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';
import waitForSave from './../../../../../functions/intern/caseView/waitForSave';

describe('when opening the case with AND and OR testscenarios', ( ) => {

	let and1 = $('[data-name="voorwaarde_and_1"]'),
		and2 = $('[data-name="voorwaarde_and_2"]'),
		or1 = $('[data-name="voorwaarde_or_1"]'),
		or2 = $('[data-name="voorwaarde_or_2"]'),
		andResult = $('[data-name="voorwaarde_and_resultaat"] .value-list'),
		orResult = $('[data-name="voorwaarde_or_resultaat"] .value-list');

	beforeAll(( ) => {

		navigateAs('admin', 64);

		caseNav.openPhase('1');

	});

	describe('and setting the AND conditions to no and no', ( ) => {
	
		beforeAll(( ) => {
	
			and1.$('[value="Nee"]').click();
			and2.$('[value="Nee"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(andResult.getText()).toEqual('False');
	
		});
	
	});

	describe('and setting the AND conditions to yes and no', ( ) => {
	
		beforeAll(( ) => {
	
			and1.$('[value="Ja"]').click();
			and2.$('[value="Nee"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(andResult.getText()).toEqual('False');
	
		});
	
	});

	describe('and setting the AND conditions to no and yes', ( ) => {
	
		beforeAll(( ) => {
	
			and1.$('[value="Nee"]').click();
			and2.$('[value="Ja"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(andResult.getText()).toEqual('False');
	
		});
	
	});

	describe('and setting the AND conditions to yes and yes', ( ) => {
	
		beforeAll(( ) => {
	
			and1.$('[value="Ja"]').click();
			and2.$('[value="Ja"]').click();
	
		});
	
		it('the result of the rule should be true', ( ) => {
	
			expect(andResult.getText()).toEqual('True');
	
		});
	
	});

	describe('and setting the OR conditions to no and no', ( ) => {
	
		beforeAll(( ) => {
	
			or1.$('[value="Nee"]').click();
			or2.$('[value="Nee"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(orResult.getText()).toEqual('False');
	
		});
	
	});

	describe('and setting the OR conditions to yes and no', ( ) => {
	
		beforeAll(( ) => {
	
			or1.$('[value="Ja"]').click();
			or2.$('[value="Nee"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(orResult.getText()).toEqual('True');
	
		});
	
	});

	describe('and setting the OR conditions to no and yes', ( ) => {
	
		beforeAll(( ) => {
	
			or1.$('[value="Nee"]').click();
			or2.$('[value="Ja"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(orResult.getText()).toEqual('True');
	
		});
	
	});

	describe('and setting the OR conditions to yes and yes', ( ) => {
	
		beforeAll(( ) => {
	
			or1.$('[value="Ja"]').click();
			or2.$('[value="Ja"]').click();
	
		});
	
		it('the result of the rule should be true', ( ) => {
	
			expect(orResult.getText()).toEqual('True');
	
		});
	
	});

	afterAll(( ) => {

		waitForSave();

	});

});
