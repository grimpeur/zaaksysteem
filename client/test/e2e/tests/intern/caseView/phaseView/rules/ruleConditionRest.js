import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';

describe('when opening the case with all basic casetype settings off', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 62);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="algemeen_vertrouwelijkheidsaanduiding"] .value-list').getText()).toEqual('Geheim');
		expect($('[data-name="algemeen_bezwaar_en_beroep_mogelijk"] .value-list').getText()).toEqual('True');
		expect($('[data-name="algemeen_publicatie"] .value-list').getText()).toEqual('True');
		expect($('[data-name="algemeen_bag"] .value-list').getText()).toEqual('True');
		expect($('[data-name="algemeen_lex_silencio_positivo"] .value-list').getText()).toEqual('True');
		expect($('[data-name="algemeen_opschorten_mogelijk"] .value-list').getText()).toEqual('True');
		expect($('[data-name="algemeen_verlengen_mogelijk"] .value-list').getText()).toEqual('True');
		expect($('[data-name="algemeen_wet_dwangsom"] .value-list').getText()).toEqual('True');
		expect($('[data-name="algemeen_wkpb"] .value-list').getText()).toEqual('True');

	});

});

describe('when opening the case with all basic casetype settings on', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 63);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="algemeen_vertrouwelijkheidsaanduiding"] .value-list').getText()).toEqual('Uit');
		expect($('[data-name="algemeen_bezwaar_en_beroep_mogelijk"] .value-list').getText()).toEqual('False');
		expect($('[data-name="algemeen_publicatie"] .value-list').getText()).toEqual('False');
		expect($('[data-name="algemeen_bag"] .value-list').getText()).toEqual('False');
		expect($('[data-name="algemeen_lex_silencio_positivo"] .value-list').getText()).toEqual('False');
		expect($('[data-name="algemeen_opschorten_mogelijk"] .value-list').getText()).toEqual('False');
		expect($('[data-name="algemeen_verlengen_mogelijk"] .value-list').getText()).toEqual('False');
		expect($('[data-name="algemeen_wet_dwangsom"] .value-list').getText()).toEqual('False');
		expect($('[data-name="algemeen_wkpb"] .value-list').getText()).toEqual('False');

	});

});

