import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';

describe('when opening the case with confidentiality public', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 59);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="vertrouwelijkheid_openbaar"] .value-list').getText()).toEqual('True');
		expect($('[data-name="vertrouwelijkheid_intern"] .value-list').getText()).toEqual('False');
		expect($('[data-name="vertrouwelijkheid_vertrouwelijk"] .value-list').getText()).toEqual('False');

	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});

describe('when opening the case with confidentiality internal', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 60);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="vertrouwelijkheid_openbaar"] .value-list').getText()).toEqual('False');
		expect($('[data-name="vertrouwelijkheid_intern"] .value-list').getText()).toEqual('True');
		expect($('[data-name="vertrouwelijkheid_vertrouwelijk"] .value-list').getText()).toEqual('False');

	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});

describe('when opening the case with confidentiality internal', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 61);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="vertrouwelijkheid_openbaar"] .value-list').getText()).toEqual('False');
		expect($('[data-name="vertrouwelijkheid_intern"] .value-list').getText()).toEqual('False');
		expect($('[data-name="vertrouwelijkheid_vertrouwelijk"] .value-list').getText()).toEqual('True');

	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});

