import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';
import waitForSave from './../../../../../functions/intern/caseView/waitForSave';

describe('when opening the case with the show and hide testscenarios', ( ) => {

	let choice1 = $('[data-name="toon_verberg_1_keuze"]'),
		choice2 = $('[data-name="toon_verberg_2_keuze"]'),
		text3 = $('[data-name="toon_verberg_3_tekstveld"]'),
		choice4 = $('[data-name="toon_verberg_4_keuze"]'),
		choice5 = $('[data-name="toon_verberg_5_keuze"]'),
		text6 = $('[data-name="toon_verberg_6_tekstveld"]'),
		choiceB1 = $('[data-name="toon_verberg_blok_1_keuze"]'),
		choiceB2 = $('[data-name="toon_verberg_blok_2_keuze"]'),
		block1 = $('[data-name="text_block_828"]'),
		choiceG1 = $('[data-name="toon_verberg_groep_1_keuze"]'),
		choiceG2 = $('[data-name="toon_verberg_groep_2_keuze"]'),
		textG3 = $('[data-name="toon_verberg_groep_3_tekstveld"]'),
		choiceG4 = $('[data-name="toon_verberg_groep_4_keuze"]'),
		choiceG5 = $('[data-name="toon_verberg_groep_5_keuze"]'),
		textG6 = $('[data-name="toon_verberg_groep_6_tekstveld"]');

	beforeAll(( ) => {

		navigateAs('admin', 66);

		caseNav.openPhase('1');

	});

	describe('and hiding the later shown attribute 3', ( ) => {
	
		beforeAll(( ) => {

			text3.$('input').sendKeys('Test');
			choice2.$('[value="Ja"]').click();
			choice1.$('[value="Nee"]').click();
	
		});
	
		it('attribute 3 should not be hidden', ( ) => {
	
			expect(text3.isPresent()).toBe(true);
	
		});

		it('attribute 3 should still have its value', ( ) => {
	
			expect(text3.$('input').getAttribute('value')).toEqual('Test');
	
		});

		describe('and stop showing attribute 3', ( ) => {
		
			beforeAll(( ) => {
			
				choice2.$('[value="Nee"]').click();
			
			});
		
			it('attribute 3 should be hidden', ( ) => {
	
				expect(text3.isPresent()).toBe(false);
		
			});
		
			describe('and when showing attribute 3 again', ( ) => {
			
				beforeAll(( ) => {
				
					choice2.$('[value="Ja"]').click();
				
				});
			
				it('attribute 3 should not be hidden', ( ) => {
	
					expect(text3.isPresent()).toBe(true);
			
				});

				it('attribute 3 should have lost its value', ( ) => {
			
					expect(text3.$('input').getAttribute('value')).toEqual('');
			
				});
			
			});
		
		});
	
	});

	describe('and showing systemattribute 5, filling it with yes, triggering the showing of attribute 6', ( ) => {
	
		beforeAll(( ) => {
		
			choice4.$('[value="Ja"]').click();
		
		});
	
		it('attribute 5 should be hidden', ( ) => {
	
			expect(choice5.isPresent()).toBe(false);
	
		});

		it('attribute 6 should not be hidden', ( ) => {
	
			expect(text6.isPresent()).toBe(true);
	
		});

		describe('and when hiding systemattribute 5', ( ) => {
		
			beforeAll(( ) => {
			
				choice4.$('[value="Nee"]').click();
			
			});
		
			it('attribute 6 should be hidden', ( ) => {
	
				expect(text6.isPresent()).toBe(false);
		
			});
		
		});
	
	});

	describe('and when hiding text block 1', ( ) => {
	
		beforeAll(( ) => {
		
			choiceB1.$('[value="Nee"]').click();
		
		});
	
		it('text block 1 should be hidden', ( ) => {
	
			expect(block1.isPresent()).toBe(false);
	
		});
	
		describe('and when showing text block 1', ( ) => {
		
			beforeAll(( ) => {
			
				choiceB2.$('[value="Ja"]').click();
			
			});
		
			it('text block 1 should be shown', ( ) => {
	
				expect(block1.isPresent()).toBe(true);
		
			});
		
		});
	
	});

	describe('and hiding the later shown group 3', ( ) => {
	
		beforeAll(( ) => {

			textG3.$('input').sendKeys('Test');
			choiceG2.$('[value="Ja"]').click();
			choiceG1.$('[value="Nee"]').click();
	
		});
	
		it('attribute 3 should not be hidden', ( ) => {
	
			expect(textG3.isPresent()).toBe(true);
	
		});

		it('attribute 3 should still have its value', ( ) => {
	
			expect(textG3.$('input').getAttribute('value')).toEqual('Test');
	
		});

		describe('and stop showing group 3', ( ) => {
		
			beforeAll(( ) => {
			
				choiceG2.$('[value="Nee"]').click();
			
			});
		
			it('attribute 3 should be hidden', ( ) => {
	
				expect(textG3.isPresent()).toBe(false);
		
			});
		
			describe('and when showing group 3 again', ( ) => {
			
				beforeAll(( ) => {
				
					choiceG2.$('[value="Ja"]').click();
				
				});
			
				it('attribute 3 should not be hidden', ( ) => {
	
					expect(textG3.isPresent()).toBe(true);
			
				});

				it('attribute 3 should have lost its value', ( ) => {
			
					expect(textG3.$('input').getAttribute('value')).toEqual('');
			
				});
			
			});
		
		});
	
	});

	describe('and showing group 5, filling it with yes, triggering the showing of attribute 6', ( ) => {
	
		beforeAll(( ) => {
		
			choiceG4.$('[value="Ja"]').click();
		
		});
	
		it('attribute 5 should be hidden', ( ) => {
	
			expect(choiceG5.isPresent()).toBe(false);
	
		});

		it('attribute 6 should not be hidden', ( ) => {
	
			expect(textG6.isPresent()).toBe(true);
	
		});

		describe('and when hiding group 5', ( ) => {
		
			beforeAll(( ) => {
			
				choiceG4.$('[value="Nee"]').click();
			
			});
		
			it('attribute 6 should be hidden', ( ) => {
	
				expect(textG6.isPresent()).toBe(false);
		
			});
		
		});
	
	});

	afterAll(( ) => {

		waitForSave();

	});

});
