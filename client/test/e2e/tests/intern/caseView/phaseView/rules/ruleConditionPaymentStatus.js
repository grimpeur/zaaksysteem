import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';

describe('when opening the case with a completed payment', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 55);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="betaalstatus_wachten_op_betaling"] .value-list').getText()).toEqual('False');
		expect($('[data-name="betaalstatus_voltooid"] .value-list').getText()).toEqual('True');

	});

});

describe('when opening the case with a pending payment', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 56);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="betaalstatus_wachten_op_betaling"] .value-list').getText()).toEqual('True');
		expect($('[data-name="betaalstatus_voltooid"] .value-list').getText()).toEqual('False');

	});

});
