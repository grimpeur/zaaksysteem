import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';
import inputSelectFirst from './../../../../../functions/common/input/inputSelectFirst';
import waitForSave from './../../../../../functions/intern/caseView/waitForSave';

describe('when opening the case with attribute type testscenarios', ( ) => {

	let evk = $('[data-name="voorwaarde_enkelvoudige_keuze"]'),
		mvk = $('[data-name="voorwaarde_meervoudige_keuze"]'),
		listSelect = $('[data-name="voorwaarde_keuzelijst"] select'),
		addressDataName = 'voorwaarde_adres_postcode',
		evkResult = $('[data-name="voorwaarde_enkelvoudige_keuze_resultaat"] .value-list'),
		mvkResult = $('[data-name="voorwaarde_meervoudige_keuze_resultaat"] .value-list'),
		listResult = $('[data-name="voorwaarde_keuzelijst_resultaat"] .value-list'),
		addressResult = $('[data-name="voorwaarde_adres_postcode_resultaat"] .value-list'),
		attributeList = $('.vorm-field-list'),
		addressClear = $('[data-name="voorwaarde_adres_postcode"] .mdi-close');

	beforeAll(( ) => {

		navigateAs('admin', 65);

		caseNav.openPhase('1');

	});

	describe('and setting the evk to word', ( ) => {
	
		beforeAll(( ) => {
	
			evk.$('[value="Woord"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(evkResult.getText()).toEqual('Woord');
	
		});
	
	});

	describe('and setting the evk to word with extra tekst', ( ) => {
	
		beforeAll(( ) => {
	
			evk.$('[value="Woord met extra tekst"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(evkResult.getText()).toEqual('Woord met extra tekst');
	
		});
	
	});

	describe('and setting the evk to zero', ( ) => {
	
		beforeAll(( ) => {
	
			evk.$('[value="0"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(evkResult.getText()).toEqual('Nul');
	
		});
	
	});

	describe('and setting the evk to one', ( ) => {
	
		beforeAll(( ) => {
	
			evk.$('[value="1"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(evkResult.getText()).toEqual('Een');
	
		});
	
	});

	describe('and setting the evk to diacritic', ( ) => {
	
		beforeAll(( ) => {
	
			evk.$('[value="Diakriët"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(evkResult.getText()).toEqual('Diakriët');
	
		});
	
	});

	describe('and setting the mvk to word', ( ) => {
	
		beforeAll(( ) => {
	
			mvk.$('[value="Woord"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(mvkResult.getText()).toEqual('Woord');
	
		});
	
	});

	describe('and setting the mvk to word with extra tekst', ( ) => {
	
		beforeAll(( ) => {
	
			mvk.$('[value="Woord met extra tekst"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(mvkResult.getText()).toEqual('Woord met extra tekst');
	
		});
	
	});

	describe('and setting the mvk to zero', ( ) => {
	
		beforeAll(( ) => {
	
			mvk.$('[value="0"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(mvkResult.getText()).toEqual('Nul');
	
		});
	
	});

	describe('and setting the mvk to one', ( ) => {
	
		beforeAll(( ) => {
	
			mvk.$('[value="1"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(mvkResult.getText()).toEqual('Een');
	
		});
	
	});

	describe('and setting the mvk to diacritic', ( ) => {
	
		beforeAll(( ) => {
	
			mvk.$('[value="Diakriët"]').click();
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(mvkResult.getText()).toEqual('Diakriët');
	
		});
	
	});

	describe('and setting the list to word', ( ) => {
	
		beforeAll(( ) => {
	
			listSelect.sendKeys('Woord');
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(listResult.getText()).toEqual('Woord');
	
		});

		afterAll(( ) => {

			listResult.click();

		});
	
	});

	describe('and setting the list to word with extra tekst', ( ) => {
	
		beforeAll(( ) => {
	
			listSelect.sendKeys('Woord met extra tekst');
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(listResult.getText()).toEqual('Woord met extra tekst');
	
		});
	
		afterAll(( ) => {

			listResult.click();

		});

	});

	describe('and setting the list to zero', ( ) => {
	
		beforeAll(( ) => {
	
			listSelect.sendKeys('0');
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(listResult.getText()).toEqual('Nul');
	
		});

		afterAll(( ) => {

			listResult.click();

		});
	
	});

	describe('and setting the list to one', ( ) => {
	
		beforeAll(( ) => {
	
			listSelect.sendKeys('1');
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(listResult.getText()).toEqual('Een');
	
		});

		afterAll(( ) => {

			listResult.click();

		});
	
	});

	describe('and setting the list to diacritic', ( ) => {
	
		beforeAll(( ) => {
	
			listSelect.sendKeys('Diakriët');
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(listResult.getText()).toEqual('Diakriët');
	
		});

		afterAll(( ) => {

			listResult.click();

		});
	
	});

	describe('and setting the address to lower than 6343AC', ( ) => {
	
		beforeAll(( ) => {
	
			inputSelectFirst(attributeList, addressDataName, '6343AB');
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(addressResult.getText()).toEqual('Lower than 6343AC');
	
		});

		afterAll(( ) => {

			addressClear.click();

		});
	
	});

	describe('and setting the address to 6343AC', ( ) => {
	
		beforeAll(( ) => {
	
			inputSelectFirst(attributeList, addressDataName, '6343AC');
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(addressResult.getText()).toEqual('Exact 6343AC');
	
		});

		afterAll(( ) => {

			addressClear.click();

		});
	
	});

	describe('and setting the address to higher than 6343AC', ( ) => {
	
		beforeAll(( ) => {
	
			inputSelectFirst(attributeList, addressDataName, '6343AD');
	
		});
	
		it('the result of the rule should be false', ( ) => {
	
			expect(addressResult.getText()).toEqual('Lower than 6343AC');
	
		});

		afterAll(( ) => {

			addressClear.click();

		});
	
	});

	afterAll(( ) => {

		waitForSave();

	});

});
