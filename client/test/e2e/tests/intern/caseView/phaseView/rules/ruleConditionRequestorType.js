import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';

describe('when opening the case with the citizen as requestor', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 50);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="aanvrager_type_00"] .value-list').getText()).toEqual('True');
		expect($('[data-name="aanvrager_type_01"] .value-list').getText()).toEqual('False');
		expect($('[data-name="aanvrager_type_10"] .value-list').getText()).toEqual('True');
		expect($('[data-name="aanvrager_type_11"] .value-list').getText()).toEqual('True');

	});

});

describe('when opening the case with the company as requestor', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 51);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="aanvrager_type_00"] .value-list').getText()).toEqual('True');
		expect($('[data-name="aanvrager_type_01"] .value-list').getText()).toEqual('True');
		expect($('[data-name="aanvrager_type_10"] .value-list').getText()).toEqual('False');
		expect($('[data-name="aanvrager_type_11"] .value-list').getText()).toEqual('True');

	});

});

describe('when opening the case with the employee as requestor', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 52);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="aanvrager_type_00"] .value-list').getText()).toEqual('True');
		expect($('[data-name="aanvrager_type_01"] .value-list').getText()).toEqual('True');
		expect($('[data-name="aanvrager_type_10"] .value-list').getText()).toEqual('False');
		expect($('[data-name="aanvrager_type_11"] .value-list').getText()).toEqual('True');

	});

});
