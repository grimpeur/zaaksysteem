import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';

describe('when opening the case with the zipcode requestor', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 53);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="aanvrager_postcode_kleiner"] .value-list').getText()).toEqual('False');
		expect($('[data-name="aanvrager_postcode_exact"] .value-list').getText()).toEqual('True');
		expect($('[data-name="aanvrager_postcode_groter"] .value-list').getText()).toEqual('False');
		expect($('[data-name="aanvrager_postcode_omvat"] .value-list').getText()).toEqual('True');

	});

});
