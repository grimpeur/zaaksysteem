import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';

describe('when opening the case with pause testscenarios', ( ) => {

	let choice = $('[data-name="pauze_keuze"]'),
		placeholder1 = $('[data-name="placeholder_1"]'),
		pauseMessage = $('zs-case-pause-application');

	beforeAll(( ) => {

		navigateAs('admin', 68);

		caseNav.openPhase('1');

	});

	describe('and activating the pause', ( ) => {
	
		beforeAll(( ) => {
	
			choice.$('[value="Pauze normaal"]').click();
	
		});

		it('the pause message should not be present', ( ) => {
	
			expect(pauseMessage.isPresent()).toBe(false);
	
		});
	
		it('the attribute following the pausing attribute should be present', ( ) => {
	
			expect(placeholder1.isPresent()).toBe(true);
	
		});
	
	});

});
