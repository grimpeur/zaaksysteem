import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';

// For efficiency this testscenario only includes two out of seven options.
// Others can be included in the future should the need for them arise.

describe('when opening the case with the zipcode requestor', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 57);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="contactkanaal_behandelaar"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_balie"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_telefoon"] .value-list').getText()).toEqual('True');
		expect($('[data-name="contactkanaal_post"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_email"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_webformulier"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_sociale_media"] .value-list').getText()).toEqual('False');

	});

});

describe('when opening the case with the zipcode requestor', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 58);

		caseNav.openPhase('1');

		$('[data-name="boolean"] [value="Ja"]').click();

	});

	it('the attributes should have the correct values', ( ) => {

		expect($('[data-name="contactkanaal_behandelaar"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_balie"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_telefoon"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_post"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_email"] .value-list').getText()).toEqual('False');
		expect($('[data-name="contactkanaal_webformulier"] .value-list').getText()).toEqual('True');
		expect($('[data-name="contactkanaal_sociale_media"] .value-list').getText()).toEqual('False');

	});

});

