import navigateAs from './../../../../../functions/common/navigateAs';
import caseNav from './../../../../../functions/intern/caseView/caseNav';
import waitForSave from './../../../../../functions/intern/caseView/waitForSave';

describe('when opening the case with set value scenarios', ( ) => {

	let setValue1 = $('[data-name="vul_waarde_in_1_keuze"]'),
		setValue2 = $('[data-name="vul_waarde_in_2_keuze"]'),
		setValue3 = $('[data-name="vul_waarde_in_3_tekst"]'),
		setValue4 = $('[data-name="vul_waarde_in_4_keuze"]'),
		setValue5 = $('[data-name="vul_waarde_in_5_tekst"]'),
		setValue6 = $('[data-name="vul_waarde_in_6_keuze"]'),
		setFormula1 = $('[data-name="vul_formule_in_1_keuze"]'),
		setFormula2 = $('[data-name="vul_formule_in_2_tekst"]'),
		setFormula3 = $('[data-name="vul_formule_in_3_valuta"]'),
		setFormula4 = $('[data-name="vul_formule_in_4_tekst"]'),
		setFormula5 = $('[data-name="vul_formule_in_5_valuta"]'),
		setFormula6 = $('[data-name="vul_formule_in_6_tekst"]'),
		setFormula7 = $('[data-name="vul_formule_in_7_valuta"]'),
		setFormula8 = $('[data-name="vul_formule_in_8_keuze"]'),
		setFormula9 = $('[data-name="vul_formule_in_9_tekst"]'),
		result1 = $('.result-options li:nth-child(2)');

	beforeAll(( ) => {

		navigateAs('admin', 69);

		caseNav.openPhase('1');

	});

	describe('and activating the set values', ( ) => {

		beforeAll(( ) => {

			setValue1.$('[value="Ja"]').click();

		});

		it('the value should be what was set and read-only', ( ) => {

			expect(setValue2.$('.value-list').getText()).toEqual('Ja');

		});

		it('the followup value should be what was set and read-only', ( ) => {

			expect(setValue3.$('.value-list').getText()).toEqual('Tekst');

		});

		it('the value should be what was set and not read-only', ( ) => {

			expect(setValue4.$('vorm-radio-group label:nth-child(1) input').isSelected()).toBe(true);

		});

		it('the followup value should be what was set and not read-only', ( ) => {

			expect(setValue5.$('input').getAttribute('value')).toEqual('Tekst');

		});

		describe('and when deactivating the set values', ( ) => {

			beforeAll(( ) => {

				setValue5.$('input').sendKeys(' en nog meer tekst');

				setValue4.$('[value="Nee"]').click();

				setValue1.$('[value="Nee"]').click();

			});

			it('the followup value should be what was set and not read-only', ( ) => {

				expect(setValue2.$('vorm-radio-group label:nth-child(1) input').isSelected()).toBe(true);

			});

			it('the followup value should be what was set and not read-only', ( ) => {

				expect(setValue4.$('vorm-radio-group label:nth-child(2) input').isSelected()).toBe(true);

			});

			it('the followup value should be what was set and not read-only', ( ) => {

				expect(setValue5.$('input').getAttribute('value')).toEqual('Tekst en nog meer tekst');

			});

		});

	});

	describe('and when activating the result rule', ( ) => {

		beforeAll(( ) => {

			setValue6.$('[value="Ja"]').click();

			caseNav.openPhase('2');

		});

		it('the result should be set', ( ) => {

			expect(result1.$('input[checked="checked"]').isPresent()).toBe(true);

		});

		it('the result should be read-only', ( ) => {

			expect(result1.$('input[disabled="disabled"]').isPresent()).toBe(true);

		});

		describe('and when deactivating the result rule', ( ) => {

			beforeAll(( ) => {

				caseNav.openPhase('1');

				browser.sleep(500);

				setValue6.$('[value="Nee"]').click();

				caseNav.openPhase('2');

			});

			it('the result should be set', ( ) => {

				expect(result1.$('input[checked="checked"]').isPresent()).toBe(true);

			});

			it('the result should not be read-only', ( ) => {

				expect(result1.$('input[disabled="disabled"]').isPresent()).toBe(false);

			});

		});

		afterAll(( ) => {

			caseNav.openPhase('1');

		});

	});

	describe('and activating the formula rules', ( ) => {

		beforeAll(( ) => {

			setFormula2.$('input').sendKeys('1.2345');

			setFormula3.$('input').sendKeys('1.23');

			setFormula1.$('[value="Ja"]').click();

		});

		it('the result should be 0 and read-only', ( ) => {

			expect(setFormula4.$('.value-list').getText()).toEqual('0');

		});

		it('the result should be 0,00 and read-only', ( ) => {

			expect(setFormula5.$('.value-list').getText()).toEqual('€ 0,00');

		});

		it('the result should be 1.518435 and read-only', ( ) => {

			expect(setFormula6.$('.value-list').getText()).toEqual('1.518435');

		});

		it('the result should be 1.52 and read-only', ( ) => {

			expect(setFormula7.$('.value-list').getText()).toEqual('€ 1,52');

		});

		it('the result should be 2.467 and read-only', ( ) => {

			expect(setFormula8.$('.value-list').getText()).toEqual('2.46');

		});

		it('the followup field should become displayed', ( ) => {

			expect(setFormula9.isPresent()).toBe(true);

		});

		describe('and when deactivating the formula rules', ( ) => {

			beforeAll(( ) => {

				setFormula3.$('input').clear().sendKeys('2.46');

				waitForSave();

			});

			it('the result should be 4.92 and read-only', ( ) => {

				expect(setFormula6.$('.value-list').getText()).toEqual('3.03687');

			});

			it('the result should be 4.92 and read-only', ( ) => {

				expect(setFormula7.$('.value-list').getText()).toEqual('€ 3,04');

			});

			it('the result should be 4.92 and read-only', ( ) => {

				expect(setFormula8.$('.value-list').getText()).toEqual('4.92');

			});

			it('the followup field should become hidden', ( ) => {

				expect(setFormula9.isPresent()).toBe(false);

			});

			describe('and when reloading the page', ( ) => {

				beforeAll(( ) => {

					browser.get('/intern/zaak/69/fase/registreren/');

				});

				it('the result should be 4.92 and read-only', ( ) => {

					expect(setFormula6.$('.value-list').getText()).toEqual('3.03687');

				});

				it('the result should be 4.92 and read-only', ( ) => {

					expect(setFormula7.$('.value-list').getText()).toEqual('€ 3,04');

				});

				it('the result should be 4.92 and read-only', ( ) => {

					expect(setFormula8.$('.value-list').getText()).toEqual('4.92');

				});

			});

		});

	});

});
