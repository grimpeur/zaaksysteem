import navigateAs from './../../../../functions/common/navigateAs';
import caseMenu from './../../../../functions/intern/caseView/caseMenu';

describe('when opening case 72', ( ) => {

	let attrValue = $('[data-name="vertrouwelijkheidsniveau"] .value-item');

	beforeAll(( ) => {
	
		navigateAs('admin', 72);
	
	});

	describe('and when updating the confidentiality to confidential', ( ) => {
	
		beforeAll(( ) => {
		
			caseMenu.updateConfidentiality('vertrouwelijk');
		
		});

		it('the confidentiality should have changed to confidential', ( ) => {

			expect(caseMenu.getAboutValue('Openbaarheid')).toEqual('Vertrouwelijk');
	
		});

		it('the rules should have updated using the confidential confidentiality', ( ) => {
		
			expect(attrValue.getText()).toEqual('Vertrouwelijk');
	
		});
	
		describe('and when updating the confidentiality to internal', ( ) => {
		
			beforeAll(( ) => {
		
				caseMenu.updateConfidentiality('intern');

			});
		
			it('the confidentiality should have changed to internal', ( ) => {

				expect(caseMenu.getAboutValue('Openbaarheid')).toEqual('Intern');
	
			});

			it('the rules should have updated using the internal confidentiality', ( ) => {
			
				expect(attrValue.getText()).toEqual('Intern');
		
			});

			describe('and when updating the confidentiality to public', ( ) => {
			
				beforeAll(( ) => {
		
					caseMenu.updateConfidentiality('openbaar');

				});
			
				it('the confidentiality should have changed to public', ( ) => {

					expect(caseMenu.getAboutValue('Openbaarheid')).toEqual('Openbaar');

				});

				it('the rules should have updated using the public confidentiality', ( ) => {
				
					expect(attrValue.getText()).toEqual('Openbaar');
			
				});
			
			});

		});
	
	});

});
