import navigateAs from './../../../functions/common/navigateAs';
import plusMenu from './../../../functions/intern/plusMenu';
import waitForElement from './../../../functions/common/waitForElement';

describe('when starting a case registration', ( ) => {

	beforeAll(( ) => {

		navigateAs();

		let newCase = {
			casetype: 'basic casetype',
			requestorType: 'natuurlijk_persoon',
			requestor: '123456789'
		};

		plusMenu.createCase(newCase);

	});

	it('should redirect to zaak/create', ( ) => {

		expect(browser.getCurrentUrl()).toContain('/zaak/create/balie');

	});

	describe('and completing the steps', ( ) => {

		beforeAll(( ) => {

			$('[name=submit_to_next]').click();

			browser.driver.wait(( ) => {
				return browser.driver.getCurrentUrl().then((url) => {
					return url.indexOf('submit_to_next=1') !== -1;
				});
			}, 5000);

		});

		it('should move to the next phase', ( ) => {

			expect(browser.getCurrentUrl()).toContain('submit_to_next=1');

		});

		describe('and submitting the form', ( ) => {

			beforeAll(( ) => {

				$('[name=submit_to_next]').click();

			});

			it('should redirect to the case', ( ) => {

				waitForElement('.case-view');

				expect(browser.getCurrentUrl()).toContain('/intern/zaak');

			});

		});

	});

});
