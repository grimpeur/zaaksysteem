import navigateAs from './../../../functions/common/navigateAs';
import plusMenu from './../../../functions/intern/plusMenu';
import caseNav from './../../../functions/intern/caseView/caseNav';
import waitForElement from './../../../functions/common/waitForElement';

describe('when opening case 110', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 110);

	});

	describe('when using a template via the plusmenu to create an odt', ( ) => {

		beforeAll(( ) => {

			let data = {};

			plusMenu.useTemplate(data);

			caseNav.openTab('docs');

			waitForElement('.document-list-main');

		});

		it('there should be an odt in the documentstab', ( ) => {

			expect($('.document-list-main').getText()).toContain('Plusknop zaak.odt');

		});

		afterAll(( ) => {

			caseNav.openTab('phase');

		});

	});

	describe('when using a template via the plusmenu to create an pdf', ( ) => {

		beforeAll(( ) => {

			let data = {
				filetype: 'pdf'
			};

			plusMenu.useTemplate(data);

			caseNav.openTab('docs');

		});

		it('there should be a pdf in the documentstab', ( ) => {

			expect($('.document-list-main').getText()).toContain('Plusknop zaak.pdf');

		});

		afterAll(( ) => {

			caseNav.openTab('phase');

		});

	});

	describe('when using a template via the plusmenu to create a document as case document', ( ) => {

		beforeAll(( ) => {

			let data = {
				template: 'Plusknop zaak als zaakdocument',
				caseDocument: 'Document'
			};

			plusMenu.useTemplate(data);

			caseNav.openTab('docs');

		});

		it('there should be a document in the documentstab with the case document label', ( ) => {

			expect($('.document-list-main').getText()).toContain('Plusknop zaak als zaakdocument.odt\nDocument');

		});

		afterAll(( ) => {

			caseNav.openTab('phase');

		});

	});

});
