import navigateAs from './../../../functions/common/navigateAs';
import navigateTo from './../../../functions/common/navigateTo';
import plusMenu from './../../../functions/intern/plusMenu';
import waitForElement from './../../../functions/common/waitForElement';

describe('when opening case 110', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 110);

	});

	describe('when sending an e-mail via the plusmenu', ( ) => {

		let data = {
			recipientType: 'Aanvrager (T. Testpersoon)',
			subject: 'Test',
			content: 'Plusknop email standaard'
		};

		beforeAll(( ) => {

			plusMenu.sendEmail(data);

			browser.get('/intern/zaak/110/timeline/');

			browser.ignoreSynchronization = true;

			waitForElement('[data-event-type="email/send"]');

		});

		it('there should be a log of an email in the timeline', ( ) => {

			expect($('zs-case-timeline-view').getText()).toContain(data.content);

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			navigateTo(110);

		});

	});

	describe('when sending an e-mail template via the plusmenu', ( ) => {

		let data = {
			template: 'Plusknop zaak email',
			recipientType: 'Aanvrager (T. Testpersoon)'
		};

		beforeAll(( ) => {

			plusMenu.sendEmail(data);

			browser.get('/intern/zaak/110/timeline/');

			browser.ignoreSynchronization = true;

			waitForElement('[data-event-type="email/send"]');

		});

		it('there should be a log of an email in the timeline', ( ) => {

			expect($('zs-case-timeline-view').getText()).toContain('Plusknop zaak email - 110');

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			navigateTo(110);

		});

	});

	describe('when sending an e-mail with CC and BBC via the plusmenu', ( ) => {

		let data = {
			recipientType: 'Aanvrager (T. Testpersoon)',
			subject: 'Test',
			content: 'Plusknop email cc bcc',
			cc: 'plusknopcc@zaaksysteem.nl',
			bcc: 'plusknopbcc@zaaksysteem.nl'
		};

		beforeAll(( ) => {

			plusMenu.sendEmail(data);

			browser.get('/intern/zaak/110/timeline/');

			browser.ignoreSynchronization = true;

			waitForElement('[data-event-type="email/send"]');

		});

		it('there should be a log of an email in the timeline that was send to the cc', ( ) => {

			expect($('zs-case-timeline-view').getText()).toContain(data.cc);

		});

		it('there should be a log of an email in the timeline that was send to the bcc', ( ) => {

			expect($('zs-case-timeline-view').getText()).toContain(data.bcc);

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			navigateTo(110);

		});

	});

});
