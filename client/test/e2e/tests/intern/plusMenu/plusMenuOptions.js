import navigateAs from './../../../functions/common/navigateAs';
import mouseOverCreateButton from './../../../functions/common/mouseOverCreateButton';

let checkPlusMenuOptions = options => {

	for (let option in options) {

		let optionPresence = options[option] ? 'available' : 'unavailable';

		it(`the plusMenu should have the ${option} option ${optionPresence}`, ( ) => {

			let dataname;

			switch (option) {

				case 'createCase':
					dataname = 'zaak';
				break;

				case 'createContact':
					dataname = 'contact';
				break;

				case 'createContactMoment':
					dataname = 'contact-moment';
				break;

				case 'createWidget':
					dataname = 'create_widget';
				break;

				case 'uploadDocument':
					dataname = 'upload';
				break;

				case 'useTemplate':
					dataname = 'sjabloon';
				break;

				case 'relateSubject':
					dataname = 'betrokkene';
				break;

				case 'sendEmail':
					dataname = 'email';
				break;

				case 'planCase':
					dataname = 'geplande-zaak';
				break;

				default:
				break;
			}

			expect($(`zs-contextual-action-menu .popup-menu-list [data-name="${dataname}"]`).isPresent()).toBe(options[option]);

		});

	}

};

describe('when on the dashboard as contactmanager', ( ) => {

	beforeAll(( ) => {

		navigateAs('contactbeheerder');

		mouseOverCreateButton();

	});

	let options = {
		createCase: true,
		createContact: true,
		createContactMoment: true,
		createWidget: true,
		uploadDocument: false,
		useTemplate: false,
		relateSubject: false,
		sendEmail: false,
		planCase: false
	};

	checkPlusMenuOptions(options);

});

describe('when on the dashboard as employee without special system roles', ( ) => {

	beforeAll(( ) => {

		navigateAs('plusknop');

		mouseOverCreateButton();

	});

	let options = {
		createCase: true,
		createContact: false,
		createContactMoment: true,
		createWidget: true,
		uploadDocument: false,
		useTemplate: false,
		relateSubject: false,
		sendEmail: false,
		planCase: false
	};

	checkPlusMenuOptions(options);

});

describe('when in a new case without assignee with edit rights', ( ) => {

	beforeAll(( ) => {

		navigateAs('plusknop', 105);

		mouseOverCreateButton();

	});

	let options = {
		createCase: true,
		createContact: false,
		createContactMoment: true,
		createWidget: false,
		uploadDocument: true,
		useTemplate: false,
		relateSubject: false,
		sendEmail: false,
		planCase: false
	};

	checkPlusMenuOptions(options);

});

describe('when in a new case with assignee with edit rights', ( ) => {

	beforeAll(( ) => {

		navigateAs('plusknop', 106);

		mouseOverCreateButton();

	});

	let options = {
		createCase: true,
		createContact: false,
		createContactMoment: true,
		createWidget: false,
		uploadDocument: true,
		useTemplate: true,
		relateSubject: true,
		sendEmail: true,
		planCase: true
	};

	checkPlusMenuOptions(options);

});

describe('when in a new case without assignee with manage rights', ( ) => {

	beforeAll(( ) => {

		navigateAs('plusknop', 107);

		mouseOverCreateButton();

	});

	let options = {
		createCase: true,
		createContact: false,
		createContactMoment: true,
		createWidget: false,
		uploadDocument: true,
		useTemplate: true,
		relateSubject: true,
		sendEmail: true,
		planCase: true
	};

	checkPlusMenuOptions(options);

});

describe('when in an open case without template', ( ) => {

	beforeAll(( ) => {

		navigateAs('plusknop', 108);

		mouseOverCreateButton();

	});

	let options = {
		createCase: true,
		createContact: false,
		createContactMoment: true,
		createWidget: false,
		uploadDocument: true,
		useTemplate: false,
		relateSubject: true,
		sendEmail: true,
		planCase: true
	};

	checkPlusMenuOptions(options);

});

describe('when in a closed case', ( ) => {

	beforeAll(( ) => {

		navigateAs('plusknop', 109);

		mouseOverCreateButton();

	});

	let options = {
		createCase: true,
		createContact: false,
		createContactMoment: true,
		createWidget: false,
		uploadDocument: false,
		useTemplate: false,
		relateSubject: false,
		sendEmail: false,
		planCase: false
	};

	checkPlusMenuOptions(options);

});
