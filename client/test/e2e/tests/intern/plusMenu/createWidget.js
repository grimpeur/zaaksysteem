import navigateAs from './../../../functions/common/navigateAs';
import plusMenu from './../../../functions/intern/plusMenu';

describe('when viewing the dashboard', ( ) => {

	beforeAll(( ) => {

		navigateAs('dashboardempty');

	});

	describe('and creating a case intake widget', ( ) => {

		beforeAll(( ) => {

			plusMenu.createWidget('Zoekopdracht', 'Zaakintake');

		});

		it('the dashboard should contain a functional case intake', ( ) => {

			expect($('[data-name="intake"] [href="/intern/zaak/44"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			$('.widget-header-remove-button').click();

		});

	});

	describe('and creating a favorite casetype widget', ( ) => {

		beforeAll(( ) => {

			plusMenu.createWidget('Favoriete zaaktypen');

		});

		it('the dashboard should contain a functional favorite casetype widget', ( ) => {

			expect($('[data-name=""] .widget-favorite-link').getText()).toEqual('Dashboard');

		});

		afterAll(( ) => {

			$('.widget-header-remove-button').click();

		});

	});

});
