import navigateAs from './../../../functions/common/navigateAs';
import plusMenu from './../../../functions/intern/plusMenu';
import caseNav from './../../../functions/intern/caseView/caseNav';

describe('when opening case 110', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 110);

	});

	describe('when planning a case (singular) via the plusmenu', ( ) => {

		let data = {
			casetype: 'Basic casetype',
			from: '13-01-2020'
		};

		beforeAll(( ) => {

			plusMenu.planCase(data);

			caseNav.openTab('relations');

		});

		it('there should be a planned case for the given date in the relationstab', ( ) => {

			expect($('.planned-cases zs-table-body').getText()).toContain(data.from);

		});

		afterAll(( ) => {

			caseNav.openTab('phase');

		});

	});

	describe('when planning a case (repetative) via the plusmenu', ( ) => {

		let data = {
			casetype: 'Basic casetype',
			from: '13-01-2020',
			pattern: true,
			count: '3',
			type: 'Weken',
			repeat: '5'
		};

		beforeAll(( ) => {

			plusMenu.planCase(data);

			caseNav.openTab('relations');

		});

		it('there should be a planned case for the given date in the relationstab', ( ) => {

			expect($('.planned-cases zs-table-body').getText()).toContain(data.from);

		});

		it('there should be a planned case with the given pattern in the relationstab', ( ) => {

			expect($('.planned-cases zs-table-body').getText()).toContain(`Elke ${data.count} ${data.type.toLowerCase()}`);

		});

		it('there should be a planned case with the given repeats in the relationstab', ( ) => {

			expect($('.planned-cases zs-table-body').getText()).toContain(`(${data.repeat} herhalingen)`);

		});

		afterAll(( ) => {

			caseNav.openTab('phase');

		});

	});

});
