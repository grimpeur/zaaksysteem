import navigateAs from './../../../functions/common/navigateAs';
import plusMenu from './../../../functions/intern/plusMenu';
import caseNav from './../../../functions/intern/caseView/caseNav';

describe('when uploading a document via the plusmenu', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', 110);

		plusMenu.uploadDocument('text.txt');

		caseNav.openTab('docs');

	});

	it('there should be a document in the documentstab', ( ) => {

		expect($('.document-list-main').getText()).toContain('text.txt');

	});

});
