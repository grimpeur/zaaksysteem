import navigateAs from './../../../functions/common/navigateAs';
import universalSearch from './../../../functions/intern/universalSearch';

describe('when searching for cases', ( ) => {

	beforeAll(( ) => {

		navigateAs();

		universalSearch.search('basic casetype');

	});

	it('and query is "zaak"', ( ) => {

		expect(universalSearch.countResults()).toBeGreaterThan(0);

	});

	afterEach(( ) => {

		universalSearch.exit();
		
	});

});
