import loginPage from './../../../functions/intern/loginPage';

describe('when logged in', ( ) => {

	describe('and user logs out', ( ) => {

		beforeEach(( ) => {

			loginPage.login();

			loginPage.logout();

		});

		it('should redirect to the login page', ( ) => {

			expect(browser.getCurrentUrl()).toMatch(/auth\/page/);

		});

	});

});
