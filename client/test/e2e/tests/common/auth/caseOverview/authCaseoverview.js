import navigateAs from './../../../../functions/common/navigateAs';
import navigateTo from './../../../../functions/common/navigateTo';
import universalSearch from './../../../../functions/intern/universalSearch';
import getContactUrl from './../../../../functions/common/getValue/getContactUrl';

describe('when logging in as user with authorisation to search case 25', ( ) => {

	beforeAll(( ) => {

		navigateAs('metzaakoverzichtrechten');

	});

	describe('and arriving on the dashboard', ( ) => {

		it('should have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(true);

		});

	});

	describe('and searching for case 25 in general search', ( ) => {

		beforeAll(( ) => {

			universalSearch.search('zaakoverzichtrechten');

		});

		it('should have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			universalSearch.exit();

		});

	});

	describe('and opening advanced search', ( ) => {

		beforeAll(( ) => {

			browser.get('/search/all');

		});

		it('should have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(true);

		});

	});

	describe('and opening contact overview', ( ) => {

		beforeAll(( ) => {

			browser.ignoreSynchronization = true;

			browser.get(getContactUrl('np', '3'));

		});

		it('should have case 25 present', ( ) => {

			expect($('#zaak25').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			navigateTo();

		});

	});

});

describe('when logging in as user without authorisation to search case 25', ( ) => {

	beforeAll(( ) => {

		navigateAs('zonderzaakoverzichtrechten');

	});

	describe('and arriving on the dashboard', ( ) => {

		it('should not have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(false);

		});

	});

	describe('and searching for case 25 in general search', ( ) => {

		beforeAll(( ) => {

			universalSearch.search('zaakoverzichtrechten');

		});

		it('should not have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(false);

		});

		afterAll(( ) => {

			universalSearch.exit();

		});

	});

	describe('and opening advanced search', ( ) => {

		beforeAll(( ) => {

			browser.get('/search/all');

		});

		it('should not have case 25 present', ( ) => {

			expect($('[href="/intern/zaak/25"]').isPresent()).toBe(false);

		});

	});

	describe('and opening contact overview', ( ) => {

		beforeAll(( ) => {

			browser.ignoreSynchronization = true;

			browser.get(getContactUrl('np', '3'));

		});

		it('should not have case 25 present', ( ) => {

			expect($('#zaak25').isPresent()).toBe(false);

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			navigateTo();

		});

	});

});
