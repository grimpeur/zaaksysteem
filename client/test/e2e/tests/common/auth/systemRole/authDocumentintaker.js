import navigateAs from './../../../../functions/common/navigateAs';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		navigateAs('behandelaar');

	});

	describe('and opening the documentintake', ( ) => {

		beforeAll(( ) => {

			browser.get('/zaak/intake?scope=documents');

		});

		it('should not have the type selector present', ( ) => {

			expect($('.document-list-visibility-select').isDisplayed()).toBe(false);
				
		});

	});

});

describe('when logging in as documentintaker', ( ) => {

	beforeAll(( ) => {

		navigateAs('documentintaker');

	});

	describe('and opening the documentintake', ( ) => {

		beforeAll(( ) => {

			browser.get('/zaak/intake?scope=documents');

		});

		it('should have the type selector present', ( ) => {

			expect($('.document-list-visibility-select').isDisplayed()).toBe(true);
				
		});

	});

});
