import navigateAs from './../../../../functions/common/navigateAs';
import mainMenu from './../../../../functions/intern/mainMenu';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		navigateAs('behandelaar');

	});

	describe('and open the notifications', ( ) => {

		beforeAll(( ) => {

			mainMenu.toggleNotifications();

		});

		it('should not have a new account notification present', ( ) => {

			expect($('[icon-type="account-star-variant"]').isPresent()).toBe(false);

		});

	});

});

describe('when logging in as gebruikersbeheerder', ( ) => {

	beforeAll(( ) => {

		navigateAs('gebruikersbeheerder');

	});

	describe('and open the notifications', ( ) => {

		beforeAll(( ) => {

			mainMenu.toggleNotifications();

		});

		it('should have a new account notification present', ( ) => {
			
			expect($('[icon-type="account-star-variant"]').isPresent()).toBe(true);
			
		});

	});

});
