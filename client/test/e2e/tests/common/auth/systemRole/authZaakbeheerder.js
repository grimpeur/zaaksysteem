import navigateAs from './../../../../functions/common/navigateAs';
import plusMenu from './../../../../functions/intern/plusMenu';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		navigateAs('behandelaar');

	});

	describe('and open a registration form', ( ) => {

		beforeAll(( ) => {

			let newCase = {
				casetype: 'basic casetype',
				requestorType: 'natuurlijk_persoon',
				requestor: '123456789'
			};

			plusMenu.createCase(newCase);

		});

		it('should not have the skip required option present', ( ) => {

			expect($('.cheating').isPresent()).toBe(false);

		});

	});

});

describe('when logging in as zaakbeheerder', ( ) => {

	beforeAll(( ) => {

		navigateAs('zaakbeheerder');

	});

	describe('and open a registration form', ( ) => {

		beforeAll(( ) => {

			let newCase = {
				casetype: 'basic casetype',
				requestorType: 'natuurlijk_persoon',
				requestor: '123456789'
			};

			plusMenu.createCase(newCase);

		});

		it('should have the skip required option present', ( ) => {

			expect($('.cheating').isPresent()).toBe(true);

		});

	});

});
