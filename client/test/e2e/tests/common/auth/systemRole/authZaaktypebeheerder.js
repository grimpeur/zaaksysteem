import navigateAs from './../../../../functions/common/navigateAs';
import navigateTo from './../../../../functions/common/navigateTo';
import verifyRestricted from './../../../../functions/common/auth/verifyRestricted';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		navigateAs('behandelaar');

	});

	describe('and opening the catalog', ( ) => {

		it('should show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/bibliotheek')).toBe(true);

		});

	});

	describe('and opening a case', ( ) => {

		beforeAll(( ) => {

			navigateTo(24);

		});

		it('should not have the rule mode button present', ( ) => {

			expect($('[data-name="debug_rules"]').isPresent()).toBe(false);

		});

	});

});

describe('when logging in as zaaktypebeheerder', ( ) => {

	beforeAll(( ) => {

		navigateAs('zaaktypebeheerder');

	});

	describe('and opening the catalog', ( ) => {

		it('should not show the no access page', ( ) => {

			expect(verifyRestricted('/beheer/bibliotheek')).toBe(false);

		});

	});

	describe('and opening a case', ( ) => {

		beforeAll(( ) => {

			navigateTo(24);

		});

		it('should have the rule mode button present', ( ) => {

			expect($('[data-name="debug_rules"]').isPresent()).toBe(true);

		});

	});

});
