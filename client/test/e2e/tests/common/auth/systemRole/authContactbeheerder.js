import navigateAs from './../../../../functions/common/navigateAs';
import navigateTo from './../../../../functions/common/navigateTo';
import mouseOverCreateButton from './../../../../functions/common/mouseOverCreateButton';
import mouseOut from './../../../../functions/common/mouseOut';
import getContactUrl from './../../../../functions/common/getValue/getContactUrl';

describe('when logging in as behandelaar', ( ) => {

	beforeAll(( ) => {

		navigateAs('behandelaar');

	});

	describe('and opening the plus button', ( ) => {

		beforeAll(( ) => {

			mouseOverCreateButton();

		});

		it('should not have the create contact button present', ( ) => {

			expect($('li[data-name="contact"]').isPresent()).toBe(false);

		});

		afterAll(( ) => {

			mouseOut();

		});

	});

	describe('and opening a contact overview', ( ) => {

		beforeAll(( ) => {

			browser.ignoreSynchronization = true;

			browser.get(getContactUrl('np', '1'));

		});

		it('should not have the edit contact button present', ( ) => {

			expect($('[title="Bewerken"]').isPresent()).toBe(false);

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			navigateTo();

		});

	});

});

describe('when logging in as contactbeheerder', ( ) => {

	beforeAll(( ) => {

		navigateAs('contactbeheerder');

	});

	describe('and opening the plus button', ( ) => {

		beforeAll(( ) => {

			mouseOverCreateButton();

		});

		it('should have the create contact button present', ( ) => {

			expect($('li[data-name="contact"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			mouseOut();

		});

	});

	describe('and opening a contact overview', ( ) => {

		beforeAll(( ) => {

			browser.ignoreSynchronization = true;

			browser.get(getContactUrl('np', '1'));

		});

		it('should have the edit contact button present', ( ) => {

			expect($('[title="Bewerken"]').isPresent()).toBe(true);

		});

		afterAll(( ) => {

			browser.ignoreSynchronization = false;

			navigateTo();

		});

	});

});
