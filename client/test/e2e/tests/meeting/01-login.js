import navigateAs from './../../functions/common/navigateAs';

describe('when logging in to the meeting app', ( ) => {

	describe('with valid credentials', ( ) => {

		beforeEach(() => {

			navigateAs('burgemeester', '/vergadering/bbv/');

		});

		it('should redirect to the application', ( ) => {

			expect(browser.getCurrentUrl()).toMatch(/vergadering/);

		});

	});

	describe('with invalid credentials', ( ) => {

		beforeEach(() => {

			navigateAs('burgemeester', '/vergadering/bbv/', 'wrong password');

		});

		it('should redirect to the login form', ( ) => {

			expect(browser.getCurrentUrl()).toMatch('/auth/page');

		});

	});

});
