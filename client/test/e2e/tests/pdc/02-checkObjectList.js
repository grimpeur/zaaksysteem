import navigateAs from './../../functions/common/navigateAs';

describe('when opening the pdc app', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', '/pdc/');

	});

	it('should contain a header', ( ) => {

		let nav = element(by.css('pdc-nav'));
		
		expect(nav.isPresent());

	});

	it('should contain a list with items', ( ) => {

		let objectList = element(by.css('object-list-item-list'));

		expect(objectList.isPresent());

	});


	it('should contain objects', ( ) => {

		let objects = element.all(by.css('object-list-item-list-item'));
		
		expect(objects.count()).toBeGreaterThan(0);

	});

});
