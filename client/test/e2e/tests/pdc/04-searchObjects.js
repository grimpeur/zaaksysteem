import navigateAs from './../../functions/common/navigateAs';

describe('when attempting to search in the pdc app', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', '/pdc/');

	});

	it('should contain a searchbar', ( ) => {

		let nav = element(by.css('.object-list-search input'));
		
		expect(nav.isPresent());

	});


	it('should update the list when entering a query', ( ) => {

		element(by.css('.object-list-search input')).sendKeys('XXXX');
		element(by.css('.object-list-search button')).click();

		let objects = element.all(by.css('object-list-item-list-item')).filter( (products) => {
			return products.isDisplayed();
		});
		
		expect(objects.count()).toBe(0);

	});

});
