import navigateAs from './../../functions/common/navigateAs';

describe('when opening the pdc app', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', '/pdc/');

	});

	it('should redirect to the PDC app without logging in', ( ) => {

		expect(browser.getCurrentUrl()).toContain('/pdc/');

	});

});
