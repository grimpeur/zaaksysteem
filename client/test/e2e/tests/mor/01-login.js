import navigateAs from './../../functions/common/navigateAs';

describe('when logging in to the mor app', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', '/mor/');

	});

	describe('with valid credentials', ( ) => {

		it('should redirect to the application', ( ) => {

			expect(browser.getCurrentUrl()).toMatch(/mor/);

		});

	});

	describe('with invalid credentials', ( ) => {

		it('should redirect to the login form', ( ) => {

			navigateAs('admin', '/mor/', 'wrong password');

			expect(browser.getCurrentUrl()).toContain('/auth/page');

		});

	});

});
