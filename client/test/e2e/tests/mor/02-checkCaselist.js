import navigateAs from './../../functions/common/navigateAs';

describe('when opening the mor app', ( ) => {

	beforeAll(( ) => {

		navigateAs('admin', '/mor/');

	});

	it('should contain a header', ( ) => {

		let nav = element.all(by.css('mor-nav'));
		
		expect(nav.count()).toBeGreaterThan(0);

	});


	it('should contain two caseLists on Open', ( ) => {

		let caseListOpen = element.all(by.css('.case-group'));
		
		expect(caseListOpen.count()).toBe(2);

	});

	it('should contain one caseList on Afgehandeld', ( ) => {

		browser.get('/mor/zaken/afgehandeld');

		let caseList = element.all(by.css('.case-group'));

		expect(caseList.count()).toBe(1);

	});


	it('should contain cases', ( ) => {

		let cases = element.all(by.css('case-list-item-list-item'));
		
		expect(cases.count()).toBeGreaterThan(0);

	});

});
