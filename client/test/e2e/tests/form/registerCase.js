import navigateAs from './../../functions/common/navigateAs';
import plusMenu from './../../functions/intern/plusMenu';
import caseMenu from './../../functions/intern/caseView/caseMenu';
import completeFormStep from './../../functions/form/completeFormStep';

describe('when registering a case with a citizen as requestor', ( ) => {

	beforeAll(( ) => {

		navigateAs();

		let newCase = {
			casetype: 'basic casetype',
			requestorType: 'natuurlijk_persoon',
			requestor: '123456789'
		};

		plusMenu.createCase(newCase);

		completeFormStep(2);

	});

	it('the case should be registered', ( ) => {

		expect($('.case-view').isPresent()).toBe(true);

	});

	it('the requestor should be equal to the citizen', ( ) => {

		expect(caseMenu.getAboutValue('Aanvrager')).toEqual('T. Testpersoon');

	});

});

describe('when registering a case with an organisation as requestor', ( ) => {

	beforeAll(( ) => {

		navigateAs();

		let newCase = {
			casetype: 'basic casetype',
			requestorType: 'bedrijf',
			requestor: '12345678'
		};

		plusMenu.createCase(newCase);

		completeFormStep(2);

	});

	it('the case should be registered', ( ) => {

		expect($('.case-view').isPresent()).toBe(true);

	});

	it('the requestor should be equal to the organisation', ( ) => {

		expect(caseMenu.getAboutValue('Aanvrager')).toEqual('Testbedrijf');

	});

});

describe('when registering a case with an employee as requestor', ( ) => {

	beforeAll(( ) => {

		navigateAs();

		let newCase = {
			casetype: 'basic casetype',
			requestorType: 'medewerker'
		};

		plusMenu.createCase(newCase);

		completeFormStep(2);

	});

	it('the case should be registered', ( ) => {

		expect($('.case-view').isPresent()).toBe(true);

	});

	it('the requestor should be equal to the employee', ( ) => {

		expect(caseMenu.getAboutValue('Aanvrager')).toEqual('admin');

	});

});
