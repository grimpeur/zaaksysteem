require('babel-core/register')({
	presets: [ 'es2015' ],
	plugins: [ 'add-module-exports' ]
});

var path = require('path'), // eslint-disable-line
	baseUrl = 'https://testbase-local.zaaksysteem.nl',
	reporter = require('./custom_reporter.js'),
	folder = process.env.npm_config_folder ? `${process.env.npm_config_folder}/**/` : '',
	file = process.env.npm_config_file ? process.env.npm_config_file : '*',
	testpath = `**/${folder}${file}.js`;

exports.config = {
	allScriptsTimeout: 2500000,
	specs: [
		`tests/${testpath}`
	],
	suites: {
		common: `tests/common/${testpath}`,
		form: `tests/form/${testpath}`,
		intern: `tests/intern/${testpath}`,
		apps: [`tests/meeting/${testpath}`, `tests/mor/${testpath}`, `tests/pdc/${testpath}`, `tests/pip/${testpath}`]
	},
	capabilities: {
		'browserName': 'chrome',
		'chromeOptions': {
			args: [
				'--disable-extensions',
				'--disable-web-security',
				`--user-data-dir=${path.join(__dirname, './../../../inc/chrome-profile')}`
			]
		}
	},
	chromeOnly: true,
	framework: 'jasmine2',
	directConnect: true,
	baseUrl: baseUrl, //eslint-disable-line
	seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
	jasmineNodeOpts: {
		defaultTimeoutInterval: 2500000
	},
	onPrepare: function ( ) { //eslint-disable-line

		jasmine.getEnv().clearReporters();

		jasmine.getEnv().addReporter(reporter);

		browser.driver.get(baseUrl + '/auth/logout'); //eslint-disable-line

		browser.executeScript('window.sessionStorage.clear();');
		browser.executeScript('window.localStorage.clear();');

	}
};
