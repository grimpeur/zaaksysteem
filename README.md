# Zaaksysteem.nl

This is the main code repository for the Zaaksysteem framework.

For more information, visit our [website](http://www.zaaksysteem.nl/).

## Short technical introduction

All development for Zaaksysteem follows the following procedure when it comes
to getting the code in this codebase. Since this is open source software, you
are free to fork, modify, redistribute and open pull-requests, under the
limitations of the
[EUPL license](http://joinup.ec.europa.eu/software/page/eupl).

## Branches

* master
    The master branch contains the latest *stable* and released version of
    Zaaksysteem. This is the version most people want.

* quarterly
    The quarterly branch has a common ancestor at the current master
    branch, and contains all new features, bugfixes and other
    modifications done in a two week sprint This branch is considered
    *stable*, as it has been tested internally, but it has not seen a
    production environment yet.

* sprint
    This branch contains active and ongoing development modifications, it is
    *unstable* as it may contain modifications that have not been tested fully.
    It is part of our bi-weekly sprint cycle and will be reset at every
    sprint.

## Contributing

You are free to submit pull-requests for improvements to Zaaksysteem. Please
target those requests to our most recent sprint branch, and describe
liberally what the change does, why it does so, and what you believe the impact
will be.

# Running the development environment

Zaaksysteem uses [docker](http://www.docker.com/) to manage development
environments that are the same for every developer. To create a new
environment, you first need to install `docker` and `docker-compose`.
For docker please follow the installation instructions as found on the
[docker documenation page](https://docs.docker.com/engine/installation/).

Docker is very disk consuming, make sure you have sufficient space
somewhere for docker to use. One can tweak the default `/var/lib/docker`
to be elsewhere. On Debian/Ubuntu edit `/etc/default/docker` and add the
`-g` option: `DOCKER_OPTS="-g /path/to/free/space"`.  On Fedora/CentOS
one should edit `/etc/sysconfig/docker`, eg `other_args="-g
/path/to/free/space"`. For more information see the [docker forums]
(https://forums.docker.com/t/how-do-i-change-the-docker-image-installation-directory/1169).

## Generate configuration files
When you have completed the instalation you need to create some
configuration files by running `dev-bin/generate_config.sh`. Then add
`development.zaaksysteem.nl` in your `/etc/hosts` file:

`127.0.0.1   localhost development.zaaksysteem.nl`

## Start your docker

You can now start your development environment by running:

`docker-compose up`

This will build and start all the relevant containers.

## Connect with your database

You can connect with your database by installing a postgres SQL client
on your local box and add a `.pgpass` file with the relevant information,
the default user is `zaaksysteem` and the password is also
`zaaksysteem`.

`psql -h localhost -U zaaksysteem`

## SSL certificates

TODO: Explain more about `./dev-bin/generate_dev_certs.sh` and/or
importing the SSL certs from Docker to client and importing them in your
local trust store by running `certutil -d sql:$STORE -A -t "C,," -n "$label" -i $file`

## Override the docker-compose.yml file

In case you want to override certain `docker-compose.yml` entries but you
don't want to check them you can make use of the
`docker-compose.overide.yml` file, you can find a working example in
`docker-compose.overide.example`.

## Frontend development

Frontend developers may choose to have a local installed development
tools and don't want to rebuild their docker image on every change. They
are encouraged to use the override method that is provided via
`docker-compose.overide.example`. After adding the override method and
starting the frontend container, it is possible to run our JavaScript
bundler (Webpack) in dev mode (`npm run dev`) on your host machine
instead of inside the Docker container. This will allow hot reloading
and affords a speed increase in bundling to achieve much faster
development of frontend modules. See the `client` and `frontend`
directories for more information.

## Backend development

Building a new Perl docker layer can be done by running:
`docker build -f docker/Dockerfile.perl -t registry.gitlab.com/zaaksysteem/zaaksysteem-perl .`

Pushing the new layer is done like so (this can only be done by Mintlab
developers): `docker push registry.gitlab.com/zaaksysteem/zaaksysteem-perl`

### Support

We only support the community version of the software through the
[wiki](http://wiki.zaaksysteem.nl/). For professional support, please contact
[Mintlab](http://www.zaaksysteem.nl/).

-- 
The [Mintlab](http://www.mintlab.nl/) Team
