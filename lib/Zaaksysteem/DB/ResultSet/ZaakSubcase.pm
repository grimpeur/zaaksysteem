package Zaaksysteem::DB::ResultSet::ZaakSubcase;

use Moose;
use Data::Dumper;
use Params::Profile;

use Zaaksysteem::Constants;

extends 'DBIx::Class::ResultSet';


Params::Profile->register_profile(
    method  => 'update_subcase_ids',
    profile => {
        required => [ qw/zaak_id/],
        optional => [ qw/zaaktype_relatie_ids keep_zaaktype_ids/ ],
    }
);
sub update_subcase_ids {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options" . Dumper $dv unless $dv->success;

    die "need zaaktype_relatie_ids" unless("ids: " . $params->{zaaktype_relatie_ids});

    eval {
        $self->result_source->schema->txn_do(sub {
            $self->result_source->resultset->search({
                zaak_id => $params->{zaak_id},
                zaaktype_relatie_id => {
                    -not_in => $params->{keep_zaaktype_ids}
                },
            })->delete;

            for my $zaaktype_relatie_id (@{$params->{zaaktype_relatie_ids}}) {


                my $zaak_subcase = $self->create({
                    zaaktype_relatie_id         => $zaaktype_relatie_id,
                });

                my $zaaktype_relatie = $self->result_source->schema
                    ->resultset('ZaaktypeRelatie')
                    ->find($zaaktype_relatie_id);

                die "couldn't load zaaktype_relatie obj for id = $zaaktype_relatie_id"
                    unless($zaaktype_relatie);

                foreach my $field (qw/eigenaar_type kopieren_kenmerken ou_id role_id/) {
                    warn "field: $field";
                    warn "value: " . $zaaktype_relatie->$field;
                    $zaak_subcase->$field($zaaktype_relatie->$field);
                }

                $zaak_subcase->update();
            }
        });
    };

    if ($@) {
        warn 'Arguments: ' . Dumper($params);
        die 'update_subcase_ids failed: ' . $@;
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 update_subcase_ids

TODO: Fix the POD

=cut

