package Zaaksysteem::DB::ResultSet::ZaaktypeStandaardBetrokkenen;
use Moose;

extends qw(
    DBIx::Class::ResultSet
    Zaaksysteem::Zaaktypen::BaseResultSet
);

use Data::UUID;
use List::Util qw(none);
use Zaaksysteem::Constants qw(BASE_RELATION_ROLES);
use Zaaksysteem::Tools;

use constant PROFILE => {
    required => [],
    optional => [],
};

sub _validate_session {
    my $self    = shift;
    my $profile = PROFILE;
    my $rv      = {};

    $self->__validate_session(@_, $profile);
}

sub _commit_session {
    my ($self, $node, $element_session_data) = @_;

    my $schema = $self->result_source->schema;

    my $custom_roles = $schema->resultset('Config')->get_value('custom_relation_roles') || [];

    foreach my $betrokkene_row (keys %$element_session_data) {
        my $row = $element_session_data->{$betrokkene_row};

        $row->{gemachtigd} //= 0;
        $row->{notify} //= 0;

        $row->{uuid} //= Data::UUID->new->create_str();

        if (none { $row->{rol} eq $_ } @{ BASE_RELATION_ROLES() }, @$custom_roles) {
            push @$custom_roles, $row->{rol};
        }
    }

    $schema->resultset('Config')->save(
        { 'custom_relation_roles' => $custom_roles },
        1, # "advanced" flag - used on insert when the doesn't exist yet.
        0, # "apply defaults" - don't do that.
    );

    $self->next::method($node, $element_session_data);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
