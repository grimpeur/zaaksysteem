package Zaaksysteem::Controller::API::v1::General::CountryCodes;
use Moose;
use DateTime;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw(RGBZ_LANDCODES);
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Object::Types::CountryCode;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has api_capabilities => (
    is          => 'ro',
    default     => sub { return [qw/extern intern allow_pip/] }
);

has '+namespace' => (
    default => 'country_codes'
);

=head1 NAME

Zaaksysteem::Controller::API::v1::General::CountryCodes - API v1 controller for country codes

=head1 DESCRIPTION

This controller returns the 'landcodes' in use by Dutch local governments and ISO 3166

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/general/base') : PathPart('country_codes') : CaptureArgs(0) : Scope('general') {
    my ($self, $c)      = @_;

    $self->get_all_country_codes($c);
}

=head2 get_all_country_codes

Get all the legal entitities from Zaaksysteem

=cut

sub get_all_country_codes {
    my ($self, $c)      = @_;

    my $entities = RGBZ_LANDCODES();

    my @o;
    foreach (sort keys %$entities) {
        push(@o, Zaaksysteem::Object::Types::CountryCode->new(
            dutch_code  => $_,
            label       => $entities->{$_},
        ));
    }

    @o = sort { $a->label cmp $b->label } @o;

    my $set = Zaaksysteem::API::v1::ArraySet->new(
        content       => \@o,
        allow_rows_per_page => 500,
    );

    $c->stash->{set} = $set;
}

=head2 list

=head3 URL Path

C</api/v1/general/country_codes>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->list_set($c);
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    my $entities = RGBZ_LANDCODES();

    if ($entities->{$uuid}) {
        $c->stash->{$self->namespace} = Zaaksysteem::Object::Types::CountryCode->new(
            dutch_code  => $uuid,
            label       => $entities->{$uuid},
        );
    }
    else {
        throw(
            'api/v1/general/country_codes/not_found',
            "Unable to find country code '$uuid'",
        );
    }
}

=head2 get

=head3 URL Path

C</api/v1/general/country_codes/[DUTCH_CODE]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->get_object($c);
}


=head2 create

This action is not implemented

=head3 URL Path

C</api/v1/general/country_codes/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    throw(
        'api/v1/general/country_codes/create/not_allowed',
        'Creation of country codes is not allowed',
    );

}

=head2 delete

This action is not implemented

=head3 URL Path

C</api/v1/general/country_codes/UUID/delete>

=cut

sub delete : Chained('instance_base') : PathPart('delete') : Args(0) : RW {
    my ($self, $c)      = @_;

    $self->assert_post($c);

    throw(
        'api/v1/general/country_codes/delete/not_allowed',
        'Deletion of country codes is not allowed',
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
