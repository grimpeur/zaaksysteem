package Zaaksysteem::Zaken::Model;

use Moose;

with 'MooseX::Log::Log4perl';

use URI;
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Zaken::Model - A model for a Zaaksysteem Case object to work with

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::Zaken::Model;

    my $model = Zaaksysteem::Zaken::Model->new(
        queue_model => $queue_model,
    );

    $model->execute_phase_actions($zaak, { phase => 1 });

=head1 ATTRIBUTES

=head2 queue_model

An L<Zaaksysteem::Object::Qeueu::Model> object

=cut

has queue_model => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Object::Queue::Model',
    required => 1
);

=head2 subject

Holds a reference toi the effective user interacting with the model.

=cut

has subject => (
    is => 'ro',
    isa => 'Zaaksysteem::Backend::Subject::Component'
);

=head1 METHODS

=head2 execute_phase_actions

    $self->execute_phase_actions($zaak, { phase => 1 });

Execute action for a given phase

Named arguments

=over

=item phase

The ID of the phase/milstone.

=item skip_assignment

Skip the allocation assignment, booleanish value

=back

=cut

define_profile execute_phase_actions => (
    required => { phase           => 'Int', },
    optional => { skip_assignment => 'Bool' },
);

sig execute_phase_actions => 'Zaaksysteem::Zaken::ComponentZaak, HashRef';

sub execute_phase_actions {
    my $self = shift;
    my $zaak = shift;
    my $args = assert_profile(shift)->valid;

    my $phase = $args->{ phase };

    # Handle phase actions
    my $actions;
    if ($phase == 1) {
        $actions = $zaak->registratie_fase->case_actions->search(
            { case_id => $zaak->id })->sorted;

    }
    else {
        $actions
            = $zaak->zaaktype_node_id->zaaktype_statussen->search(
            { status => $phase })->first->case_actions->search({ $zaak->id })
            ->sorted;
    }

    $actions->apply_rules(
        {
            case      => $zaak,
            milestone => $args->{phase},
        }
    );

    my @case_actions = $actions->all;

    # Get them by execution ordering (sorted just gives the ordering per type)
    my @ordered_actions = (
        (grep { $_->type eq 'subject' } @case_actions),
        (grep { $_->type eq 'allocation' } @case_actions),
        (grep { $_->type eq 'template' } @case_actions),
        (grep { $_->type eq 'case' } @case_actions),
        (grep { $_->type eq 'email' } @case_actions)
    );

    for my $action (@ordered_actions) {
        next unless $action->automatic;

        $self->log->debug(
            sprintf(
                'Running case registration phase action %d ("%s")',
                $action->id, $action->label
            )
        );

        # ZS-13337: Skip allocation actions if a specific assignee is provided
        if ($action->type eq 'allocation' && $args->{skip_assignment}) {
            $self->log->debug(
                'Allocation action overridden by specific assignee');
            next;
        }

        $self->queue_model->run(
            $zaak->fire_action(
                action       => $action,
                current_user => $self->subject
            )
        );
    }

    # Handwired rule execution... sigh
    my $rules_result = $zaak->execute_rules({ status => $args->{phase} });

    if ($rules_result) {
        if ($rules_result->{send_external_system_message}) {
            try {
                $zaak->send_external_system_messages(
                    base_url => $self->queue_model->base_uri,
                    rules_result =>
                        $rules_result->{send_external_system_message}
                );
            }
            catch {
                $self->log->error($_);
            }
        }

        if ($rules_result->{wijzig_registratiedatum}) {
            try {
                $zaak->process_date_of_registration_rule(
                    %{ $rules_result->{wijzig_registratiedatum} });
            }
            catch {
                $self->log->error($_);
            }
        }
    }
    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
