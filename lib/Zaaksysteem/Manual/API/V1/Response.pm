=head1 NAME

Zaaksysteem::API::V1::Response - API v1 request response

=head1 DESCRIPTION

This page documents the top-level structure of response messages that clients
can be expected to receive.

The content-type for respones will be C<application/json> by default, with
exception of requests that result in a file attachment.

All response bodies contain a wrapped C<result> object, which is the specific
response of the request.

=head1 JSON

=begin javascript

{
    "api_version": "1",
    "request_id": "env-123456-abcdef",
    "development": false,
    "status_code": 200,
    "result": { ... }
}

=end javascript

=head2 Fields

=over 4

=item api_version

This indicates the version of the used API.

=item request_id

This field contains a structured request identifier, which the API machine
logs. Use this identifier if the connecting client is receiving unexpected or
invalid data in the response of a request.

=item development

State flag, indicates whether the request was made on a development
environment or not.

=item status_code

A reflection of the HTTP status code of the response.

=item result

Contains the result of the request. The result may be C<null>, in which case
the status code should be indicative of request success.

The result, if not C<null>, will be returned as an object, the structure of
which is documented L<here|Zaaksysteem::API::V1::ObjectSerialization>.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
