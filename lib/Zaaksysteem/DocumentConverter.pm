package Zaaksysteem::DocumentConverter;
use Moose;

with 'MooseX::Log::Log4perl';

use autodie;
use File::Temp;
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::DocumentConverter - Convert documents using unoconv

=head1 SYNOPSIS

    my $converter = Zaaksysteem::DocumentConverter->new();
    $converter->convert_file(
        'destination_type' => 'pdf',
        'destination_filename' => '/tmp/my.pdf',
        'source_filename'      => '/tmp/source.odt',
    );

=cut

=head1 ATTRIBUTES

=head2 unoconv_executable

Command used  to run unoconv.

Defaults to a value that includes setting the locale to nl_NL.UTF_8 before
running unoconv.

=cut

has unoconv_executable => (
    is      => 'ro',
    isa     => 'ArrayRef[Str]',
    default => sub {
        [
            # flock: creates/locks the specified file, and only allows one instance to run at once.
            # env:   forces the specified environment variables to be set
            qw(/usr/bin/flock /tmp/zs-unoconv.lock /usr/bin/env LC_ALL=nl_NL.UTF-8 /usr/bin/unoconv)
        ]
    },
);

=head1 METHODS

=head2 convert_file

Converts a named file and puts the output in another named file.

Arguments:

=over

=item * destination_type

The file type you want to create, like "pdf", "odt" or "xlsx".

The conversion will use PDF/A if "pdf" is selected.

The full list of available types can be found with C<unoconv --show>.

=item * source_filename

Name of the file to convert.

=item * destination_filename

Name of the file the conversion result will be sent to.

=back

And optionally:

=over

=item * filter_options

Specify a value for the "--input" flag of unoconv. Can be used to tune the CSV
parser (separator and quote characters used, etc.).

=back

=cut

define_profile convert_file => (
    required => {
        destination_type     => 'Str',
        destination_filename => 'Str',
        source_filename      => 'Str',
    },
    optional => {
        filter_options => 'Str',
    },
);

sub convert_file {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $fh = $self->convert_to_fh(
        destination_type => $args->{destination_type},
        source_filename  => $args->{source_filename},
        filter_options   => $args->{filter_options},
    );

    open my $dst_fh, ">", $args->{destination_filename};
    binmode($dst_fh);
    {
        local $/;
        print $dst_fh readline($fh);
    }
    close $dst_fh;

    return;
}

=head2 convert_scalar

Converts the contents of a scalar into another scalar.

Arguments:

=over

=item * destination_type

See C<convert_file>

=item * source

Scalar containing the source document.

=back

And optionally:

=over

=item * filter_options

See C<convert_file>

=back

=cut

define_profile convert_scalar => (
    required => {
        destination_type => 'Str',
        source           => 'Str',
        source_extension => 'Str',
    },
    optional => {
        filter_options => 'Str',
    },
);

sub convert_scalar {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $src = File::Temp->new(
        TEMPLATE => 'zsconvert-XXXXX',
        SUFFIX   => $args->{source_extension},
        DIR      => '/tmp',
        UNLINK   => 1,
    );
    binmode($src);
    print $src $args->{source};
    $src->close();

    my $fh = $self->convert_to_fh(
        destination_type => $args->{destination_type},
        source_filename  => $src->filename,
        filter_options   => $args->{filter_options},
    );

    my $output = do {
        local $/;
        readline($fh);
    };

    return $output;
}

=head2 convert_to_fh

Converts a named file and returns a file handle that can be read to get the
converted document.

Used internally by C<convert_scalar> and C<convert_file>.

Arguments:

=over

=item * destination_type

See C<convert_file>.

=item * source_filename

See C<convert_file>.

=back

And optionally:

=over

=item * filter_options

See C<convert_file>.

=back

=cut

define_profile convert_to_fh => (
    required => {
        destination_type     => 'Str',
        source_filename      => 'Str',
    },
    optional => {
        filter_options => 'Str',
    },
);

sub convert_to_fh {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my @extra_args;
    if($args->{filter_options}) {
        push @extra_args, "-i", "FilterOptions=$args->{filter_options}"
    }

    if ($args->{destination_type} eq 'pdf') {
        # Force PDF/A
        push @extra_args, "-e", "SelectPdfVersion=1"
    }

    my @command = (
        @{ $self->unoconv_executable },
        "--format=$args->{destination_type}",
        @extra_args,
        "--stdout",
        $args->{source_filename},
    );

    $self->log->debug("Starting unoconv as: " . join(" ", @command));

    open my $fh, "-|", @command;
    binmode($fh);

    return $fh;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
