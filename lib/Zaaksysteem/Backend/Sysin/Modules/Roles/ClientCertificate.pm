package Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate;
use Moose::Role;

use Crypt::OpenSSL::X509;
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate - Role for interfaces that need to verify a client certificate

=head1 SYNOPSIS

    package Zaaksysteem::Backend::Sysin::Modules::MyAwesomeInterface;
    extends 'Zaaksysteem::Backend::Sysin::Modules';

    with qw/
        Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric
        Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate
    /;

    # etc.
    # The /sysin/interface/XXX/soap API endpoint uses a configuration field
    # named "(interface_)client_certificate" to find the configured one.

=head1 METHODS

=head2 verify_client_certificate

Verify if the given certificate hash matches the supplied certificate.

=cut

define_profile verify_client_certificate => (
    required => {
        get_certificate_cb => 'CodeRef',
    },
    optional => {
        client_cert => 'Str',
        dn => 'Str',
    },
);

sub verify_client_certificate {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $stored_certificate = $params->{get_certificate_cb}->();

    if (!$stored_certificate) {
        $self->log->error("Verification failed: No certificate found to check against.");
        return;
    }

    if (!$params->{client_cert}) {
        $self->log->error("Verification failed: No certificate found to check against.");
        return;
    }

    my $stored_certificate_obj = Crypt::OpenSSL::X509->new_from_file($stored_certificate->get_path);
    my $given_certificate_obj  = Crypt::OpenSSL::X509->new_from_string($params->{client_cert});

    $self->log->debug("Stored subject-DN:  " . $stored_certificate_obj->subject());
    $self->log->debug("Received subject-DN:  " . $params->{dn});

    if($given_certificate_obj->fingerprint_sha256() eq $stored_certificate_obj->fingerprint_sha256()) {
        $self->log->debug(
            sprintf(
                "Client certificate SUCCESS: SHA256 match: %s",
                $given_certificate_obj->fingerprint_sha256,
            ),
        );
        return 1;
    }

    $self->log->error(
        sprintf(
            "Client certificate FAILURE: SHA256 mismatch: %s != %s",
            $stored_certificate_obj->fingerprint_sha256(),
            $given_certificate_obj->fingerprint_sha256,
        ),
    );

    return;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
