package Zaaksysteem::Schema::ObjectRelationships;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ObjectRelationships

=cut

__PACKAGE__->table("object_relationships");

=head1 ACCESSORS

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 object1_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 object2_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 type1

  data_type: 'text'
  is_nullable: 0

=head2 type2

  data_type: 'text'
  is_nullable: 0

=head2 object1_type

  data_type: 'text'
  is_nullable: 0

=head2 object2_type

  data_type: 'text'
  is_nullable: 0

=head2 blocks_deletion

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 title1

  data_type: 'text'
  is_nullable: 1

=head2 title2

  data_type: 'text'
  is_nullable: 1

=head2 owner_object_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=cut

__PACKAGE__->add_columns(
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "object1_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "object2_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "type1",
  { data_type => "text", is_nullable => 0 },
  "type2",
  { data_type => "text", is_nullable => 0 },
  "object1_type",
  { data_type => "text", is_nullable => 0 },
  "object2_type",
  { data_type => "text", is_nullable => 0 },
  "blocks_deletion",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "title1",
  { data_type => "text", is_nullable => 1 },
  "title2",
  { data_type => "text", is_nullable => 1 },
  "owner_object_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
);
__PACKAGE__->set_primary_key("uuid");

=head1 RELATIONS

=head2 object2_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object2_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object2_uuid" },
);

=head2 object1_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object1_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object1_uuid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-11-02 19:42:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:P/MFyb7VtztYsGiSpygDKw

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

