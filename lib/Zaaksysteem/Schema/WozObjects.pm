package Zaaksysteem::Schema::WozObjects;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::WozObjects

=cut

__PACKAGE__->table("woz_objects");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'woz_objects_id_seq'

=head2 object_data

  data_type: 'text'
  is_nullable: 1

=head2 owner

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 object_id

  data_type: 'varchar'
  is_nullable: 0
  size: 32

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "woz_objects_id_seq",
  },
  "object_data",
  { data_type => "text", is_nullable => 1 },
  "owner",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "object_id",
  { data_type => "varchar", is_nullable => 0, size => 32 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:07:01
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SSSMhfvPvyV2lYG18vKIhg

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::WozObjects');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::WozObjects",
    __PACKAGE__->load_components()
);

use JSON::XS qw();

__PACKAGE__->inflate_column('object_data', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->encode(shift // {}) },
});

# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

