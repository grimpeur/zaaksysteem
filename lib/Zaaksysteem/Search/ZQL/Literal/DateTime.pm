package Zaaksysteem::Search::ZQL::Literal::DateTime;

use Moose;

use DateTime::Format::Strptime;

use Zaaksysteem::Search::Term::Literal;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'DateTime' );

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;
    my %param = @_;

    my $string = $param{ value };

    # Strip the millisecond and Z components
    $string =~ s/(\.\d{1,3})?Z//;

    my $format = DateTime::Format::Strptime->new(
        pattern => '%FT%T', # 2001-01-01T12:34:56Z
        time_zone => 'UTC'   # UTC because of the Z part in the pattern
    );

    $class->$orig(
        value => $format->parse_datetime($string)
    );
};

sub dbixify {
    my $self = shift;

    return Zaaksysteem::Search::Term::Literal->new(value => $self->value);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 dbixify

TODO: Fix the POD

=cut

