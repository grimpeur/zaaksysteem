package Zaaksysteem::Constants::Users;
use warnings;
use strict;

=head1 NAME

Zaaksysteem::Constants::Users - Constants for user rights

=head1 DESCRIPTION

This module defined constants for user rights. Currently only does:

C<REGULAR> meaning logged in user
C<API>  meaning API user
C<PIP>  meaning PIP user
C<FORM> meaning FORM user

=head1 SYNOPSIS

    use Zaaksysteem::Constants::Users qw(PIP API REGULAR FORM);

    # Check if the user is logged in or not
    $c->assert_user(PIP);

=cut

require Exporter;
our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(REGULAR PIP API FORM);
our %EXPORT_TAGS  = ( all => \@EXPORT_OK );

use constant REGULAR  => 8;
use constant API      => 4;
use constant PIP      => 2;
use constant FORM     => 1;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
