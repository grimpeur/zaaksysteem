package Zaaksysteem::Filestore::Engine;
use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksyteem::Filestore::Engine - Base

=head1 SYNOPSIS

    package Zaaksysteem::Filestore::Engine::Something;
    use Moose;
    with 'Zaaksysteem::Filestore::Engine';

    sub get_fh { ... }
    sub write { ... }

=head1 ATTRIBUTES

=head2 name [Required]

The name (as specified in the configuration) of this engine.

=cut

has name => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head1 REQUIRED METHODS

Classes implementing this role are required to implement the following methods:

=head2 download_url(uuid)

Return an (internal) URL for use in the C<X-Accel-Redirect> header, which Nginx
uses to offload file transfers. This prevents backend processes from staying
"busy/in use" until the download is finished.

This should return an absolute path, usually something like
C</download/pluginname/plugin_specific_part>, which is also configured in Nginx
as an "internal" location that does the actual download/proxying.

=head2 get_path(uuid)

Get the path to the file on a local disk, suitable for "open".

Returning a "self-cleaning" L<File::Temp> handle (which stringifies to the
file name) instead is allowed.

=head2 get_fh(uuid)

Get a file handle to read the file.

=head2 $id = write($fh)

Create a new file with data read from the specified file handle. Returns a
unique identifier that can be used to retrieve the data again.

=cut

requires 'download_url', 'get_path', 'get_fh', 'write';

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
