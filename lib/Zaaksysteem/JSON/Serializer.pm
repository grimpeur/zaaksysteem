package Zaaksysteem::JSON::Serializer;

use DateTime::Format::ISO8601;
use JSON::XS ();

=head2 from_json($JSON_STRING)

Return value: $PERL_HASH

    Zaaksysteem::JSON::Serializer::from_json($JSON_STRING);

Returns a perl hash, where the following objects are deserialized

=over 4

=item DateTime

    __DateTime__: '2014-01-01T09:00:00'

    Objects serialized with the above key will be serialized back to
    DateTime objects

=back

=cut

sub from_json {
    my $class           = shift;
    my $val             = shift;

    JSON::XS
      ->new
      ->filter_json_single_key_object (__DateTime__ => sub {
          my $dt = DateTime::Format::ISO8601->parse_datetime( shift );

          # Timestamps are stored as UTC but without a time zone indicator.
          # As "floating" time stamps are a pain to work with, set the time
          # zone to UTC
          $dt->set_time_zone('UTC');

          return $dt;
      })
      ->decode($val);
}

=head2 to_json($PERL_HASH)

Return value: $JSON_STRING

    Zaaksysteem::JSON::Serializer::to_json($PERL_HASH);

Returns a JSON formatted string. Objects defined in C<from_json> will be serialized.


=cut

sub to_json {
    my $class           = shift;
    my $val             = shift;

    no warnings 'redefine';
    local *DateTime::TO_JSON = sub {
        # Always store UTC
        { __DateTime__ => shift->clone->set_time_zone('UTC')->iso8601 . 'Z' };
    };

    return JSON
        ->new
        ->utf8(0)
        ->allow_blessed(1)
        ->convert_blessed(1)
        ->encode($val);
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

