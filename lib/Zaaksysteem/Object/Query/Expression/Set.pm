package Zaaksysteem::Object::Query::Expression::Set;

use Moose;

use Moose::Util::TypeConstraints qw[find_type_constraint role_type];
use Zaaksysteem::Tools;

with 'Zaaksysteem::Object::Query::Expression';

=head1 NAME

Zaaksysteem::Object::Query::Expression::Set - Abstracted list/collection
of expressions

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 expressions

Collection of L<expressions|Zaaksysteem::Object::Query::Expression> in the
set.

=cut

has expressions => (
    is => 'rw',
    isa => find_type_constraint('ArrayRef')->parameterize(
        role_type('Zaaksysteem::Object::Query::Expression')
    ),
    traits => [qw[Array]],
    required => 1,
    handles => {
        all_expressions => 'elements'
    }
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Zaaksysteem::Object::Query::Expression/stringify>.

    qb_set(qb_lit('text', 'abc'), qb_lit('timestamp', DateTime->now()))->stringify
    # "[ text(abc), timestamp(2017-05-11T11:24:34) ]"

=cut

sub stringify {
    my $self = shift;

    return sprintf('[ %s ]',
        join(', ', map { $_->stringify } $self->all_expressions )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
