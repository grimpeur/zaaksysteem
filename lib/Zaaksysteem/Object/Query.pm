package Zaaksysteem::Object::Query;

use Moose;
use namespace::autoclean;

use Moose::Exporter;
use Moose::Util::TypeConstraints qw[role_type];

use Zaaksysteem::Tools;

use Zaaksysteem::Object::Query::Expression::Conjunction;
use Zaaksysteem::Object::Query::Expression::Disjunction;
use Zaaksysteem::Object::Query::Expression::Inversion;
use Zaaksysteem::Object::Query::Expression::Equal;
use Zaaksysteem::Object::Query::Expression::Literal;
use Zaaksysteem::Object::Query::Expression::ContainsString;
use Zaaksysteem::Object::Query::Expression::MemberRelation;
use Zaaksysteem::Object::Query::Expression::Regex;
use Zaaksysteem::Object::Query::Expression::Field;
use Zaaksysteem::Object::Query::Expression::Set;
use Zaaksysteem::Object::Query::Sort;

Moose::Exporter->setup_import_methods(
    as_is => [qw[
        qb
        qb_and qb_or qb_not qb_in
        qb_eq qb_neq qb_like qb_re
        qb_field qb_lit qb_set
        qb_sort
    ]]
);

=head1 NAME

Zaaksysteem::Object::Query - High-level object query abstraction and API

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 type

Refers to the C<type> of the L<Zaaksysteem::Object> which instances of a
query should search for.

=cut

has type => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 cond 

Holds a reference to the root conditional expression with which the data
source should be queried.

Values must be objects which implement
L<Zaaksysteem::Object::Query::Expression>.

=cut

has cond => (
    is => 'rw',
    isa => role_type('Zaaksysteem::Object::Query::Expression'),
    predicate => 'has_cond',
    clearer => 'clear_cond'
);

=head2 sort

Holds a reference to a L<Zaaksysteem::Object::Query::Sort> object which should
be used for ordering of the results.

=cut

has sort => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::Query::Sort',
    predicate => 'has_sort',
    clearer => 'clear_sort'
);

=head1 METHODS

=head2 stringify

Generates a human-readable string representing the current query.

=cut

sub stringify {
    my $self = shift;

    my @parts = (sprintf('search for "%s" objects', $self->type));

    if ($self->has_cond) {
        push @parts, sprintf('where %s', $self->cond->stringify);
    }

    if ($self->has_sort) {
        push @parts, sprintf('sorted by %s', $self->sort->stringify);
    }

    return join ', ', @parts;
}

=head1 FUNCTIONS

This collection of functions provide convenience calls to instantiators of
expression objects.

=head2 qb

Instantiates a new query.

    my $query = qb('case');

Additionally, a C<HashRef> can be used to directly set the query's attributes.

    my $query = qb('case', {
        cond => qb_eq('case.number', 1234)
    });

=cut

sub qb {
    my ($type, $args) = (@_, {});

    return __PACKAGE__->new({ type => $type, %{ $args } });
}

=head2 qb_and

    # 1 AND 2 == 3 AND abc ILIKE '%a%'
    qb_and(1, qb_eq(2, 3), qb_like('abc', 'a'))

=cut

sub qb_and {
    my @expressions = @_;

    return Zaaksysteem::Object::Query::Expression::Conjunction->new(
        expressions => \@expressions
    );
}

=head2 qb_or

    # 1 OR 'Kings' = 'Queens' OR my_field ~ '^my.*value$'
    qb_or(1, qb_eq('Kings', 'Queens'), qb_re('my_field', '^my.*value$'))

=cut

sub qb_or {
    my @expressions = @_;

    return Zaaksysteem::Object::Query::Expression::Disjunction->new(
        expressions => \@expressions
    );
}

=head2 qb_in

    # 'abc' IN (1, 2, 3, 4, 'a', 'b', 'c', 'abc')
    qb_in('abc', [qw[1 2 3 a b c abc]])

=cut

sub qb_in {
    my $expression = _qb_maybe_expr(shift);
    my $set = _qb_maybe_expr(shift);

    return Zaaksysteem::Object::Query::Expression::MemberRelation->new(
        expression => $expression,
        set => $set
    );
}

=head2 qb_eq

    # requestor = <subject_uuid>
    qb_eq('requestor', $my_subject)

=cut

sub qb_eq {
    my $field = _qb_maybe_expr(shift);
    my $value = _qb_maybe_lit(shift);

    return Zaaksysteem::Object::Query::Expression::Equal->new(
        expressions => [ $field, $value ]
    );
}

=head2 qb_not

    # field != 'def'
    qb_not(qb_eq('field', 'def'))

=cut

sub qb_not {
    return Zaaksysteem::Object::Query::Expression::Inversion->new(
        expression => shift
    );
}

=head2 qb_neq

    # NOT(a = 'b')
    qb_neq('a', 'b')

=cut

sub qb_neq {
    return Zaaksysteem::Object::Query::Expression::Inversion->new(
        expression => qb_eq(@_)
    );
}

=head2 qb_like

    # my_field ILIKE '%text%'
    qb_like('my_field', 'text')

    # my_field ILIKE 'text%'
    qb_like('my_field', 'text', 'prefix')

=cut

sub qb_like {
    return Zaaksysteem::Object::Query::Expression::ContainsString->new(
        string => _qb_maybe_expr(shift),
        match => _qb_maybe_lit(shift),
        mode => (shift || 'infix')
    );
}

=head2 qb_re

    # my_field ~ '^a.[bc]+$'
    qb_re('my_field', '^a.[bc]+$')

=cut

sub qb_re {
    return Zaaksysteem::Object::Query::Expression::Regex->new(
        string => _qb_maybe_expr(shift),
        pattern => shift,
    );
}

=head2 qb_lit

    # 'abc'
    qb_lit('text', 'abc')

    # '2017-05-11T11:01:00'
    qb_lit('timestamp', DateTime->now)

    # 'e90cd356-ebfe-4b45-b9a8-7bc8aad8f4c3'
    qb_lit('object', Zaaksysteem::Object->new)

=cut

sub qb_lit {
    return Zaaksysteem::Object::Query::Expression::Literal->new(
        type => shift,
        value => shift
    );
}

=head2 qb_set

    # ('abc', '518b9270-0be1-433f-88fb-e3cbb0e2986b')
    qb_set(qb_lit('text', 'abc'), qb_lit('object', Zaaksysteem::Object->new))

=cut

sub qb_set {
    return Zaaksysteem::Object::Query::Expression::Set->new(
        expressions => \@_
    );
}

=head2 qb_field

    # my_field
    qb_field('my_field')

=cut

sub qb_field {
    return Zaaksysteem::Object::Query::Expression::Field->new(
        name => shift
    );
}

=head2 qb_sort

    # ORDER BY my_field DESC
    qb_sort('my_field', 'desc')

=cut

sub qb_sort {
    my $expr = _qb_maybe_expr(shift);
    my %reverse_map = ( asc => 0, desc => 1 );
    my $reverse = $reverse_map{ shift() || 'asc' };
    
    return Zaaksysteem::Object::Query::Sort->new(
        expression => $expr,
        reverse => $reverse
    );
}

# Privates and helpers here.

sub _qb_maybe_expr {
    my $expr = shift;
    my $ref = ref $expr;

    # Plain strings are interpreted as field references, so it's easy
    # to build conditions (qb_eq('my.field', 'my value'))
    return qb_field($expr) unless $ref;

    # Arrays become impliciet sets (see qb_in)
    return qb_set(map { _qb_maybe_lit($_) } @{ $expr }) if $ref eq 'ARRAY';

    # Maps become conjuncted equality conditionals, so querying with 
    # { a => 'my_val', b => 'other val' } can be done concisely
    return qb_and(map { qb_eq($_, $expr->{ $_ }) } keys %{ $expr })
        if $ref eq 'HASH';

    return $expr if blessed($expr) && $expr->does('Zaaksysteem::Object::Query::Expression');

    throw('object/query_builder/expression_expected', sprintf(
        'Unexpected expression "%s", expected literal or object',
        $expr
    ));
}

sub _qb_maybe_lit {
    my $value = shift;
    
    my $ref = ref $value;
    my $blessed = blessed($value);

    # Base case, we're already a literal object
    return $value if (
        $blessed && $value->isa('Zaaksysteem::Object::Query::Expression::Literal')
    );

    # Default type is text
    my $type = 'text';

    unless (defined $value) {
        $type = 'undefined',
    }

    if (ref $value eq 'DateTime') {
        $type = 'timestamp';
        $value = $value->iso8601;
    }

    if ($blessed && $value->does('Zaaksysteem::Object::Reference')) {
        $type = 'object';
        $value = $value->_ref; # Explicit fresh ref
    }

    if (ref $value eq 'ARRAY') {
        return qb_set(map { _qb_maybe_lit($_) } @{ $value });
    }

    return qb_lit($type, $value);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
